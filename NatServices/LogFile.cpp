#include <stdafx.h>

#include <LibSystem.h>

#include "NatServices.h"
#include "ConfigFactoryNatServices.h"

char g_szLogFile[MAX_PATH + 1];
char g_szLogFileTrace[MAX_PATH + 1];

FILE* OpenLog(char* szMode)
{
	FILE* LogFile;
	LogFile = fopen(g_szLogFile, szMode);
	return LogFile;
}

FILE* OpenLogTrace(char* szMode)
{
	FILE* LogFile;
	LogFile = fopen(g_szLogFileTrace, szMode);
	return LogFile;
}

BOOL CreateLogFileName(void)
{
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime);

	_stprintf(g_szLogFile, _T("%s\\%s.%04d%02d%02d.log"), (LPCTSTR)GetLogDir(), SZSERVICENAME,
		SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
	return TRUE;
}

BOOL CreateTraceFileName(void)
{
#ifdef _DEBUG
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime);

	wsprintf(g_szLogFileTrace, _T("%s\\%sTrace.%04d%02d%02d.log"), (LPCTSTR)GetLogDir(), SZSERVICENAME,
		SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
#endif
	return TRUE;
}

void Logf(const char* szFormat, ...)
{
	FILE* LogFile;

	LogFile = OpenLog("a");
	if (LogFile == NULL) return;

	va_list vaArgList;
	static char szBuffer[5000];

	va_start(vaArgList, szFormat);
	vsprintf(szBuffer, szFormat, vaArgList);
	fputs(szBuffer, LogFile);
	va_end(vaArgList);

	fclose(LogFile);
}

void Logfn(const char* szFormat, ...)
{
	FILE* LogFile;

	LogFile = OpenLog("a");
	if (LogFile == NULL) return;

	va_list vaArgList;
	static char szBuffer[5000];

	va_start(vaArgList, szFormat);
	vsprintf(szBuffer, szFormat, vaArgList);
	strcat(szBuffer, "\n");
	fputs(szBuffer, LogFile);
	va_end(vaArgList);

	fclose(LogFile);
}

void Tr1f(const char* szFormat, ...)
{
#ifdef _DEBUG
	FILE* TraceFile;


	TraceFile = OpenLogTrace("a");
	if (TraceFile == NULL) return;

	va_list vaArgList;
	static char szBuffer[5000];

	va_start(vaArgList, szFormat);
	vsprintf(szBuffer, szFormat, vaArgList);
	strcat(szBuffer, "\n");
	fputs(szBuffer, TraceFile);
	va_end(vaArgList);

	fclose(TraceFile);
#endif
}

void Tr2f(const char* szFormat, ...)
{
#ifdef _DEBUG
	FILE* TraceFile;

	TraceFile = OpenLogTrace("a");
	if (TraceFile == NULL) return;

	va_list vaArgList;
	static char szBuffer[5000];

	va_start(vaArgList, szFormat);
	vsprintf(szBuffer, szFormat, vaArgList);
	strcat(szBuffer, "\n");
	fputs(szBuffer, TraceFile);
	va_end(vaArgList);

	fclose(TraceFile);
#endif
}

void Tr3f(const char* szFormat, ...)
{
#ifdef _DEBUG
	FILE* TraceFile;

	TraceFile = OpenLog("a");
	if (TraceFile == NULL) return;

	va_list vaArgList;
	static char szBuffer[5000];

	va_start(vaArgList, szFormat);
	vsprintf(szBuffer, szFormat, vaArgList);
	strcat(szBuffer, "\n");
	fputs(szBuffer, TraceFile);
	va_end(vaArgList);

	fclose(TraceFile);
#endif
}
