#include <stdafx.h>
#include <winsock2.h>

#include <FileLog.h>
#include <LibSystem.h>
#include <dateref.h>
#include <telnetserver.h>

#include "NATServices.h"
#include "ConfigFactoryNatServices.h"

#include "NatClasses.h"
#include "Config.h"

//void DumpFrame2(LPCTSTR szBuffer, int nLenBuffer);
BOOL IsCommPort(LPCTSTR s);

extern HANDLE g_hEventServerWantToStop;

CDateRef g_lastconnectiondate;
CString g_lastconnectionip;

#define DUMP_LOG_FILE_FORMAT_TOKENS	"avail: {EXEDIR}/{LOGDIR}/{DUMPDIR}/{HOSTIP}/{HOSTPORT}/{REMOTEIP}/{REMOTEPORT}/{DATE}/{TIMESTAMP}"

static tt_cfgcmd g_cfgcmd[] = 
{
	{"List", "[opt] List all listen sockets\r\n"
	         "               where opt: <id>     : list peer connections\r\n"
			 "                          id       : sort by config id (default)\r\n"
			 "                          name     : sort by config name\r\n"
			 "                          locaddr  : sort by local address\r\n"
			 "                          ccon     : sort by current connections count\r\n"
			 "                          tcon     : sort by total connections count\r\n"
			 "                          noformat : raw display format"
			 , CConfig::CfgList},
	{"", NULL, NULL},

	{"AddConf", "<config_name> <list_addr:port> "
#if defined _NATLOAD
	"<con_addr1:port1[,<conf_addr2:port2>,<conf_addr3:port3>,...]> "
#elif defined _NAT
	"<con_addr:port> "
#else
	PP_ERROR
#endif
		"[bind_address] Add a new configuration", CConfig::CfgAddConf},
	{"DeleteConf", "<id> Delete a configuration", CConfig::CfgDeleteConf},
	{"RenameConf", "<id> <new_config_name>", CConfig::CfgRenameConf},
	{"AlterConfListen", "<id> <list_addr:port>", CConfig::CfgAlterConfListen},
#ifdef _NAT
	{"AlterConfConnect", "<id> <con_addr:port>", CConfig::CfgAlterConfConnect},
#endif
	{"AlterConfAddrOut", "<id> <bind_address>", CConfig::CfgAlterConfAddrOut},
	{"StartConf", "<id>|<*> Start specified configuration or all", CConfig::CfgStartConf},
	{"StopConf", "<id>|<*> Stop specified configuration or all", CConfig::CfgStopConf},
	{"EnableConf", "<id> Enable the specified configuration", CConfig::CfgEnableConf},
	{"DisableConf", "<id> Disable the specified configuration", CConfig::CfgDisableConf},
	{"LogConf", "<id> Log configuration", CConfig::CfgLogConf},
	{"UnlogConf", "<id> Don't log configuration", CConfig::CfgUnlogConf},
	{"BindPrivPortConf", "<id> Bind to a private port for NAT connection", CConfig::CfgBindPrivPortConf},
	{"UnbindPrivPortConf", "<id> Do not bind to a private port for NAT connection", CConfig::CfgUnbindPrivPortConf},
	{"ClrConCnt", "<id> Clear connections counter (TCon counter)", CConfig::CfgConnectionsCounter},
	{"DelayConf", "<id> <delay> Add a communication delay (in ms)", CConfig::CfgDelayConf},
	{"BufSizConf", "<id> <size> Set buffer size for receiving frames (in bytes)", CConfig::CfgBufSizConf},
	{"", NULL, NULL},
	
#if defined _NATLOAD
	{"AddLoad", "<id> <con_addr1:port1[,<conf_addr2:port2>,<conf_addr3:port3>,...]> Add new load addresses", CConfig::CfgAddLoad},
	{"DeleteLoad", "<id> <sid> Delete load address", CConfig::CfgDeleteLoad},
	{"DisableLoad", "<id> <sid> disable new connections to this address", CConfig::CfgDisableLoad},
	{"EnableLoad", "<id> <sid> enable new connections to this address", CConfig::CfgEnableLoad},
	{"ClearLoad", "<id> <sid> close all connections to this address", CConfig::CfgClearLoad},
	{"WeightLoad", "<id> <sid> <value(1 to 999)> change weight connection", CConfig::CfgWeightLoad},
	{"", NULL, NULL},
#endif

	{"AddACL", "<id> <addr1[,<addr2>,...]> Add Access Control List (if none all accesses are enabled)", CConfig::CfgAddACL},
	{"DelACL", "<id> <sid> Delete Access Control List", CConfig::CfgDeleteACL},
	{"ClearACL", "<id> Drop all specified Access Control List", CConfig::CfgClearACL},
	{"ListACL", "<id>|<*> List specified Access Control List or all", CConfig::CfgListACL},
	{"ClrACLCnt", "<id> Clear Access Control List counter (ACLD counter)", CConfig::CfgClearACLCounter},
	{"", NULL, NULL},

	{"ClearCon", "<id> <sid> Clear connection", CConfig::CfgClearConnection},
	{"", NULL, NULL},

	{"LogMode", "Get current log mode", CConfig::CfgLogMode},
	{"SetLogMode", "Set log mode <logmode> [0:all, 1:client only, 2:server only] <logmode>=0,1,2,3 or 4", CConfig::CfgSetLogMode},
	{"SetLogColCount", "Set log col count <colcount> [break]", CConfig::CfgSetLogColCount},
	{"SetDumpLogFileFormat", "Set dump log file format <format> (" DUMP_LOG_FILE_FORMAT_TOKENS ")", CConfig::CfgSetLogColCount},
	{"", NULL, NULL},
	
	{"SetExecOnAccept", "Set 'ExecOnAccept' <exec_on_accept_script>", CConfig::CfgSetExecOnAccept},
	{"", NULL, NULL},


	//{"Dump", "<id> <sid> <filename|stop> Dump frames in a log file", CConfig::CfgDumpFrames},
	//{"Log", "<id> <sid> Log exchanges in log file", CConfig::CfgLogExchanges},
	{"Connect", "<host:port> [addrbind:[portbind]] try connection to specified host:port", CConfig::CfgConnect},
	{"ComList", "list all serial interfaces on this computer", CConfig::CfgSerialComList},
	{"IntList", "List all ip interfaces on this computer", CConfig::CfgInterfaceList},
	{"", NULL, NULL},
	{"Config", "Display configuration informations", CConfig::CfgConfig},
	{"Ver", "Version (Display version informations)", CConfig::CfgVer},
	{"Help", "Help (Display this screen)", CConfig::CfgHelp},
	{"Quit", "Quit NATSvc config", CConfig::CfgQuit},

	//	Commandes cachees
	{"com", NULL , CConfig::CfgSerialComList},
	{"int", NULL , CConfig::CfgInterfaceList},
	{"", NULL, NULL},
	{"L", NULL, CConfig::CfgList},
	{"DelConf", NULL, CConfig::CfgDeleteConf},
	{"EnConf", NULL, CConfig::CfgEnableConf},
	{"DisConf", NULL, CConfig::CfgDisableConf},
	{"ForceSave", NULL, CConfig::CfgForceSave},
#ifdef _NATLOAD
	{"DelLoad", NULL, CConfig::CfgDeleteLoad},
	{"DisLoad", NULL, CConfig::CfgDisableLoad},
	{"EnLoad", NULL, CConfig::CfgEnableLoad},
#endif
	{"Conf", NULL, CConfig::CfgConfig},
	{"H", NULL, CConfig::CfgHelp},
	{"?", NULL, CConfig::CfgHelp},
	{"Q", NULL, CConfig::CfgQuit},
	{"exit", NULL, CConfig::CfgQuit},
};


void CConfig::SockPrintf(CTelnetServer& sock, LPCTSTR szFormat, ...)
{
	va_list vaArgList;
	static char szBuffer[2000];

	va_start(vaArgList, szFormat);
	vsprintf_s(szBuffer, sizeof(szBuffer), szFormat, vaArgList);
	sock.Send(szBuffer, (int)strlen(szBuffer));
	va_end (vaArgList);
}

void CConfig::SockWrite(CTelnetServer& sock, const BYTE * datas, int nLenBuf)
{
	sock.Send(datas, nLenBuf);
}

void CConfig::SockWrite(CTelnetServer& sock, LPCTSTR datas, int nLenBuf)
{
	sock.Send(datas, nLenBuf);
}

BOOL CConfig::CheckBadParameters(CCmdParam& cmdparam, int nMin, int nMax)	
{
#ifdef _DEBUG
	SockPrintf(cmdparam.m_sock, "ArgCount:%d min:%d max:%d\r\n", cmdparam.m_args.GetArgCount(), nMin, nMax);
#endif
	if (cmdparam.m_args.GetArgCount() < nMin || cmdparam.m_args.GetArgCount() > nMax)
	{ 
		CString sMsg;
		if (cmdparam.m_args.GetArgCount() < nMin)
		{	
			sMsg.Format("!Error: too few arguments (%d<%d)", cmdparam.m_args.GetArgCount(), nMin);
		}
		else
		{
			sMsg.Format("!Error: too many arguments (%d>%d)", cmdparam.m_args.GetArgCount(), nMax);
		}
		SockPrintf(cmdparam.m_sock, COLOR_RED "%s\r\n" COLOR_DEFAULT, sMsg);
		SockPrintf(cmdparam.m_sock, "Usage: %s %s\r\n", cmdparam.m_szCommand, cmdparam.m_szHelp?cmdparam.m_szHelp:"[None]");
		return false;
	}
	return TRUE;
}

BOOL CConfig::ConfirmAction(CTelnetServer& sock, int nId, LPCTSTR szAction)
{
	SockPrintf(sock, "Config Id:%d   Config Name:%s (listen:%s:%s)\r\n", nId, g_nat[nId].m_sConfigName, g_nat[nId].m_sAddrListen, g_nat[nId].m_sPortListen);
	SockPrintf(sock, "Are you sure you want to %s (Y/N) ?", szAction);
	char cChar;
	int nLenRead;
	for(;;)
	{
		nLenRead = sock.Receive((BYTE *)&cChar, 1);
		if (nLenRead < 0)
		{
			SockPrintf(sock, COLOR_ERROR "Confirm action %s\n" COLOR_DEFAULT, sock.GetLastErrorString());
			return FALSE;
		}
		if (nLenRead > 0)
		{
			SockPrintf(sock, "\r\n");
			return ((cChar == 'Y') || (cChar == 'y'));
		}
	}
	return FALSE;
}

BOOL CConfig::CfgQuit(CCmdParam& cmdparam)
{
	SockPrintf(cmdparam.m_sock, "End of configuration connection.\r\n");
#ifdef _DEBUG
	//*g_pfStopAll = TRUE;
#endif
	return TRUE;
}

BOOL CConfig::CfgLogMode(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 0, 0)) return FALSE;
	SockPrintf(cmdparam.m_sock, "Dump Log File Format: %s\r\n", g_sDumpLogFileFormat);
	SockPrintf(cmdparam.m_sock, "Current log mode: %d / %d\r\n", g_dwLogMode, g_dwLogModeCS);
	SockPrintf(cmdparam.m_sock, "Current log col count/break: %d / %d\r\n", g_dwLogColCount, g_dwLogBlockBreak);
	SockPrintf(cmdparam.m_sock, 
		"format mode 0 :\r\n"
		"  <date> - <title>\r\n"
		"  ****** <buffer size> ********************\r\n"
		"  AA 00 AA 00 AA ... 00 AA 00 |...........|\r\n"
		"  AA 00 AA 00 AA ... 00 AA 00 |...........|\r\n"
		"  AA 00 AA 00 AA ... 00 AA 00 |...........|\r\n"
		"  *****************************************\r\n"
		"\r\n"
		"format mode 1 :\r\n"
		"  # <date> - <title>\r\n"
		"  # ****** <buffer size> ****\r\n"
		"  AA,00,AA,00,AA,...,00,AA,00\r\n"
		"  AA,00,AA,00,AA,...,00,AA,00\r\n"
		"  AA,00,AA,00,AA,...,00,AA,00\r\n"
		"  #**************************\r\n"
		"\r\n"
		"format mode 2 :\r\n"
		"  // <date> - <title>\r\n"
		"  // ****** <buffer size> *****************************\r\n"
		"  BYTE buf_<title>[] = {\r\n"
		"  <tab>0xAA,0x00,0xAA,0x00,0xAA,...,0x00,0xAA,0x00 // |..............|\r\n"
		"  <tab>0xAA,0x00,0xAA,0x00,0xAA,...,0x00,0xAA,0x00 // |..............|\r\n"
		"  <tab>0xAA,0x00,0xAA,0x00,0xAA,...,0x00,0xAA,0x00 // |..............|\r\n"
		"  <tab>};\r\n"
		"  // **************************************************\r\n"
		"  CLIENT_OUT || SERVER_OUT(buf_<title>, sizeof(buf_<title>));\r\n"
		"\r\n"
		"format mode 3 :\r\n"
		"<binary datas>\r\n"
		"<binary datas>\r\n"
		"<binary datas>\r\n"
		"\r\n"
		"format mode 4 :\r\n"
		"==>ClientOut_1<==\r\n"
		"<binary datas>\r\n"
		"==>ServerOut_1<==\r\n"
		"<binary datas>\r\n"
		"==>ServerOut_2<==\r\n"
		"<binary datas>\r\n"
		"\r\n"
		"format mode 5 :\r\n"
		"SIZE <hex datas>                       #  xx xx xx xx xx xx xx xx  xx xx xx xx xx xx xx xx  'cccccccc cccccccc'\r\n"
		"0010 AA00AA00AA00AA00AA00AA00AA00AA00  #  AA 00 AA 00 AA 00 AA 00  AA 00 AA 00 AA 00 AA 00  '........ ........'\r\n"
		"0007 AA00AA00AA00AA                    #  AA 00 AA 00 AA 00 AA                              '.......          '\r\n"
		"etc...\r\n"
		"\r\n"
		);
	return FALSE;
}

BOOL CConfig::CfgSetLogMode(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 2)) return FALSE;
	LoadOptions();
	_SetLogMode(atoi(cmdparam.m_args[0]));
	_SetLogModeCS(0);
	if (cmdparam.m_args.GetArgCount() > 1)
	{
		_SetLogModeCS(atoi(cmdparam.m_args[1]));
	}
	SockPrintf(cmdparam.m_sock, "Setting log mode: %d [C/S:%d] ...\r\n", g_dwLogMode, g_dwLogModeCS);
	SaveOptions();
	return FALSE;
}

BOOL CConfig::CfgSetLogColCount(CCmdParam& cmdparam)
{
	if (!CheckBadParameters(cmdparam, 1, 2)) return FALSE;
	LoadOptions();
	_SetLogColCount(atoi(cmdparam.m_args[0]));
	if (cmdparam.m_args[1] != "")
	{
		_SetLogBlockBreak(atoi(cmdparam.m_args[1]));
	}
	SockPrintf(cmdparam.m_sock, "Setting log col count: %d [break:%d] ...\r\n", g_dwLogColCount, g_dwLogBlockBreak);
	SaveOptions();
	return FALSE;
}

BOOL CConfig::CfgSetExecOnAccept(CCmdParam& cmdparam)
{
	if (!CheckBadParameters(cmdparam, 1, 1)) return FALSE;

	LoadOptions();
	_SetExecOnAccept(cmdparam.m_args[0]);
	SockPrintf(cmdparam.m_sock, "Setting ExecOnAccept: '%s' [%s]...\r\n", g_sExecOnAccept, g_sExecOnAcceptRunTime);
	SaveOptions();
	return FALSE;
}

void CConfig::DisplayNoFormat(CTelnetServer& sock)
{
	for(int nID1 = 0; nID1 < sizeof(g_nat)/sizeof(g_nat[0]); nID1++)
	{
		if (g_nat[nID1].IsUsed())
		{
			SockPrintf(sock, "\"%s\" ", g_nat[nID1].m_sConfigName);
			SockPrintf(sock, "%s:%s ", g_nat[nID1].m_sAddrListen, g_nat[nID1].m_sPortListen);
#if defined _NATLOAD
			CStringArray sLoadAddr;
			for(int nID2=0; nID2<MAX_LOAD_ADDR; nID2++)
				if (g_nat[nID1].m_loadaddr[nID2].IsUsed())
					sLoadAddr.Add(g_nat[nID1].m_loadaddr[nID2].m_sAddrConnect);
			for(int i=0; i<sLoadAddr.GetSize(); i++)
			{
				SockPrintf(sock, "%s", sLoadAddr[i]);
				if (i < (sLoadAddr.GetSize()-1)) SockPrintf(sock, ",");
			}
#elif defined _NAT
			SockPrintf(sock, ":%s ", g_nat[nID1].m_sPortConnect);
			SockPrintf(sock, "%s:%s ", g_nat[nID1].m_sAddrConnect, g_nat[nID1].m_sPortConnect);
#else
			PP_ERROR,
#endif
			if (g_nat[nID1].m_sAddrOut != "")
				SockPrintf(sock, "%s", g_nat[nID1].m_sAddrOut);
			SockPrintf(sock, "\r\n");
		}
	}
}

void CConfig::ListDisplay(CTelnetServer& sock, int * ListID, int nCountID)
{
	CSockAddr addr;
	CSockPort port;
	BOOL fEven = FALSE;
	int nID;
	DWORD dwTotalAll = 0;
	DWORD dwTotalTotConAll = 0;
	for(int nIndex=0; nIndex<nCountID; nIndex++)
	{
#ifdef _NATLOAD			
		DWORD dwTotal = 0;
		DWORD dwTotalTotCon = 0;
#endif
		nID = ListID[nIndex];
		if (g_nat[nID].IsUsed())
		{
#ifdef _DEBUG
			printf("Name:%s\n", (LPCTSTR)g_nat[nID].m_sConfigName);
#endif
			SockPrintf(sock, COLOR_BRIGHTNESS);
			if (fEven) 
				SockPrintf(sock, COLOR_YELLOW);
			else 
				SockPrintf(sock, COLOR_GRAY);
			fEven = !fEven;
			g_nat[nID].m_sockListen.GetSockName(addr, port);
#if defined _NATLOAD
			SockPrintf(sock, "Id |SId|Con  |TCon |Wei| Config Name                   | Listening on        | Connect to (NAT)    | Interface Out |Delay|BufSiz|ACLD|A|E|S|P|G|L|U\r\n"); 
			SockPrintf(sock, "===================================================================================================================================================\r\n");
			SockPrintf(sock, "%3d|%-3s|%-5s|%-5s|%-3s|%-31s|%15s:%-5s|%-21s|%15s|%5d|%6u|%4d|%c|%c|%c|%c|%c|%c|%c\r\n",
				nID, "", "", "", "", 
				g_nat[nID].m_sConfigName,  
				g_nat[nID].m_sAddrListen, g_nat[nID].m_sPortListen, 
				"",
				g_nat[nID].m_sAddrOut==""?"[Default]":g_nat[nID].m_sAddrOut,
				g_nat[nID].m_nCommDelay,
				g_nat[nID].m_dwBufferSize,
				g_nat[nID].m_nACLDropped,
				g_nat[nID].IsACL()?'*':' ',
				g_nat[nID].IsEnabled()?'*':' ',
				g_nat[nID].IsStarted()?'*':' ',
				g_nat[nID].IsBindPrivatePort()?'*':' ',
				g_nat[nID].IsLogged()?'*':' ',
				' ',
				' ');
#elif defined _NAT
			SockPrintf(sock, "%3d|%5d|%5d|%-31s|%15s:%-5s|%15s:%-5s|%15s|%5d|%6u|%4d|%c|%c|%c|%c|%c\r\n", 
				nID,  g_nat[nID].GetActiveConnectionsCount(), g_nat[nID].GetTotalConnectionsCount(),
				g_nat[nID].m_sConfigName, 
				g_nat[nID].m_sAddrListen, g_nat[nID].m_sPortListen, 
				IsCommPort(g_nat[nID].m_sAddrConnect)?g_nat[nID].m_sPortConnect:g_nat[nID].m_sAddrConnect, IsCommPort(g_nat[nID].m_sAddrConnect)?g_nat[nID].m_sAddrConnect:g_nat[nID].m_sPortConnect,
				g_nat[nID].m_sAddrOut==""?"[Default]":g_nat[nID].m_sAddrOut,
				g_nat[nID].m_nCommDelay,
				g_nat[nID].m_dwBufferSize,
				g_nat[nID].m_nACLDropped,
				g_nat[nID].IsACL()?'*':' ',
				g_nat[nID].IsEnabled()?'*':' ',
				g_nat[nID].IsStarted()?'*':' ',
				g_nat[nID].IsBindPrivatePort()?'*':' ',
				g_nat[nID].IsLogged()?'*':' ');

				dwTotalAll += g_nat[nID].GetActiveConnectionsCount();
				dwTotalTotConAll += g_nat[nID].GetTotalConnectionsCount();
#else
			PP_ERROR,
#endif
			
#ifdef _NATLOAD
			for(int j=0; j<MAX_LOAD_ADDR; j++)
			{
				if (g_nat[nID].m_loadaddr[j].m_sAddrConnect != "")
				{
					SockPrintf(sock, "%-3s|%3d|%5d|%5d|%3d|%-31s|%15s %-5s|%15s:%-5s|%15s|%4s             |%c|%c|%c|%c|%c|%c|%c\r\n",
						"", j, g_nat[nID].GetActiveConnectionsCount(j), g_nat[nID].GetTotalConnectionsCount(j),
						g_nat[nID].m_loadaddr[j].m_wWeight,
						"",
						"", "", 
						g_nat[nID].m_loadaddr[j].m_sAddrConnect, 
						g_nat[nID].m_loadaddr[j].m_sPortConnect,
						"",
						"",
						' ',
						' ',
						' ',
						' ',
						' ',
						g_nat[nID].m_loadaddr[j].m_fActivated?'*':' ',
						g_nat[nID].m_loadaddr[j].m_fListenUp?'*':' ');
					dwTotal += g_nat[nID].GetActiveConnectionsCount(j);
					dwTotalTotCon += g_nat[nID].GetTotalConnectionsCount(j);
				}
			}
			SockPrintf(sock, "        ----- -----\r\n");
			SockPrintf(sock, "TotCon  %5d %5d\r\n", dwTotal, dwTotalTotCon);
			SockPrintf(sock, "---------------------------------------------------------------------------------------------------------------------------------------------------\r\n");
			dwTotalAll += dwTotal;
			dwTotalTotConAll += dwTotalTotCon;
#endif
		}
	}
	SockPrintf(sock, COLOR_BRIGHTNESS "" COLOR_GRAY);
#if defined _NATLOAD
	SockPrintf(sock, "===================================================================================================================================================\r\n");
	SockPrintf(sock, COLOR_BRIGHTNESS "" COLOR_GREEN "        %5d %5d\r\n", dwTotalAll, dwTotalTotConAll);
#else 
	SockPrintf(sock, "=======================================================================================================================================\r\n");
	SockPrintf(sock, COLOR_BRIGHTNESS "" COLOR_GREEN "    %5d %5d\r\n", dwTotalAll, dwTotalTotConAll);
#endif
}

int SortID(int nID1, int nID2)
{
	return -1;
}

int SortName(LPCTSTR szName1, LPCTSTR szName2)
{
	return strcmp(szName1, szName2);
}

int CConfig::ListSort(CTelnetServer& sock, int * ListID, int nSortType)
{
	int nCount = 0;
	int nRet;
	for(int nID1 = 0; nID1 < sizeof(g_nat)/sizeof(g_nat[0]); nID1++)
	{
		if (g_nat[nID1].IsUsed())
		{
#ifdef _DEBUG
			//SockPrintf(sock, "------------------------\r\n");
			//SockPrintf(sock, "SortName:%s\r\n", g_nat[nID1].m_sConfigName);
#endif
			if (nCount == 0)
			{
				//SockPrintf(sock, "Count==0 id=%d\r\n", nID1);
				ListID[nCount++] = nID1;
			}
			else
			{
				//SockPrintf(sock, "Count!=0 id=%d\r\n", nID1);
				int nID2;
				for(int j=0;; j++)
				{
					if (j == nCount)
					{
						//SockPrintf(sock, "j = nCount\r\n");
						ListID[nCount] = nID1;
						nCount++;
						break;
					}
					nID2 = ListID[j];
					switch(nSortType)
					{
					case ST_NAME:
						nRet = _tcsicmp(g_nat[nID1].m_sConfigName, g_nat[nID2].m_sConfigName);
						break;
					case ST_LOCADDR:
						nRet = _tcsicmp(g_nat[nID1].m_sAddrListen, g_nat[nID2].m_sAddrListen);
						if (nRet == 0)
						{
							nRet = _tcsicmp(g_nat[nID1].m_sPortListen, g_nat[nID2].m_sPortListen);
						}
						break;
					case ST_CCON:
#if defined _NATLOAD
						nRet = g_nat[nID1].GetAllActiveConnectionsCount() - g_nat[nID2].GetAllActiveConnectionsCount();
#elif defined _NAT
						nRet = g_nat[nID1].GetActiveConnectionsCount() - g_nat[nID2].GetActiveConnectionsCount();
#else
	PP_ERROR,
#endif
						break;
					case ST_TCON:
#if defined _NATLOAD
						nRet = g_nat[nID1].GetAllTotalConnectionsCount() - g_nat[nID2].GetAllTotalConnectionsCount();
#elif defined _NAT
						nRet = g_nat[nID1].GetTotalConnectionsCount() - g_nat[nID2].GetTotalConnectionsCount();
#else
	PP_ERROR,
#endif
						break;
					case ST_ID:
						nRet = nID1 - nID2;
					default:
						break;
					}
					if (nRet < 0)
					{
						//SockPrintf(sock, "ret<0 %s<%s \r\n", g_nat[nID1].m_sAddrListen, g_nat[nID2].m_sAddrListen);
						for(int o=nCount-1; o>=j; o--)
						{
							//SockPrintf(sock, "move %d --> %d\r\n", o, o+1);
							ListID[o+1] = ListID[o];
						}
						ListID[j] = nID1;
						nCount++;
						break;
					}
				}
			}
		}
	}
	return nCount;
}

BOOL CConfig::ListPeer(CTelnetServer& sock, int nId)
{
	CHECK_ID2(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(sock, COLOR_ERROR "!Error: Bad id %d" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].m_sockListen.IsSocket())
	{
		SockPrintf(sock, COLOR_ERROR "!Error: configuration not started for id %d" COLOR_DEFAULT, nId);
		return FALSE;
	}
	SockPrintf(sock, "Connected sockets for listening config \"%s\" (id %d)\r\n", g_nat[nId].m_sConfigName, nId);
#if defined _NATLAOD
	SockPrintf(sock, "Current connections:%d, Total connections:%d, listening on:%s.%s, interface out:%s, delay:%d, bufsize:%u\r\n",
		g_nat[nId].GetAllActiveConnectionsCount(), g_nat[nId].GetAllTotalConnectionsCount(), 
		g_nat[nId].m_sAddrListen, g_nat[nId].m_sPortListen, g_nat[nId].m_sAddrOut==""?"[Default]":g_nat[nId].m_sAddrOut,
		g_nat[nId].m_nCommDelay, g_nat[nId].m_dwBufferSize);
#elif defined _NAT
	SockPrintf(sock, "Current connections:%d, Total connections:%d, listening on:%s.%s, interface out:%s, delay:%d, bufsize:%u\r\n",
		g_nat[nId].GetActiveConnectionsCount(), g_nat[nId].GetTotalConnectionsCount(), 
		g_nat[nId].m_sAddrListen, g_nat[nId].m_sPortListen, g_nat[nId].m_sAddrOut==""?"[Default]":g_nat[nId].m_sAddrOut,
		g_nat[nId].m_nCommDelay, g_nat[nId].m_dwBufferSize);
#endif
	SockPrintf(sock, "--------------------------------------------------------------------------------------------------------------------------------------\r\n");
	SockPrintf(sock, "Id |AId| In Peer             | In local            | FramesIn |Kb/s    | Out local           | Out Peer            |FramesOut |Kb/s    |E|F\r\n");
	SockPrintf(sock, "--------------------------------------------------------------------------------------------------------------------------------------\r\n");
	for(int sAId=0; sAId<MAX_CONNECT; sAId++)
	{
		if (g_nat[nId].IsConnected(sAId))
		{
			CDateRef drStart(g_nat[nId].m_sAccept[sAId].m_stStartTime), drCur;
			drCur.SetCurrentLocalTime();
			double dCoeff = (drCur-drStart>0?drCur-drStart:1) * 1000;
			CSockAddr addrSockAccept, addrPeerAccept, addrSockConnect, addrPeerConnect;
			CSockPort portSockAccept, portPeerAccept, portSockConnect, portPeerConnect;
			g_nat[nId].m_sAccept[sAId].m_psockAccept->GetSockName(addrSockAccept, portSockAccept);
			g_nat[nId].m_sAccept[sAId].m_psockAccept->GetPeerName(addrPeerAccept, portPeerAccept);
			g_nat[nId].m_sAccept[sAId].m_sockConnect.GetSockName(addrSockConnect, portSockConnect);
			g_nat[nId].m_sAccept[sAId].m_sockConnect.GetPeerName(addrPeerConnect, portPeerConnect);

			char szSpeed1[30], szSpeed2[30];
			sprintf_s(szSpeed1, sizeof(szSpeed1), "%0.3lf", g_nat[nId].m_sAccept[sAId].m_dwClientTotalBytesIn/dCoeff);
			sprintf_s(szSpeed2, sizeof(szSpeed2), "%0.3lf", g_nat[nId].m_sAccept[sAId].m_dwServerTotalBytesIn/dCoeff);
			SockPrintf(sock, "%3d|%3d|%15s:%-5d|%15s:%-5d|%10u|%8s|%15s:%-5d|%15s:%-5d|%10u|%8s|%c|%c %s\r\n",
				nId, sAId, 
				addrPeerAccept.String(), portPeerAccept.HostNumber(), addrSockAccept.String(), portSockAccept.HostNumber(),
				g_nat[nId].m_sAccept[sAId].m_dwFramesIn,
				szSpeed1,
				addrSockConnect.String(), portSockConnect.HostNumber(), addrPeerConnect.String(), portPeerConnect.HostNumber(),
				g_nat[nId].m_sAccept[sAId].m_dwFramesOut,
				szSpeed2,
				g_nat[nId].m_sAccept[sAId].m_fLogExchanges?'Y':'N',
				g_nat[nId].m_sAccept[sAId].m_fDumpFrames?'Y':'N',
				g_nat[nId].m_sAccept[sAId].m_fDumpFrames?g_nat[nId].m_sAccept[sAId].m_sLogFile:"[None]"
				);
		}
	}
	SockPrintf(sock, "\r\nFlags: E=Log Exchanges, F=Dump Frames\r\n");
	return TRUE;
}

BOOL CConfig::CfgList(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 0, 1)) return FALSE;
	int nArgCount = cmdparam.m_args.GetArgCount();
	int nSortType = ST_ID;
	if (nArgCount > 0)
	{
		if (! _tcsicmp(cmdparam.m_args[0], "id")) 
			nSortType = ST_ID;
		else if (! _tcsicmp(cmdparam.m_args[0], "name")) 
			nSortType = ST_NAME;
		else if (! _tcsicmp(cmdparam.m_args[0], "locaddr")) 
			nSortType = ST_LOCADDR;
		else if (! _tcsicmp(cmdparam.m_args[0], "ccon")) 
			nSortType = ST_CCON;
		else if (! _tcsicmp(cmdparam.m_args[0], "tcon")) 
			nSortType = ST_TCON;
		else if (! _tcsicmp(cmdparam.m_args[0], "noformat") || !_tcsicmp(cmdparam.m_args[0], "nof"))
		{
			DisplayNoFormat(cmdparam.m_sock);
			return FALSE;
		}
		else 
		{
			ListPeer(cmdparam.m_sock, atoi(cmdparam.m_args[0]));
			return FALSE;
		}
	}
	
	SockPrintf(cmdparam.m_sock, "Listening sockets:\r\n"
		                        "------------------\r\n\r\n");
#if defined _NATLOAD
	//SockPrintf(cmdparam.m_sock, "Id |SId|Con  |TCon |Wei| Config Name                   | Listening on        | Connect to (NAT)    | Interface Out |Delay|ACLD|A|E|S|P|G|L|U\r\n"); 
	//SockPrintf(cmdparam.m_sock, "============================================================================================================================================\r\n");
#elif defined _NAT
	//SockPrintf(cmdparam.m_sock, COLOR_BRIGHTNESS);
	SockPrintf(cmdparam.m_sock, "Id |Con  |TCon | Config Name                   | Listening on        | Connect to (NAT)    | Interface Out |Delay|BufSiz|ACLD|A|E|S|P|G\r\n"); 
	SockPrintf(cmdparam.m_sock, "=======================================================================================================================================\r\n");
#else
	PP_ERROR,
#endif
	int nListID[MAX_CONFIG];
	int nCount = 0;
	nCount = ListSort(cmdparam.m_sock, nListID, nSortType);
	ListDisplay(cmdparam.m_sock, nListID, nCount);
	SockPrintf(cmdparam.m_sock, COLOR_DEFAULT);
	SockPrintf(cmdparam.m_sock, "\r\n");
	SockPrintf(cmdparam.m_sock, "Flags: A=ACL exists\r\n");
	SockPrintf(cmdparam.m_sock, "       E=Configuration enabled\r\n");
	SockPrintf(cmdparam.m_sock, "       S=Configuration started\r\n");
	SockPrintf(cmdparam.m_sock, "       P=Bind private port\r\n");
	SockPrintf(cmdparam.m_sock, "       G=Configuration logged\r\n");
#if defined _NATLOAD
	SockPrintf(cmdparam.m_sock, "       L=Load address enabled\r\n");
	SockPrintf(cmdparam.m_sock, "       U=Load address up\r\n");
#endif
	SockPrintf(cmdparam.m_sock, "ACLD : ACL addresses dropped\r\n");
	return FALSE;
}

BOOL CConfig::CfgStartConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	if (cmdparam.m_args[0] == "*")
	{
		SockPrintf(cmdparam.m_sock, "Starting all configurations...\r\n");
		for(int nId=0; nId<sizeof(g_nat)/sizeof(g_nat[0]); nId++)
		{
			if (! g_nat[nId].IsFree())
			{
				if (! g_nat[nId].IsStarted())
				{
					if (g_nat[nId].IsEnabled())
					{
						if (ConfirmAction(cmdparam.m_sock, nId, "start"))
						{
							CString sError;
							if (g_nat[nId].Start(sError))
								SockPrintf(cmdparam.m_sock, "Configuration %d started.\r\n", nId);
							else
								SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to start configuration %d (%s).\r\n" COLOR_DEFAULT, nId, sError);
						}
					}	
				}
			}
		}
		return FALSE;
	}
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsStarted())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already started.\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].IsEnabled())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d not enable.\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	CString sError;
	if (! g_nat[nId].Start(sError))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to start configuration %d (%s).\r\n" COLOR_DEFAULT, nId, sError);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "configuration %d started", nId);
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgStopConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	if (cmdparam.m_args[0] == "*")
	{
		SockPrintf(cmdparam.m_sock, "Stopping all configurations...\r\n");
		for(int nId=0; nId<sizeof(g_nat)/sizeof(g_nat[0]); nId++)
		{
#ifdef _DEBUG
			SockPrintf(cmdparam.m_sock, "Conf:%d Free:%d Started:%d Enabled:%d\r\n", nId, g_nat[nId].IsFree(), g_nat[nId].IsStarted(), g_nat[nId].IsEnabled());
#endif
			if (! g_nat[nId].IsFree())
			{
				if (g_nat[nId].IsStarted())
				{
					if (g_nat[nId].IsEnabled())
					{
						if (ConfirmAction(cmdparam.m_sock, nId, "stop"))
						{
							CString sError;
							if (g_nat[nId].Stop(sError))
								SockPrintf(cmdparam.m_sock, "Configuration %d stopped.\r\n", nId);
							else
								SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to stop configuration %d (%s)." COLOR_DEFAULT, nId, sError);
						}
					}	
				}
			}
		}
		//	Fin
		return FALSE;
	}
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].IsEnabled())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d not enable" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].IsStarted())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d not started" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "stop"))
	{
		CString sError;
		if (g_nat[nId].Stop(sError))
			SockPrintf(cmdparam.m_sock, "configuration %d stopped.", nId);
		else
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to stop configuration %d (%s)." COLOR_DEFAULT, nId, sError);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgEnableConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsEnabled())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already enabled" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "enable"))
	{
		g_nat[nId].Enable(TRUE);
		SockPrintf(cmdparam.m_sock, "configuration %d enabled.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgDisableConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].IsEnabled())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already disabled" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "disable"))
	{
		g_nat[nId].Enable(FALSE);
		SockPrintf(cmdparam.m_sock, "configuration %d disabled.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgLogConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsLogged())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already logged" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "log"))
	{
		g_nat[nId].Logged(TRUE);
		SockPrintf(cmdparam.m_sock, "configuration %d logged.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgUnlogConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].IsLogged())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already unlogged" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "unlog"))
	{
		g_nat[nId].Logged(FALSE);
		SockPrintf(cmdparam.m_sock, "configuration %d unlogged.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgBindPrivPortConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsBindPrivatePort())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already configured for bind private port" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "bind to private port"))
	{
		g_nat[nId].BindPrivatePort(TRUE);
		SockPrintf(cmdparam.m_sock, "configuration %d will bind to private port.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgUnbindPrivPortConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (! g_nat[nId].IsBindPrivatePort())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: configuration %d already configured for not bind private port" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "do not bind to private port"))
	{
		g_nat[nId].BindPrivatePort(FALSE);
		SockPrintf(cmdparam.m_sock, "configuration %d won't bind to private port.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgClearACLCounter(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	CString sAddr = cmdparam.m_args[1];
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
	SockPrintf(cmdparam.m_sock, "Clearing ACL counter...\r\n");
	g_nat[nId].m_nACLDropped = 0;
	SockPrintf(cmdparam.m_sock, "ACL counter cleared\r\n");
	return FALSE;
}

BOOL CConfig::CfgConnectionsCounter(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	CString sAddr = cmdparam.m_args[1];
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
	SockPrintf(cmdparam.m_sock, "Clearing connections counter...\r\n");
	g_nat[nId].ClearTotalConnectionsCount();
	SockPrintf(cmdparam.m_sock, "Connections counter cleared\r\n");
	return FALSE;
}

BOOL CConfig::CfgClearACL(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	CString sAddr = cmdparam.m_args[1];
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
	if (ConfirmAction(cmdparam.m_sock, nId, "drop all ACLs"))
	{
		SockPrintf(cmdparam.m_sock, "Dopping all ACL (id:%d)...\r\n", nId);
		g_nat[nId].ClearACL();
		SockPrintf(cmdparam.m_sock, "configuration %d all ACLs dropped.", nId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgAddACL(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	CString sAddr = cmdparam.m_args[1];
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
	SockPrintf(cmdparam.m_sock, "Adding ACL (id:%d) %s...\r\n", nId, sAddr);
	CFields fld(sAddr, ',');
	for(int j=0; j<fld.GetSize(); j++)
	{
		if (! g_nat[nId].AddACL(fld.GetAt(j)))
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to add ACL adresse %s (maybe already exists)\r\n" COLOR_DEFAULT, fld.GetAt(j));
		}
		else
		{
			SockPrintf(cmdparam.m_sock, "ACL (id:%d) %s added.\r\n", nId, fld.GetAt(j));
		}
	}
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgDeleteACL(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	CString sAddr = cmdparam.m_args[1];
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
	SockPrintf(cmdparam.m_sock, "Deleting ACL (id:%d) %s...\r\n", nId, sAddr);
	CFields fld(sAddr, ',');
	for(int j=0; j<fld.GetSize(); j++)
	{
		int nSId = atoi(fld.GetAt(j));
		if (! g_nat[nId].DeleteACL(nSId))
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to delete ACL adresse SID:%d (maybe does not exist)\r\n" COLOR_DEFAULT, nSId);
		}
		else
		{
			SockPrintf(cmdparam.m_sock, "ACL (id:%d) SID:%d deleted.\r\n", nId, nSId);
		}
	}
	SaveNatConfigurations();
	return FALSE;
}

void CConfig::ListOneACL(CTelnetServer& sock, int nId)
{
	if (g_nat[nId].m_saACL.GetSize() == 0)
	{
		SockPrintf(sock, "%3d| No ACL for this configuration (all addresses are enabled).\r\n", nId);
	}
	else
	{
		for(int i=0; i<g_nat[nId].m_saACL.GetSize(); i++)
		{
			SockPrintf(sock, "%3d|%3d|%s\r\n", nId, i, g_nat[nId].m_saACL[i]);
		}
	}
}

BOOL CConfig::CfgListACL(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	CString sAddr = cmdparam.m_args[0];
	if (sAddr == "*")
	{
		SockPrintf(cmdparam.m_sock, "ID |SID|Addresses             \r\n"); 
		SockPrintf(cmdparam.m_sock, "==============================\r\n");
		for(int sAId=0; sAId<sizeof(g_nat)/sizeof(g_nat[0]); sAId++)
		{
			if (g_nat[sAId].IsUsed())
			{	
				ListOneACL(cmdparam.m_sock, sAId);
			}
		}
	}
	else
	{
		int nId = atoi(cmdparam.m_args[0]);
		CHECK_ID(nId);
		if (nId >= MAX_CONFIG)
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
			return FALSE;
		}
		if (g_nat[nId].IsFree())
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
			return FALSE;
		}
		SockPrintf(cmdparam.m_sock, "Listing ACL (id:%d)...\r\n", nId);
		SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
		SockPrintf(cmdparam.m_sock, "ID |SID|Addresses (%d dropped)\r\n", g_nat[nId].m_nACLDropped); 
		SockPrintf(cmdparam.m_sock, "==============================\r\n");
		ListOneACL(cmdparam.m_sock, nId);
	}
	return FALSE;
}

BOOL CConfig::CfgClearConnection(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	CString sAddr = cmdparam.m_args[1];
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	int nAId = atoi(cmdparam.m_args[1]);
	if (! g_nat[nId].IsConnected(nAId))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid connection sub id\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "close this connection"))
	{
		g_nat[nId].CloseConnection(nAId);
		SockPrintf(cmdparam.m_sock, "connection closed.");
	}
	return FALSE;
}

#if defined _NATLOAD
BOOL CConfig::CfgAddLoad(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (nId >= MAX_CONFIG)
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Invalid config ID\r\n" COLOR_DEFAULT);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", g_nat[nId].m_sConfigName);
	
	CString sAddrPortConnect = cmdparam.m_args[1]; 
	SockPrintf(cmdparam.m_sock, "Adding Load addresses (id:%d) %s...\r\n", nId, sAddrPortConnect);
	CFields fld(sAddrPortConnect, ',');
	for(int j=0; j<fld.GetSize(); j++)
	{
		CFields param(fld[j], ':');
		if (param[1] == "")
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Bad syntax for this command.\r\n" COLOR_DEFAULT);
			return false;
		}
	}

	for(int j=0; j<fld.GetSize(); j++)
	{
		CFields param(fld[j], ':');
		SockPrintf(cmdparam.m_sock, "Adding Load address (id:%d) %s...\r\n", nId, fld[j]);
		CString sText;
		sText.Format("Enable load address %s", fld[j]);
		BOOL fEnable = false;
		if (ConfirmAction(cmdparam.m_sock, nId, sText))
		{
			fEnable = true;
		}
		g_nat[nId].AddLoadAddress(param[0], param[1], fEnable, 1);
	}
	
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgDeleteLoad(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	int nSubId = atoi(cmdparam.m_args[1]);
	CHECK_SID(nSubId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: not a configuration %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	CString sText;
	sText.Format("delete load address %s", g_nat[nId].m_loadaddr[nSubId].m_sAddrConnect);
	if (ConfirmAction(cmdparam.m_sock, nId, sText))
	{
		if (g_nat[nId].GetLoadAddressCount() < 2)
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Can't delete last load adress\r\n" COLOR_DEFAULT);
		}
		else
		{
			g_nat[nId].DeleteLoad(nSubId);
			SockPrintf(cmdparam.m_sock, "Config id %d:%d deleted\r\n", nId, nSubId);
			SaveNatConfigurations();
		}
	}
	return FALSE;
}

BOOL CConfig::CfgDisableLoad(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	int nSubId = atoi(cmdparam.m_args[1]);
	CHECK_SID(nSubId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: not a configuration %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	CString sText;
	sText.Format("disable load address %s", g_nat[nId].m_loadaddr[nSubId].m_sAddrConnect);
	if (ConfirmAction(cmdparam.m_sock, nId, sText))
	{
		g_nat[nId].m_loadaddr[nSubId].m_fActivated = FALSE;
		SockPrintf(cmdparam.m_sock, "Config id %d:%d disabled\r\n", nId, nSubId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgClearLoad(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	int nSubId = atoi(cmdparam.m_args[1]);
	CHECK_SID(nSubId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: not a configuration %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	CString sText;
	sText.Format("close all connections on load address %s", g_nat[nId].m_loadaddr[nSubId].m_sAddrConnect);
	if (ConfirmAction(cmdparam.m_sock, nId, sText))
	{
		g_nat[nId].ClearConnections(nSubId);
		SockPrintf(cmdparam.m_sock, "Config id %d:%d cleared\r\n", nId, nSubId);
	}
	return FALSE;
}

BOOL CConfig::CfgEnableLoad(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	int nSubId = atoi(cmdparam.m_args[1]);
	CHECK_SID(nSubId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	CString sText;
	sText.Format("enable load address %s", g_nat[nId].m_loadaddr[nSubId].m_sAddrConnect);
	if (ConfirmAction(cmdparam.m_sock, nId, sText))
	{
		g_nat[nId].m_loadaddr[nSubId].m_fActivated = TRUE;
		SockPrintf(cmdparam.m_sock, "Config id %d:%d enabled\r\n", nId, nSubId);
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgWeightLoad(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 3, 3)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	int nSubId = atoi(cmdparam.m_args[1]);
	CHECK_SID(nSubId);
	WORD wNewWeight = atoi(cmdparam.m_args[2]);
	CHECK_WEIGHT(wNewWeight);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no configuration for id %d\r\n" COLOR_DEFAULT, nId);
	}
	else
	{
		SockPrintf(cmdparam.m_sock, "Id:%d Name:%s (listen:%s:%s)\r\n", nId, g_nat[nId].m_sConfigName, g_nat[nId].m_sAddrListen, g_nat[nId].m_sPortListen);
		SockPrintf(cmdparam.m_sock, "Config id %d:%d Old weight value:%d, new weight value: %d\r\n", nId, nSubId, g_nat[nId].m_loadaddr[nSubId].m_wWeight, wNewWeight);
		g_nat[nId].m_loadaddr[nSubId].m_wWeight = wNewWeight;
	}
	SaveNatConfigurations();
	return FALSE;
}
#endif

BOOL CConfig::CfgDelayConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	int nCommDelay = atoi(cmdparam.m_args[1]);
	SockPrintf(cmdparam.m_sock, "%s Setting communication delay : %d\r\n", g_nat[nId].m_sConfigName, nCommDelay);
	g_nat[nId].m_nCommDelay = nCommDelay;
	SockPrintf(cmdparam.m_sock, "Config id %d delayed with %d ms\r\n", nId, nCommDelay);
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgBufSizConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	DWORD dwBufSiz = atoi(cmdparam.m_args[1]);
	SockPrintf(cmdparam.m_sock, "%s Setting buffer size : %d\r\n", g_nat[nId].m_sConfigName, dwBufSiz);
	g_nat[nId].m_dwBufferSize = dwBufSiz;
	SockPrintf(cmdparam.m_sock, "Config id %d buffer size set to %u bytes\r\n", nId, dwBufSiz);
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgRenameConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Old config name:%s  New config name:%s\r\n", g_nat[nId].m_sConfigName, cmdparam.m_args[1]);
	g_nat[nId].m_sConfigName = (LPCTSTR)cmdparam.m_args[1];
	SockPrintf(cmdparam.m_sock, "Config id %d renamed.\r\n", nId);
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgAlterConfListen(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsStarted())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to alter configuration id %d while it is running\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	
	CFields fldListen(cmdparam.m_args[1], ':');
	if (! g_nat[nId].AlterConfListen(fldListen[0], fldListen[1]))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to alter configuration id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config id %d altered.\r\n", nId);
	SaveNatConfigurations();
	return FALSE;
}

#ifdef _NAT
BOOL CConfig::CfgAlterConfConnect(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsStarted())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to alter configuration id %d while it is running\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	
	CFields fldConnect(cmdparam.m_args[1], ':');
	if (! g_nat[nId].AlterConfConnect(fldConnect[0], fldConnect[1]))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to alter configuration id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config id %d altered.\r\n", nId);
	SaveNatConfigurations();
	return FALSE;
}
#endif

BOOL CConfig::CfgAlterConfAddrOut(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 2, 2)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (g_nat[nId].IsStarted())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to alter configuration id %d while it is running\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	
	if (! g_nat[nId].AlterConfAddrOut(cmdparam.m_args[1]))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: unable to alter configuration id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Config id %d altered.\r\n", nId);
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgAddConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 3, 4)) return FALSE;
	CString sAddrPortListen = cmdparam.m_args[1];
	CString sAddrPortConnect = cmdparam.m_args[2];
	//CFields fldListen(cmdparam.m_args[1], ':');
	//CFields fldConnect(cmdparam.m_args[2], ':');
	/*CString sAddrListen  = fldListen[0], 
	        sPortListen  = fldListen[1], 
			sAddrConnect = fldConnect[0], 
			sPortConnect = fldConnect[1];*/
	SockPrintf(cmdparam.m_sock, "Config Name: %s\r\n", cmdparam.m_args[0]);
#if defined _NATLOAD
	CFields fld(sAddrPortConnect, ',');
	CString sActivated, sWeight;
	for(int i=0; i<fld.GetSize(); i++)
	{
		sActivated += "Y,";
		sWeight += "1,";
	}
#endif
	for(int nId=0; ; nId++)
	{
		if (nId >= sizeof(g_nat)/sizeof(g_nat[0]))
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to add new configuration, no more free space\r\n" COLOR_DEFAULT);
			return false;
		}
		if (g_nat[nId].IsFree())
		{
#if defined _NAT
			SockPrintf(cmdparam.m_sock, "Adding NAT (id:%d) %s %s %s...\r\n", nId, sAddrPortListen, sAddrPortConnect, cmdparam.m_args[3]);
#endif
#if defined _NATLOAD
			SockPrintf(cmdparam.m_sock, "Adding NAT (id:%d) %s %s %s...\r\n", nId, sAddrPortListen, sAddrPortConnect, cmdparam.m_args[3]);
#endif
			g_nat[nId].SetConf(cmdparam.m_args[0], sAddrPortListen, sAddrPortConnect, 
#if defined _NATLOAD
				sActivated, sWeight, 
#endif
				cmdparam.m_args[3], 0, TRUE, FALSE);
			if (ConfirmAction(cmdparam.m_sock, nId, "start"))
			{
				CString sError;
				if (! g_nat[nId].Start(sError))
				{
					SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to start configuration %d (%s).\r\n" COLOR_DEFAULT, nId, sError);
				}
				else
				{
					SockPrintf(cmdparam.m_sock, "configuration %d started\r\n", nId);
				}
			}
			break;
		}
	}
	SaveNatConfigurations();
	return FALSE;
}

BOOL CConfig::CfgDeleteConf(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 1)) return FALSE;
	int nId = atoi(cmdparam.m_args[0]);
	CHECK_ID(nId);
	if (g_nat[nId].IsFree())
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: no listening socket on id %d\r\n" COLOR_DEFAULT, nId);
		return FALSE;
	}
	if (ConfirmAction(cmdparam.m_sock, nId, "delete"))
	{
		CString sError;
		if (g_nat[nId].Delete(sError))
			SockPrintf(cmdparam.m_sock, "config id %d deleted\r\n", nId);
		else
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: config id %d not deleted (%s)\r\n" COLOR_DEFAULT, nId, g_nat[nId].m_sockListen.GetLastErrorString());
		SaveNatConfigurations();
	}
	return FALSE;
}

BOOL CConfig::CfgSerialComList(CCmdParam& cmdparam)
{
	SockPrintf(cmdparam.m_sock, "Available comm ports:\r\n");
	COMMCONFIG CommConfig;

	CommConfig.dwSize = sizeof(CommConfig);
	CommConfig.wVersion = 1;
	DWORD dwSizeCommConfig;
	char szCommPort[10];
	//CString sPortConfig;
	for(int i = 1; i <= 20; i++)
	{
		StringCchPrintf(szCommPort, sizeof(szCommPort), "COM%d", i);
		if (GetDefaultCommConfig(szCommPort, &CommConfig, &dwSizeCommConfig))
		{
			//sPortConfig.Format("%s : %", CommConfig.
			SockPrintf(cmdparam.m_sock, szCommPort);
			SockPrintf(cmdparam.m_sock, "\r\n");
			//saCommPorts.Add(szCommPort);
		}
	}
	return FALSE;
}

BOOL CConfig::CfgInterfaceList(CCmdParam& cmdparam)
{
	SockPrintf(cmdparam.m_sock, "Available ips interfaces:\r\n");
	struct hostent * phe = gethostbyname(CSockAddr::GetHostName());
	if (! phe)
	{
		SockPrintf(cmdparam.m_sock, "[none]\r\n");
		return FALSE;
	}
	DWORD ** p = (DWORD **)phe->h_addr_list;
	for(;;)
	{
		if (*p == NULL) break;
		CSockAddr sa(**p);
		SockPrintf(cmdparam.m_sock, "%s\r\n", sa.String());
		p++;
	}
	return FALSE;
}

BOOL CConfig::CfgConnect(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 1, 2)) return FALSE;
	CFields fld(cmdparam.m_args[0], ':');
	CFields fldbind(cmdparam.m_args[1], ':');
	SockPrintf(cmdparam.m_sock, "Trying connection to %s:%s (bind %s:%s)...\r\n", fld[0], fld[1], fldbind[0], fldbind[1]);
	CSock sockConnect;
	if (! sockConnect.Create(SOCK_STREAM))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to create socket (%s)\r\n" COLOR_DEFAULT, sockConnect.GetLastErrorString());
		return FALSE;
	}
	if (fldbind[0] != "" || fldbind[1] != "")
	{
		CString bindaddr = (const char*)(fldbind[0]==""?"0.0.0.0":fldbind[0]);
		CString bindport = (const char*)(fldbind[1]==""?"0":fldbind[1]);
		if (! sockConnect.Bind(CSockAddr(bindaddr), CSockPort(bindport)))
		{
			SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to bind socket %s:%s (%s)\r\n" COLOR_DEFAULT, bindaddr, bindport, sockConnect.GetLastErrorString());
			return FALSE;
		}
	}
	if (! sockConnect.Connect(CSockAddr(fld[0]), CSockPort(fld[1])))
	{
		SockPrintf(cmdparam.m_sock, COLOR_ERROR "!Error: Unable to connect to %s:%s (%s)\r\n" COLOR_DEFAULT, fld[0], fld[1], sockConnect.GetLastErrorString());
		return FALSE;
	}
	else
	{
		SockPrintf(cmdparam.m_sock, "Connection to %s:%s socket ok !!!\r\n", fld[0], fld[1]);
#ifdef _DEBUG
		Sleep(15000);
#else
		Sleep(2000);
#endif
		SockPrintf(cmdparam.m_sock, "Disconnected !!!\r\n");
		sockConnect.Close();
	}
	return FALSE;
}

BOOL CConfig::CfgVer(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 0, 0))
	{
		return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Display version informations:\r\n"
		           "-----------------------------\r\n");
	SockPrintf(cmdparam.m_sock,
		"%s (configuration) Ver %s\r\n"
		"Compile Date: %s %s\r\n", 
		GET_SERVICE_NAME,
		VER_NATSERVICES_FULL,
		__DATE__, __TIME__);
	return FALSE;
}

BOOL CConfig::CfgForceSave(CCmdParam& cmdparam)
{
	SockPrintf(cmdparam.m_sock, "Saving parameters...\r\n");
	SaveNatConfigurations();
	SockPrintf(cmdparam.m_sock, "Saving saved.");
	return FALSE;
}

BOOL CConfig::CfgHelp(CCmdParam& cmdparam)
{
	if (! CheckBadParameters(cmdparam, 0, 0))
	{
		if (cmdparam.m_args[0] != "hide") return FALSE;
	}
	SockPrintf(cmdparam.m_sock, "Help screen:\r\n"
		           "------------\r\n");
	for(int i=0; i<sizeof(g_cfgcmd)/sizeof(g_cfgcmd[0]); i++)
	{
		if (! strcmp(g_cfgcmd[i].szCommand, ""))
		{
			SockPrintf(cmdparam.m_sock, "\r\n");
		}
		else if (g_cfgcmd[i].szHelp != NULL || (g_cfgcmd[i].szHelp == NULL && cmdparam.m_args[0] != ""))
		{
			char szBuf[2000];
			StringCchPrintf(szBuf, sizeof(szBuf), "%-12s : %s\r\n", g_cfgcmd[i].szCommand, g_cfgcmd[i].szHelp != NULL?g_cfgcmd[i].szHelp:"[None]");
			SockPrintf(cmdparam.m_sock, szBuf);
		}
	}
	return FALSE;
}

BOOL CConfig::CfgConfig(CCmdParam& cmdparam)
{
	CSockAddr addrlocal, addrpeer;
	CSockPort portlocal, portpeer;
	cmdparam.m_sock.GetSock().GetSockName(addrlocal, portlocal);
	cmdparam.m_sock.GetSock().GetPeerName(addrpeer, portpeer);

	CConfig::SockPrintf(cmdparam.m_sock,
		"\r\n"
		"Configuration:\r\n"
		"--------------\r\n"
		"Max configurations         : %d\r\n"
#ifdef _NATLOAD
		"Max load addresses         : %d\r\n"
#endif
		"Max connections addresses  : %d\r\n"
#ifdef _NATLOAD
		"Check listen up time (sec.): %d\r\n"
#endif
		"LogMode                    : %d / %d\r\n"
		"LogColCount/Break          : %d / %d\r\n"
		"DumpLogFileFormat          : %s\r\n"
		"   e.g. => %s\r\n"
		"Exec on Accept             : %s [%s]\r\n"
		"Allocated memory           : %u\r\n"
		"\r\n",
		MAX_CONFIG,
#ifdef _NATLOAD
		MAX_LOAD_ADDR,
#endif
		MAX_CONNECT
#ifdef _NATLOAD
		, DEF_CHECK_LISTEN_UP_TIME / 1000
#endif
		, g_dwLogMode, g_dwLogModeCS
		, g_dwLogColCount, g_dwLogBlockBreak
		, g_sDumpLogFileFormat, ReplaceTokensFileName(g_sDumpLogFileFormat, addrlocal.String(), portlocal.String(), addrpeer.String(), portpeer.String())
		, (LPCTSTR)g_sExecOnAccept
		, (LPCTSTR)g_sExecOnAcceptRunTime
		, sizeof(g_nat)
		);
	CConfig::SockPrintf(cmdparam.m_sock, 
		"Config listening address   : %s:%s\r\n"
		"Remote connection address  : %s:%s\r\n",
		addrlocal.String(), portlocal.String(),
		addrpeer.String(), portpeer.String());
	CConfig::SockPrintf(cmdparam.m_sock,
		"\r\n"
		"Last connection date       : %s\r\n"
		"Last connection address    : %s\r\n",
		g_lastconnectionip == ""?"[None]":g_lastconnectiondate.ToString(CDateRef::DateAndTimeFR),
		g_lastconnectionip == ""?"[None]":g_lastconnectionip);
	CConfig::SockPrintf(cmdparam.m_sock,
		"\r\n"
		"Installed at               : %s\r\n"
		"Config file                : %s\r\n"
		"Log directory              : %s\r\n   => %s\r\n"
		"Dump directory             : %s\r\n   => %s\r\n"
		"Dat directory              : %s\r\n   => %s\r\n"
		"Current log file           : %s\r\n"
		, (LPCTSTR)GetModuleFileNameStr()
		, g_config.GetConfigRepository()
		, g_sLogDir, (LPCTSTR)GetLogDir()
		, g_sDumpDir, (LPCTSTR)GetDumpDir()
		, g_sDatDir, (LPCTSTR)GetDatDir()
		, g_szLogFile
	);
	return FALSE;
}

BOOL CConfig::ConfigCommand(CTelnetServer& sock, char * szCommand)
{
	CArguments Args(szCommand);

	if (! Args.GetArgCount()) return FALSE;
	SockPrintf(sock, "\r\n");
	int nIndCmd;
	CCmdParam cmdparam(sock, Args);
	for(nIndCmd = 0; nIndCmd<sizeof(g_cfgcmd)/sizeof(g_cfgcmd[0]); nIndCmd++)
	{
		if (_tcsicmp(g_cfgcmd[nIndCmd].szCommand, ""))
			if (! _tcsicmp(g_cfgcmd[nIndCmd].szCommand, Args[0]))
			{
				Args.RemoveAt(0);
				cmdparam.m_szCommand = g_cfgcmd[nIndCmd].szCommand;
				cmdparam.m_szHelp = g_cfgcmd[nIndCmd].szHelp;
				return g_cfgcmd[nIndCmd].func(cmdparam);//sock, Args);
			}
	}
	SockPrintf(sock, "Unknown command '%s'", Args[0]);
	return FALSE;
}

void CConfig::CfgSendPrompt(CTelnetServer& sock)
{
	CString sPrompt;
	
	sPrompt.Format("\r\n"
#if defined _NATLOAD
			COLOR_LIGHT_CYAN "%s"
#elif defined _NAT
			COLOR_LIGHT_GREEN "%s"
#else
			PPERROR,
#endif
			COLOR_DEFAULT "["
#if defined _NATLOAD
			COLOR_CYAN "%s"
#elif defined _NAT
			COLOR_GREEN "%s"
#else
			PPERROR,
#endif
			COLOR_DEFAULT "]$ ", GetServiceName(), GetComputerNameStr());
	SockPrintf(sock, sPrompt);
	SockPrintf(sock, COLOR_DEFAULT);
}

//	Algo password
//
//	<Annee(AA)+secondes><mois+minute><jour+heures> additions sans retenue
//
//
BOOL CConfig::IsGoodPassword(const CString& sPassword, const SYSTEMTIME& lt)
{
	CString sGoodPassword;
	sGoodPassword.Format("%02d%02d%02d", lt.wYear-2000+lt.wSecond, lt.wMonth+lt.wMinute, lt.wDay+lt.wHour);
	return sPassword == sGoodPassword;
}

void DumpFrame(LPCTSTR szTitle, CFileText& ftLog, LPCTSTR szBuffer, int nLenBuffer2, BOOL fServerOut);

DWORD WINAPI CConfig::ConfigThreadTCP(LPVOID lpParameter)
{
	static int nIdConfig = 0;
	nIdConfig++;

	CString sLastConnectionIP;
	CDateRef drLastConnectionDate;
	CSock &sockaccept = *((CSock *)lpParameter);
	CSockAddr addrLocal, addrPeer;
	CSockPort portLocal, portPeer;
	sockaccept.GetSockName(addrLocal, portLocal);
	sockaccept.GetPeerName(addrPeer, portPeer);
#ifdef _DEBUG
	printf("%s [%d] Administration: Démarrage sur %s:%u peer %s:%u\n", (LPCTSTR)GetDateTimeStr(), nIdConfig, (LPCTSTR)addrLocal.String(), portLocal.HostNumber(), (LPCTSTR)addrPeer.String(), portPeer.HostNumber());
#endif
	Logfn("%s [%d] Administration: Démarrage sur %s:%u peer %s:%u", (LPCTSTR)GetDateTimeStr(), nIdConfig, (LPCTSTR)addrLocal.String(), portLocal.HostNumber(), (LPCTSTR)addrPeer.String(), portPeer.HostNumber());
	CTelnetServer sock((CSock *)lpParameter);
	sock.Negociate();

	CString sMsg;
	sMsg.Format(_T("  %s - Setup module - Ver %s  "), _T("NatSVC"), VER_NATSERVICES_FULL);
	static LPCTSTR szDash = "----------------------------------------------------------------------------------------";
	static LPCTSTR szCrLf = "\r\n";
	CConfig::SockPrintf(sock, "\r\n" COLOR_BRIGHTNESS "" COLOR_YELLOW);
	CConfig::SockWrite(sock, szDash, sMsg.GetLength());
	CConfig::SockWrite(sock, szCrLf, (int)strlen(szCrLf));
	CConfig::SockWrite(sock, (LPCTSTR)sMsg, sMsg.GetLength());
	CConfig::SockWrite(sock, szCrLf, (int)strlen(szCrLf));
	CConfig::SockPrintf(sock, "  " COLOR_DEFAULT "" COLOR_YELLOW "Host        : " COLOR_BRIGHTNESS "%s\r\n", GetComputerNameStr());
	CConfig::SockPrintf(sock, "  " COLOR_DEFAULT "" COLOR_YELLOW "Service name: " COLOR_BRIGHTNESS "%s\r\n", GET_SERVICE_NAME);
	CConfig::SockPrintf(sock, COLOR_BRIGHTNESS "" COLOR_YELLOW);
	CConfig::SockWrite(sock, szDash, sMsg.GetLength());
	CConfig::SockWrite(sock, szCrLf, (int)strlen(szCrLf));
	CConfig::SockWrite(sock, szCrLf, (int)strlen(szCrLf));
	CConfig::SockWrite(sock, szCrLf, (int)strlen(szCrLf));
	CConfig::SockPrintf(sock, COLOR_DEFAULT "Compile Date: %s %s\r\n", __DATE__, __TIME__);
	
	SYSTEMTIME lt;
	GetLocalTime(&lt);
	CString sDateTime;
	sDateTime.Format("%02d/%02d/%02d %02d:%02d:%02d", lt.wYear-2000, lt.wMonth, lt.wDay, lt.wHour, lt.wMinute, lt.wSecond);
	CConfig::SockPrintf(sock, "\r\n %s\r\n", sDateTime);
#ifndef _DEBUG
	//	Demande password
	for(int i=0;; i++)
	{
		if (i >= 3)
		{
			CConfig::SockPrintf(sock, "\r\nBad password, exiting...\r\n");
			Logfn("%s [%d] Administration: Arret du thread: mauvaise saisie du mot de passe", GetDateTimeStr(), nIdConfig);
			delete ((CSock *)lpParameter);
			return 0;
		}
		CConfig::SockPrintf(sock, "\r\n Password: ");
		CString sPassword;
		if (! sock.GetsLF(sPassword, TRUE))
		{
			Logfn("%s [%d] Administration: Arret du thread: perte de communication lors de la saisie du mot de passe(%s)", GetDateTimeStr(), nIdConfig, sock.GetLastErrorString());
			delete ((CSock *)lpParameter);
			return 0;
		}
		if (CConfig::IsGoodPassword(sPassword, lt)) break;
	}
#endif
	//	Save Last Date/IP connection
	CSockAddr addr;
	CSockPort port;
	sockaccept.GetPeerName(addr, port);
	sLastConnectionIP = (LPCTSTR)addr.String();
	drLastConnectionDate.SetCurrentLocalTime();

	char szCommand[2000];
	char szBuffer[2000];
	int nIndCmd = 0;
	int nLenRead;
	CArguments args("");
	CCmdParam cmdparam(sock, args);
	CfgConfig(cmdparam);
	CConfig::CfgSendPrompt(sock);

	//	Save Last Date/IP connection
	g_lastconnectionip = (LPCTSTR)sLastConnectionIP;
	g_lastconnectiondate = drLastConnectionDate;

	ASSERT(g_hEventServerWantToStop);
	LONG lRetEvent;
	for(BOOL fQuit=FALSE;;)
	{
		if (fQuit) break;
		lRetEvent = WaitForSingleObject(g_hEventServerWantToStop, 0);
		if ((lRetEvent-WAIT_OBJECT_0) == 0) break;
		nLenRead = sock.Receive((BYTE *)szBuffer, sizeof(szBuffer));
		if (nLenRead < 0)
		{
			if (nLenRead < -1) 
			{
				Logfn("%s [%d] Administration: Probleme de lecture sur la socket '%s' (%s)", GetDateTimeStr(), nIdConfig, sock.GetSockTitle(), sock.GetLastErrorString());
				sock.Close();
				break;
			}
			Logfn("%s [%d] Administration: Deconnexion demandee par l'hote distant", GetDateTimeStr(), nIdConfig);
			sock.Close();
			break;
		}
#ifdef _DEBUG0
		szBuffer[nLenRead] = '\0';
		printf("Buffer='%s' (premier caractere=%d\n", szBuffer, szBuffer[0]);
#endif
		if (nLenRead > 0)
		{
			for(int n=0; n<nLenRead; n++)
			{
				if (szBuffer[n] == 0xFF)
				{
					//	IAC	
				}
				else if ((szBuffer[n] != '\r') && (szBuffer[n] != '\n'))
				{
					if (szBuffer[n] == 8)
					{
						//	Back space
						if (nIndCmd > 0)
						{
							nIndCmd--;
							sock.Send("\x008 \x008", 3);
						}
					}
					else
					{
						if ((szBuffer[n]>=32) && (szBuffer[n] <= 127))
						{
							szCommand[nIndCmd++] = szBuffer[n];
							sock.Send(szBuffer+n, 1);
						}
					}
				}
				else
				{
					if (szBuffer[n] == '\r') // || (szBuffer[n] == '\n'))
					{
						szCommand[nIndCmd] = '\0';
						nIndCmd = 0;
						//DumpFrame2(szCommand, strlen(szCommand));
						if (strlen(szCommand)>0)
						{
							Logfn("%s [%d] Administration: command: '%s'", GetDateTimeStr(), nIdConfig, szCommand);
							if (CConfig::ConfigCommand(sock, szCommand))
							{
								//	Demande de deconnexion
								Logfn("%s [%d] Deconnexion: (command Quit)", GetDateTimeStr(), nIdConfig);
								sock.Close();
								fQuit = TRUE;
								break;
							}
						}
						CConfig::CfgSendPrompt(sock);
					}
				}
			}
		}
	}

	delete ((CSock *)lpParameter);

#ifdef _DEBUG
	printf("%s [%d] Administration: end of thread\n", (LPCTSTR)GetDateTimeStr(), nIdConfig);
#endif
	Logfn("%s [%d] Administration: end of thread", (LPCTSTR)GetDateTimeStr(), nIdConfig);
	return 0;
}