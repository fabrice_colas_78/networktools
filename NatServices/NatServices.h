#pragma once

#define VER_NATSERVICES_MAJOR		"3.1"
#define VER_NATSERVICES_REVISION	"a"

#define VER_NATSERVICES_FULL			_T(VER_NATSERVICES_MAJOR) _T(VER_NATSERVICES_REVISION)

#include "resource.h"

#ifdef _DEBUG
#define SZSERVICENAME			GetServiceName()
#define SZSERVICEDISPLAYNAME	GetDisplayName()
#else
#define SZSERVICENAME			GetServiceName()
#define SZSERVICEDISPLAYNAME	GetDisplayName()
#endif
#define SZDEPENDENCIES			""

#ifdef _DEBUG
#ifdef _NATLOAD
#define GET_SERVICE_NAME		"NatLoadSvcD"
#endif
#ifdef _NAT
#define GET_SERVICE_NAME		"NatSvcD"
#endif
#else
#define GET_SERVICE_NAME		GetServiceName()
#endif

#define DEF_INTERVAL_PURGE_DAYS	7

#ifdef _NATLOAD
#ifdef _DEBUG
#define MAX_CONFIG				6
#else
#define MAX_CONFIG				40
#endif
#endif

#ifdef _NAT
#ifdef _DEBUG
#define MAX_CONFIG				10
#else
#define MAX_CONFIG				400
#endif
#endif

#ifdef _NATLOAD
#define MAX_LOAD_ADDR			10
#endif

#ifdef _DEBUG
#define MAX_CONNECT				4
#else
#define MAX_CONNECT				300
#endif

#ifdef _NATLOAD
#ifdef _DEBUG
#define DEF_CHECK_LISTEN_UP_TIME	5000
#else
#define DEF_CHECK_LISTEN_UP_TIME	30000
#endif
#endif

void DisplayVersion();
const char * GetServiceName();
const char * GetDisplayName();
CString GetDumpDir();
CString GetDatDir();
CString GetLogDir();

CString ReplaceTokensFileName(LPCTSTR szFilePathFormatted, LPCTSTR sockaddrLocal, LPCTSTR sockportLocal, LPCTSTR sockaddrPeer, LPCTSTR sockportPeer);

BOOL CreateTraceFileName (void);
BOOL CreateLogFileName (void);
void Logf (const char * szFormat, ...);
void Logfn (const char * szFormat, ...);

extern char g_szLogFile[MAX_PATH + 1];