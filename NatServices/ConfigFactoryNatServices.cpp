#include "stdafx.h"

#include "NatServices.h"

#include "ConfigFactoryNatServices.h"

CNatServicesOptions::CNatServicesOptions()
{
	_purgeLogDays = DEF_INTERVAL_PURGE_DAYS;
	_logMode = 0;
	_logModeCS = 0;
	_logColCount = 16;
	_logBlockBreak = 8;

	_dumpLogFileFormat = "{DUMPDIR}\\{TIMESTAMP}_{HOSTIP}.{HOSTPORT}_{REMOTEIP}.{REMOTEPORT}.log";

	_logDir = "{EXEDIR}\\log";
	_dumpDir = "{EXEDIR}\\dump";
	_datDir = "{EXEDIR}\\log";

	_configAddr = "0.0.0.0";
#if defined _NATLOAD
	_configPort = 867;
#elif defined _NAT
	_configPort = 857;
#else
	_configPort = PP_ERROR;
#endif

#ifdef _DEBUG
	_configPort -= 1;
#endif
}

CConfigFactoryNatServices::CConfigFactoryNatServices() : CConfigFactory(STR_REGISTRY_ENTRY /* registry */, STR_XML_ENTRY /* Xml */)
{
}

bool CConfigFactoryNatServices::Prepare(LPCTSTR szConfigFilePath, CConfigFactory::ConfigFormat configFormat)
{
	GetPersistenConfigJSonFile().SetFileName(szConfigFilePath);

	SetConfigClass(configFormat);
	if (!GetConfig().IsExist())
	{
		CString sError;
		if (!GetConfig().Reset(sError)) return false;
	}
	return true;
}

bool CConfigFactoryNatServices::ReadOptions(CNatServicesOptions& options)
{
	HANDLE hNodeOptions;
	if (!GetConfig().OpenNode(hNodeOptions, NULL, _T("options"), false)) return false;

	GetConfig().QueryValue(hNodeOptions, _T("configAddr"), options._configAddr);
	GetConfig().QueryValue(hNodeOptions, _T("configPort"), options._configPort);
	GetConfig().QueryValue(hNodeOptions, _T("purgeLogDays"), options._purgeLogDays);
	GetConfig().QueryValue(hNodeOptions, _T("logMode"), options._logMode);
	GetConfig().QueryValue(hNodeOptions, _T("logModeCS"), options._logModeCS);
	GetConfig().QueryValue(hNodeOptions, _T("logColCount"), options._logColCount);
	GetConfig().QueryValue(hNodeOptions, _T("logBlockBreak"), options._logBlockBreak);
	GetConfig().QueryValue(hNodeOptions, _T("dumpLogFileFormat"), options._dumpLogFileFormat);
	GetConfig().QueryValue(hNodeOptions, _T("dumpDir"), options._dumpDir);
	GetConfig().QueryValue(hNodeOptions, _T("logDir"), options._logDir);
	GetConfig().QueryValue(hNodeOptions, _T("datDir"), options._datDir);
	GetConfig().QueryValue(hNodeOptions, _T("execOnAccept"), options._execOnAccept);

	//	Calculated
	options._execOnAcceptRunTime = TranslateEnv(options._execOnAccept, false);

	return true;
}

bool CConfigFactoryNatServices::WriteOptions(const CNatServicesOptions& options)
{
	HANDLE hNodeOptions;
	if (!GetConfig().OpenNode(hNodeOptions, NULL, _T("options"), true))
	{
		printf("Unable to open config repository\n");
		return false;
	}

	printf("Writing options in config repository...\n");
#ifdef _DEBUG
	printf("LogDir: '%s'\n", (LPCTSTR)options._logDir);
#endif
	GetConfig().SetValue(hNodeOptions, _T("configAddr"), options._configAddr);
	GetConfig().SetValue(hNodeOptions, _T("configPort"), options._configPort);
	GetConfig().SetValue(hNodeOptions, _T("purgeLogDays"), options._purgeLogDays);
	GetConfig().SetValue(hNodeOptions, _T("logMode"), options._logMode);
	GetConfig().SetValue(hNodeOptions, _T("logModeCS"), options._logModeCS);
	GetConfig().SetValue(hNodeOptions, _T("logColCount"), options._logColCount);
	GetConfig().SetValue(hNodeOptions, _T("logBlockBreak"), options._logBlockBreak);
	GetConfig().SetValue(hNodeOptions, _T("dumpLogFileFormat"), options._dumpLogFileFormat);
	GetConfig().SetValue(hNodeOptions, _T("dumpDir"), options._dumpDir);
	GetConfig().SetValue(hNodeOptions, _T("logDir"), options._logDir);
	GetConfig().SetValue(hNodeOptions, _T("datDir"), options._datDir);
	GetConfig().SetValue(hNodeOptions, _T("execOnAccept"), options._execOnAccept);

	return true;
}

#if defined _NAT
bool CConfigFactoryNatServices::ReadNatConfigurations(CNat* nat, int size)
#endif
#if defined _NATLOAD
bool CConfigFactoryNatServices::ReadNatConfigurations(CNatLoad* nat, int size)
#endif
{
	HANDLE hNodeNatConfigs;
	if (!GetConfig().OpenNode(hNodeNatConfigs, NULL, _T("natconfigs"), false)) return false;

	CString strKey;
	CString sAddrConnect, sPortConnect, sAddrListen, sPortListen;
	CString sConfigName;
#if defined _NATLOAD
	CString sAddrConnectActivated, sAddrConnectWeight;
#endif
	CString  sAddrOut;
	int nCommDelay;
	BOOL fActivated, fLogged;
	for (int nIndex = 0; nIndex < size; nIndex++)
	{
		int i;

		sConfigName = sAddrConnect = sPortConnect = sAddrListen = sPortListen = "";
#if defined _NATLOAD
		sAddrConnectActivated = sAddrConnectWeight = "";
#endif
		strKey.Format("%03d.ConfigName", nIndex);
		if (GetConfig().QueryValue(hNodeNatConfigs, strKey, sConfigName.GetBuffer(100), 100))
		{
			fActivated = TRUE;
			strKey.Format("%03d.Activated", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, fActivated);

			fLogged = FALSE;
			strKey.Format("%03d.Logged", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, fLogged);

			nCommDelay = 0;
			strKey.Format("%03d.CommDelay", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, nCommDelay);

			strKey.Format("%03d.AddrListen", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sAddrListen.GetBuffer(50), 50);

			strKey.Format("%03d.PortListen", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sPortListen.GetBuffer(50), 50);

			strKey.Format("%03d.AddrConnect", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sAddrConnect.GetBuffer(2000), 2000);

			strKey.Format("%03d.PortConnect", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sPortConnect.GetBuffer(50), 50);

			strKey.Format("%03d.AddrOut", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sAddrOut.GetBuffer(100), 100);

			strKey.Format("%03d.BindPrivatePort", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_fBindPrivatePort);

			strKey.Format("%03d.BufferSize", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_dwBufferSize);

			CString sACL;
			strKey.Format("%03d.ACL", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sACL.GetBuffer(2000), 2000);
			CFields fldACL(sACL, ',');
			for (i = 0; i < fldACL.GetSize(); i++)
			{
				if (fldACL[i] != "")
					g_nat[nIndex].m_saACL.Add(fldACL[i]);
			}

#if defined _NATLOAD
			strKey.Format("%03d.AddrConnectActivated", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sAddrConnectActivated.GetBuffer(400), 400);

			strKey.Format("%03d.AddrConnectWeight", nIndex);
			GetConfig().QueryValue(hNodeNatConfigs, strKey, sAddrConnectWeight.GetBuffer(2000), 2000);
#endif

			if (sAddrListen != "" && sPortListen != "" && sAddrConnect != "" && sPortConnect != ""
#if defined _NATLOAD
				&& sAddrConnectActivated != ""
#endif
				)
			{
				CString sAddrPortListen, sAddrPortConnect;
				sAddrPortListen.Format("%s:%s", (LPCTSTR)sAddrListen, (LPCTSTR)sPortListen);
#if defined _NAT
				sAddrPortConnect.Format("%s:%s", (LPCTSTR)sAddrConnect, (LPCTSTR)sPortConnect);
#elif defined _NATLOAD
				CFields fldAddrConnect(sAddrConnect, ','),
					fldPortConnect(sPortConnect, ',');
				for (int k = 0; k < fldAddrConnect.GetSize(); k++)
				{
					CString sTmp;
					sTmp.Format("%s:%s", fldAddrConnect[k], fldPortConnect[k] == "" ? fldPortConnect[0] : fldPortConnect[k]);
					sAddrPortConnect += sTmp;
					sAddrPortConnect += ",";
				}
				//sAddrPortConnect.Format("%s:%s", "150.175.42.144", "6023");
#endif
				g_nat[nIndex].SetConf(sConfigName, sAddrPortListen, sAddrPortConnect,
#if defined _NATLOAD
					sAddrConnectActivated, sAddrConnectWeight,
#endif
					sAddrOut, nCommDelay, fActivated, fLogged);
			}
		}
	}

	//	Dump nat configs in log file
	//CString sAddrConnect, sPortConnect;
	for (int nIndex = 0; nIndex < MAX_CONFIG; nIndex++)
	{
		if (g_nat[nIndex].IsUsed())
		{
#ifdef _DEBUG
			printf("Connection parameters : %d\n", nIndex);
			printf(" Nom de la config.    : %s\n", (LPCTSTR)g_nat[nIndex].m_sConfigName);
			printf("  - Adresse ecoute    : %s\n", (LPCTSTR)g_nat[nIndex].m_sAddrListen);
			printf("  - Port ecoute       : %s\n", (LPCTSTR)g_nat[nIndex].m_sPortListen);
#endif
			Logfn("Paramétres connexion  : %d", nIndex);
			Logfn("  - Adresse ecoute    : %s", (LPCTSTR)g_nat[nIndex].m_sAddrListen);
			Logfn("  - Port ecoute       : %s", (LPCTSTR)g_nat[nIndex].m_sPortListen);
#if defined _NATLOAD
			for (int nIndexAddr = 0; ; nIndexAddr++)
			{
				if (g_nat[nIndex].m_loadaddr[nIndexAddr].IsFree()) break;
				sAddrConnect += (LPCTSTR)g_nat[nIndex].m_loadaddr[nIndexAddr].m_sAddrConnect;
				sAddrConnect += g_nat[nIndex].m_loadaddr[nIndexAddr].IsActivated() ? "(Y)" : "(N)";
				sAddrConnect += ",";
				sPortConnect += (LPCTSTR)g_nat[nIndex].m_loadaddr[nIndexAddr].m_sPortConnect;
				sPortConnect += ",";
			}
#elif defined _NAT
			sAddrConnect = g_nat[nIndex].m_sAddrConnect;
			sPortConnect = g_nat[nIndex].m_sPortConnect;
#else
			PP_ERROR;
#endif
#ifdef _DEBUG
			printf("  - Adr. de connexion : %s\n", (LPCTSTR)sAddrConnect);
			printf("  - Por. de connexion : %s\n", (LPCTSTR)sPortConnect);
#endif
			Logfn("  - Adr. de connexion : %s", (LPCTSTR)sAddrConnect);
			Logfn("  - Por. de connexion : %s", (LPCTSTR)sPortConnect);
		}
	}

	return true;
}

#if defined _NAT
bool CConfigFactoryNatServices::WriteNatConfigurations(const CNat* nat, int size)
#endif
#if defined _NATLOAD
bool CConfigFactoryNatServices::WriteNatConfigurations(const CNatLoad* nat, int size)
#endif
{
	HANDLE hNodeNatConfigs;
	if (!GetConfig().OpenNode(hNodeNatConfigs, NULL, _T("natconfigs"), true)) return false;

	for (int nIndex = 0; nIndex < size; nIndex++)
	{
		int i;
		CString strKey;

		strKey.Format("%03d.ConfigName", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_sConfigName);

		strKey.Format("%03d.Activated", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].IsEnabled());

		strKey.Format("%03d.Logged", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].IsLogged());

		strKey.Format("%03d.CommDelay", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_nCommDelay);

		strKey.Format("%03d.AddrListen", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_sAddrListen);

		strKey.Format("%03d.PortListen", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_sPortListen);

#if defined _NATLOAD
		CString sAddrConnectList, sPortConnectList, sAddrConnectActivated, sAddrConnectWeight, s;
		for (i = 0; i < MAX_LOAD_ADDR; i++)
		{
			sAddrConnectList += g_nat[nIndex].m_loadaddr[i].m_sAddrConnect;
			sAddrConnectList += ",";
			sPortConnectList += g_nat[nIndex].m_loadaddr[i].m_sPortConnect;
			sPortConnectList += ",";
			sAddrConnectActivated += g_nat[nIndex].m_loadaddr[i].m_fActivated ? "Y" : "N";
			sAddrConnectActivated += ",";
			s.Format("%d", g_nat[nIndex].m_loadaddr[i].m_wWeight);
			sAddrConnectWeight += s;
			sAddrConnectWeight += ",";
		}
		strKey.Format("%03d.AddrConnect", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, sAddrConnectList);

		strKey.Format("%03d.PortConnect", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, sPortConnectList);

		strKey.Format("%03d.AddrConnectActivated", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, sAddrConnectActivated);

		strKey.Format("%03d.AddrConnectWeight", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, sAddrConnectWeight);
#elif defined _NAT
		strKey.Format("%03d.AddrConnect", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_sAddrConnect);

		strKey.Format("%03d.PortConnect", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_sPortConnect);
#else
		PP_ERROR;
#endif

		CString sACL;
		for (i = 0; i < g_nat[nIndex].m_saACL.GetSize(); i++)
		{
			if (g_nat[nIndex].m_saACL[i] != "")
			{
				sACL += g_nat[nIndex].m_saACL[i];
				sACL += ",";
			}
		}
		strKey.Format("%03d.ACL", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, sACL);

		strKey.Format("%03d.AddrOut", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_sAddrOut);

		strKey.Format("%03d.BindPrivatePort", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_fBindPrivatePort);

		strKey.Format("%03d.BufferSize", nIndex);
		GetConfig().SetValue(hNodeNatConfigs, strKey, g_nat[nIndex].m_dwBufferSize);
	}
	return true;
}


#if 0
void CConfigFactoryNatServices::SetConfigPort(int port)
{
	_configPort = port;
	WriteOptions();
}
void CConfigFactoryNatServices::SetConfigAddr(LPCTSTR configAddr)
{
	_configAddr = configAddr;
	WriteOptions();
}
void CConfigFactoryNatServices::SetPurgeLogDays(int purgeLogDays)
{
	_purgeLogDays = purgeLogDays;
	WriteOptions();
}
void CConfigFactoryNatServices::SetLogMode(int logMode)
{
	_logMode = logMode;
	WriteOptions();
}
void CConfigFactoryNatServices::SetLogModeCS(int logModeCS)
{
	_logModeCS = logModeCS;
	WriteOptions();
}
void CConfigFactoryNatServices::SetExecOnAccept(LPCTSTR execOnAccept)
{
	_execOnAccept = execOnAccept;
	WriteOptions();
}
#endif

//
void CConfigFactoryNatServices::Test()
{
	CConfigFactoryNatServices cf;
	cf.Prepare(CFNC(GetEnv(_T("EXEDIR")), GFNC(GetModuleFileNameStr(), GFNC_FILENAME), _T("config-test.json")));
#if 1
	if (cf.Open(true, _T("main")))
	{
#if 1
		{
			HANDLE hNode;
			if (cf.GetConfig().OpenNode(hNode, NULL, _T("node1"), true))
			{
				if (!cf.GetConfig().SetValue(hNode, _T("Param1-1"), _T("value 1")))
				{
					ASSERT(false);
				}
				cf.GetConfig().SetValue(hNode, _T("param-int1"), 12345678);
				cf.GetConfig().SetValue(hNode, _T("param-bool"), true);
				cf.GetConfig().SetValue(hNode, _T("param-DWORD"), (DWORD)987654);
				cf.GetConfig().SetValue(hNode, _T("param-long"), -12345678);
				cf.GetConfig().CloseNode(hNode);
			}
		}
		{
			HANDLE hNode;
			if (cf.GetConfig().OpenNode(hNode, NULL, _T("node2"), true))
			{
				if (!cf.GetConfig().SetValue(hNode, _T("Param2-1"), _T("value 1")))
				{
					ASSERT(false);
				}
				cf.GetConfig().CloseNode(hNode);
			}
		}
		{
			HANDLE hNode;
			if (cf.GetConfig().OpenNode(hNode, NULL, _T("node3"), true))
			{
				{
					HANDLE hNode2;
					if (!cf.GetConfig().OpenNode(hNode2, hNode, _T("sub-node3-1"), true))
					{
						ASSERT(false);
					}
				}
				{
					HANDLE hNode2;
					if (!cf.GetConfig().OpenNode(hNode2, hNode, _T("sub-node3-2"), true))
					{
						ASSERT(false);
					}
				}
				if (!cf.GetConfig().SetValue(hNode, _T("Param3-1"), _T("value 1")))
				{
					ASSERT(false);
				}
				if (!cf.GetConfig().SetValue(hNode, _T("Param3-2"), _T("value 1")))
				{
					ASSERT(false);
				}
				cf.GetConfig().SetValue(hNode, _T("param-int1"), 12345678);
				cf.GetConfig().SetValue(hNode, _T("param-bool"), (bool)true);
				cf.GetConfig().SetValue(hNode, _T("param-DWORD"), (DWORD)987654);
				cf.GetConfig().SetValue(hNode, _T("param-long"), -12345678);
				for (int i = 0; i < 10; i++)
				{
					TCHAR szValue[128];
					if (cf.GetConfig().EnumNodes(hNode, i, szValue, sizeof(szValue)))
					{
						printf(_T(" NodeName:'%s'\n"), szValue);
					}
				}
				for (int i = 0; i < 10; i++)
				{
					TCHAR szValue[128];
					if (cf.GetConfig().EnumValues(hNode, i, szValue, sizeof(szValue)))
					{
						printf(_T(" ValueName:'%s'\n"), szValue);
					}
				}
				int valueint;
				cf.GetConfig().QueryValue(hNode, _T("param-int1"), valueint);
				printf(_T("valueint:%d\n"), valueint);
				cf.GetConfig().CloseNode(hNode);
			}
		}
#endif
		cf.Close(_T("main"));
	}
#endif
}