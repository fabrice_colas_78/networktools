#include <stdafx.h>

#include <winsock2.h>

#include <dateref.h>
#include <LibSystem.h>
#include <ntservice.h>
#include <registry.h>
#include <sock.h>
#include <serial.h>

#include "NatServices.h"

#include "NatClasses.h"
#include "Config.h"
#include "ConfigFactoryNatServices.h"

HANDLE g_hEventServerWantToStop = NULL;

CNatServicesOptions g_natServicesOptions;

#if defined _NATLOAD
DWORD g_dwCheckListenUpTime = DEF_CHECK_LISTEN_UP_TIME;
CNatLoad g_nat[MAX_CONFIG];
#endif

#if defined _NAT
CNat g_nat[MAX_CONFIG];
#endif

UINT g_uConnectID = 0;
BOOL g_fBusy = FALSE;

static int g_nComID = 0;

CString GetAppVersion()
{
	CString strVersion;
	char szModuleName[200];
	SYSTEMTIME ST;
	HANDLE hFFF;
	WIN32_FIND_DATA WFD;

	if (GetModuleFileName(NULL, szModuleName, sizeof(szModuleName)) == 0)
	{
		strcpy(szModuleName, "???");
	}
	if ((hFFF = FindFirstFile(szModuleName, &WFD)) == NULL)
	{
		return CString("???");
	}
	FindClose(hFFF);
	FileTimeToSystemTime(&WFD.ftLastWriteTime, &ST);
	strVersion.Format(_T("%s%c - %02d/%02d/%04d (build %02d%02d)"),
		VER_NATSERVICES_FULL,
#ifdef _DEBUG
		'D',
#else
		' ',
#endif
		ST.wDay, ST.wMonth, ST.wYear, ST.wHour, ST.wMinute);
	return strVersion;
}

CString GetAppName()
{
	return CString(SZSERVICEDISPLAYNAME);
}

#ifdef _NAT
BOOL IsCommPort(LPCTSTR s)
{
	if (!_tcsicmp(s, "com1")) return TRUE;
	if (!_tcsicmp(s, "com2")) return TRUE;
	if (!_tcsicmp(s, "com3")) return TRUE;
	if (!_tcsicmp(s, "com4")) return TRUE;
	if (!_tcsicmp(s, "com5")) return TRUE;
	if (!_tcsicmp(s, "com6")) return TRUE;
	if (!_tcsicmp(s, "com7")) return TRUE;
	if (!_tcsicmp(s, "com8")) return TRUE;
	if (!_tcsicmp(s, "com9")) return TRUE;
	if (!_tcsicmp(s, "com10")) return TRUE;
	if (!_tcsicmp(s, "com11")) return TRUE;
	if (!_tcsicmp(s, "com12")) return TRUE;
	if (!_tcsicmp(s, "com13")) return TRUE;
	if (!_tcsicmp(s, "com14")) return TRUE;
	if (!_tcsicmp(s, "com15")) return TRUE;
	if (!_tcsicmp(s, "com16")) return TRUE;
	if (!_tcsicmp(s, "com17")) return TRUE;
	if (!_tcsicmp(s, "com18")) return TRUE;
	if (!_tcsicmp(s, "com19")) return TRUE;
	return FALSE;
}
#else
BOOL IsCommPort(LPCTSTR)
{
	return FALSE;
}
#endif

void SaveSession(int nComID, const char* szConfigName,
	const char* addrPeerClient, const char* portPeerClient,
	const char* addrLocalClient, const char* portLocalClient,
	const char* addrLocalServer, const char* portLocalServer,
	const char* addrPeerServer, const char* portPeerServer,
	const char* addrOut,
	const char* drStart, const char* drEnd, const char* drDiff,
	DWORD dwClientTotalBytesIn, DWORD dwClientTotalBytesOut, DWORD dwServerTotalBytesIn, DWORD dwServerTotalBytesOut,
	DWORD dwFramesIn, DWORD dwFramesOut,
	double dKBClientIn, double dKBClientOut, double dKBServerIn, double dKBServerOut)
{
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime);
	char szSessionFile[300];
	wsprintf(szSessionFile, "%s\\%s.%04d%02d%02d.dat", (LPCTSTR)GetDatDir(), SZSERVICENAME,
		SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
	CFileText ft;
	if (!ft.OpenCreateAppend(szSessionFile, CFile::modeWrite | CFile::shareDenyNone)) return;
	/*
	Format du fichier:
	<id con>|<nom config>|datedeb|datefin|duree|peerclient|localclient|localserver|peerserver|clienttotbytesin|clienttotbytesout|servertotbytesout|servertotbytesin
	*/
	ft.Putsf("%7d|%-40s|%s|%s|%s|%15s:%-5s|%15s:%-5s|%15s:%-5s|%15s:%-5s|"
		"%7d|%7d|%10d|%10d|%10d|%10d|%4.2lf|%4.2lf|%4.2lf|%4.2lf",
		nComID, szConfigName,
		drStart, drEnd, drDiff,
		addrPeerClient, portPeerClient, addrLocalClient, portLocalClient,
		IsCommPort(addrLocalServer) ? portLocalServer : addrLocalServer, IsCommPort(addrLocalServer) ? addrLocalServer : portLocalServer,
		addrPeerServer, portPeerServer,
		dwFramesIn, dwFramesOut,
		dwClientTotalBytesIn, dwClientTotalBytesOut, dwServerTotalBytesIn, dwServerTotalBytesOut,
		dKBClientIn, dKBClientOut, dKBServerIn, dKBServerOut
	);
	ft.Close();
}

#define SET_REG()	\
	CString strRegistrySubKey; \
	strRegistrySubKey = "Software\\FalconSoftware\\"; \
	strRegistrySubKey += GET_SERVICE_NAME; \
	CRegistry RegConfig(fUserSpace ? HKEY_CURRENT_USER : HKEY_LOCAL_MACHINE, strRegistrySubKey)


#if 0
void SaveConfig(BOOL fUserSpace)
{
	SET_REG();
	Logfn("%s Saving configuration to '%s'...", GetDateTimeStr(), strRegistrySubKey);
	if (RegConfig.Open(TRUE) == FALSE)
	{
		Logfn("%s Ecriture des parametres dans la base de registre impossible.", GetDateTimeStr());
		return;
	}
	RegConfig.SetValue("AddrConfig", g_sConfigAddr);
	RegConfig.SetValue("PortConfig", g_nConfigPort);
	RegConfig.SetValue("PurgeLogDays", (DWORD)g_uPurgeLogDays);
	RegConfig.SetValue("LogMode", g_dwLogMode);
	RegConfig.SetValue("LogModeCS", g_dwLogModeCS);
	RegConfig.SetValue("ExecOnAccept", g_sExecOnAccept2);
	Logfn(" - Purge logs                   : %d jours", g_uPurgeLogDays);
	Logfn(" - Adresse d'acces a la config. : %s", g_sConfigAddr);
	Logfn(" - Port d'acces a la config.    : %d", g_nConfigPort);
	Logfn(" - Log mode                     : %d", g_dwLogMode);
	Logfn(" - Log mode CS                  : %d", g_dwLogModeCS);
	Logfn(" - Exec on Accept               : %s [%s]", (LPCTSTR)g_sExecOnAccept2, (LPCTSTR)g_sExecOnAcceptRunTime);
}

BOOL LoadConfig(BOOL fUserSpace)
{
	//if (g_fBusy) return FALSE;
	g_fBusy = TRUE;

	SET_REG();
	Logfn("%s Loading configuration from '%s'...", GetDateTimeStr(), strRegistrySubKey);
	if (!RegConfig.Open(FALSE))
	{
		Logfn("%s Lecture des parametres dans la base de registre impossible.", GetDateTimeStr());
	}
	else
	{
		if (!RegConfig.QueryValue("PurgeLogDays", (DWORD*)& g_uPurgeLogDays))
		{
		}
		if (!RegConfig.QueryValue("AddrConfig", g_sConfigAddr.GetBuffer(100), 100))
		{
		}
		if (!RegConfig.QueryValue("PortConfig", (int*)& g_nConfigPort))
		{
		}
		if (!RegConfig.QueryValue("LogMode", (DWORD*)& g_dwLogMode))
		{
		}
		if (!RegConfig.QueryValue("LogModeCS", (DWORD*)& g_dwLogMode))
		{
		}
		CString s;
		if (!RegConfig.QueryValue("ExecOnAccept", (CString*)& s))
		{
		}
		else
		{
			_SET_EXECONACCEPT(s);
		}
	}
	Logfn(" - Purge logs                   : %d jours", g_uPurgeLogDays);
	Logfn(" - Adresse d'acces a la config. : %s", g_sConfigAddr);
	Logfn(" - Port d'acces a la config.    : %d", g_nConfigPort);
	Logfn(" - Log mode                     : %d", g_dwLogMode);
	Logfn(" - Log mode CS                  : %d", g_dwLogModeCS);
	Logfn(" - Exec on Accept               : %s [%s]", (LPCTSTR)g_sExecOnAccept2, (LPCTSTR)g_sExecOnAcceptRunTime);

	g_fBusy = FALSE;
	return TRUE;
}
#endif

#if 0
void SaveParameters(BOOL fUserSpace)
{
	SET_REG();
	Logfn("%s Saving parameters in '%s'...", GetDateTimeStr(), strRegistrySubKey);
	if (RegConfig.Open(TRUE) == FALSE)
	{
		Logfn("%s Ecriture des parametres dans la base de registre impossible.", GetDateTimeStr());
		return;
	}
	for (int nIndex = 0; nIndex < MAX_CONFIG; nIndex++)
	{
		int i;
		CString strKey;

		strKey.Format("%03d.ConfigName", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_sConfigName);

		strKey.Format("%03d.Activated", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].IsEnabled());

		strKey.Format("%03d.Logged", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].IsLogged());

		strKey.Format("%03d.CommDelay", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_nCommDelay);

		strKey.Format("%03d.AddrListen", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_sAddrListen);

		strKey.Format("%03d.PortListen", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_sPortListen);

#if defined _NATLOAD
		CString sAddrConnectList, sPortConnectList, sAddrConnectActivated, sAddrConnectWeight, s;
		for (i = 0; i < MAX_LOAD_ADDR; i++)
		{
			sAddrConnectList += g_nat[nIndex].m_loadaddr[i].m_sAddrConnect;
			sAddrConnectList += ",";
			sPortConnectList += g_nat[nIndex].m_loadaddr[i].m_sPortConnect;
			sPortConnectList += ",";
			sAddrConnectActivated += g_nat[nIndex].m_loadaddr[i].m_fActivated ? "Y" : "N";
			sAddrConnectActivated += ",";
			s.Format("%d", g_nat[nIndex].m_loadaddr[i].m_wWeight);
			sAddrConnectWeight += s;
			sAddrConnectWeight += ",";
		}
		strKey.Format("%03d.AddrConnect", nIndex);
		RegConfig.SetValue(strKey, sAddrConnectList);

		strKey.Format("%03d.PortConnect", nIndex);
		RegConfig.SetValue(strKey, sPortConnectList);

		strKey.Format("%03d.AddrConnectActivated", nIndex);
		RegConfig.SetValue(strKey, sAddrConnectActivated);

		strKey.Format("%03d.AddrConnectWeight", nIndex);
		RegConfig.SetValue(strKey, sAddrConnectWeight);
#elif defined _NAT
		strKey.Format("%03d.AddrConnect", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_sAddrConnect);

		strKey.Format("%03d.PortConnect", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_sPortConnect);
#else
		PP_ERROR
#endif

			CString sACL;
		for (i = 0; i < g_nat[nIndex].m_saACL.GetSize(); i++)
		{
			if (g_nat[nIndex].m_saACL[i] != "")
			{
				sACL += g_nat[nIndex].m_saACL[i];
				sACL += ",";
			}
		}
		strKey.Format("%03d.ACL", nIndex);
		RegConfig.SetValue(strKey, sACL);

		strKey.Format("%03d.AddrOut", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_sAddrOut);

		strKey.Format("%03d.BindPrivatePort", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_fBindPrivatePort);

		strKey.Format("%03d.BufferSize", nIndex);
		RegConfig.SetValue(strKey, g_nat[nIndex].m_dwBufferSize);
	}
	Logfn("%s Parameters saved.", GetDateTimeStr());
}

BOOL LoadParameters(BOOL fUserSpace)
{
	//if (g_fBusy) return FALSE;
	g_fBusy = TRUE;
	SET_REG();
	Logfn("%s Loading parameters from '%s'...", GetDateTimeStr(), strRegistrySubKey);

	if (!RegConfig.Open(FALSE))
	{
		Logfn("%s Lecture des parametres dans la base de registre impossible.", GetDateTimeStr());
	}
	else
	{
		CString strKey;
		CString sAddrConnect, sPortConnect, sAddrListen, sPortListen;
		CString sConfigName;
#if defined _NATLOAD
		CString sAddrConnectActivated, sAddrConnectWeight;
#endif
		CString  sAddrOut;
		int nCommDelay;
		BOOL fActivated, fLogged;
		for (int nIndex = 0; nIndex < MAX_CONFIG; nIndex++)
		{
			int i;

			sConfigName = sAddrConnect = sPortConnect = sAddrListen = sPortListen = "";
#if defined _NATLOAD
			sAddrConnectActivated = sAddrConnectWeight = "";
#endif
			strKey.Format("%03d.ConfigName", nIndex);
			if (RegConfig.QueryValue(strKey, sConfigName.GetBuffer(100), 100))
			{
				fActivated = TRUE;
				strKey.Format("%03d.Activated", nIndex);
				RegConfig.QueryValue(strKey, &fActivated);

				fLogged = FALSE;
				strKey.Format("%03d.Logged", nIndex);
				RegConfig.QueryValue(strKey, &fLogged);

				nCommDelay = 0;
				strKey.Format("%03d.CommDelay", nIndex);
				RegConfig.QueryValue(strKey, &nCommDelay);

				strKey.Format("%03d.AddrListen", nIndex);
				RegConfig.QueryValue(strKey, sAddrListen.GetBuffer(50), 50);

				strKey.Format("%03d.PortListen", nIndex);
				RegConfig.QueryValue(strKey, sPortListen.GetBuffer(50), 50);

				strKey.Format("%03d.AddrConnect", nIndex);
				RegConfig.QueryValue(strKey, sAddrConnect.GetBuffer(2000), 2000);

				strKey.Format("%03d.PortConnect", nIndex);
				RegConfig.QueryValue(strKey, sPortConnect.GetBuffer(50), 50);

				strKey.Format("%03d.AddrOut", nIndex);
				RegConfig.QueryValue(strKey, sAddrOut.GetBuffer(100), 100);

				strKey.Format("%03d.BindPrivatePort", nIndex);
				RegConfig.QueryValue(strKey, &g_nat[nIndex].m_fBindPrivatePort);

				strKey.Format("%03d.BufferSize", nIndex);
				RegConfig.QueryValue(strKey, &g_nat[nIndex].m_dwBufferSize);

				CString sACL;
				strKey.Format("%03d.ACL", nIndex);
				RegConfig.QueryValue(strKey, sACL.GetBuffer(2000), 2000);
				CFields fldACL(sACL, ',');
				for (i = 0; i < fldACL.GetSize(); i++)
				{
					if (fldACL[i] != "")
						g_nat[nIndex].m_saACL.Add(fldACL[i]);
				}

#if defined _NATLOAD
				strKey.Format("%03d.AddrConnectActivated", nIndex);
				RegConfig.QueryValue(strKey, sAddrConnectActivated.GetBuffer(400), 400);

				strKey.Format("%03d.AddrConnectWeight", nIndex);
				RegConfig.QueryValue(strKey, sAddrConnectWeight.GetBuffer(2000), 2000);
#endif

				if (sAddrListen != "" && sPortListen != "" && sAddrConnect != "" && sPortConnect != ""
#if defined _NATLOAD
					&& sAddrConnectActivated != ""
#endif
					)
				{
					CString sAddrPortListen, sAddrPortConnect;
					sAddrPortListen.Format("%s:%s", (LPCTSTR)sAddrListen, (LPCTSTR)sPortListen);
#if defined _NAT
					sAddrPortConnect.Format("%s:%s", (LPCTSTR)sAddrConnect, (LPCTSTR)sPortConnect);
#elif defined _NATLOAD
					CFields fldAddrConnect(sAddrConnect, ','),
						fldPortConnect(sPortConnect, ',');
					for (int k = 0; k < fldAddrConnect.GetSize(); k++)
					{
						CString sTmp;
						sTmp.Format("%s:%s", fldAddrConnect[k], fldPortConnect[k] == "" ? fldPortConnect[0] : fldPortConnect[k]);
						sAddrPortConnect += sTmp;
						sAddrPortConnect += ",";
					}
					//sAddrPortConnect.Format("%s:%s", "150.175.42.144", "6023");
#endif
					g_nat[nIndex].SetConf(sConfigName, sAddrPortListen, sAddrPortConnect,
#if defined _NATLOAD
						sAddrConnectActivated, sAddrConnectWeight,
#endif
						sAddrOut, nCommDelay, fActivated, fLogged);
				}
			}
		}
	}

	CString sAddrConnect, sPortConnect;
	for (int nIndex = 0; nIndex < MAX_CONFIG; nIndex++)
	{
		if (g_nat[nIndex].IsUsed())
		{
#ifdef _DEBUG
			printf("Connection parameters : %d\n", nIndex);
			printf(" Nom de la config.    : %s\n", (LPCTSTR)g_nat[nIndex].m_sConfigName);
			printf("  - Adresse ecoute    : %s\n", (LPCTSTR)g_nat[nIndex].m_sAddrListen);
			printf("  - Port ecoute       : %s\n", (LPCTSTR)g_nat[nIndex].m_sPortListen);
#endif
			Logfn("Param�tres connexion  : %d", nIndex);
			Logfn("  - Adresse ecoute    : %s", (LPCTSTR)g_nat[nIndex].m_sAddrListen);
			Logfn("  - Port ecoute       : %s", (LPCTSTR)g_nat[nIndex].m_sPortListen);
#if defined _NATLOAD
			for (int nIndexAddr = 0; ; nIndexAddr++)
			{
				if (g_nat[nIndex].m_loadaddr[nIndexAddr].IsFree()) break;
				sAddrConnect += (LPCTSTR)g_nat[nIndex].m_loadaddr[nIndexAddr].m_sAddrConnect;
				sAddrConnect += g_nat[nIndex].m_loadaddr[nIndexAddr].IsActivated() ? "(Y)" : "(N)";
				sAddrConnect += ",";
				sPortConnect += (LPCTSTR)g_nat[nIndex].m_loadaddr[nIndexAddr].m_sPortConnect;
				sPortConnect += ",";
			}
#elif defined _NAT
			sAddrConnect = g_nat[nIndex].m_sAddrConnect;
			sPortConnect = g_nat[nIndex].m_sPortConnect;
#else
			PP_ERROR;
#endif
#ifdef _DEBUG
			printf("  - Adr. de connexion : %s\n", (LPCTSTR)sAddrConnect);
			printf("  - Por. de connexion : %s\n", (LPCTSTR)sPortConnect);
#endif
			Logfn("  - Adr. de connexion : %s", (LPCTSTR)sAddrConnect);
			Logfn("  - Por. de connexion : %s", (LPCTSTR)sPortConnect);
		}
	}
	Logfn(" - Purge logs                      : %d jours", g_uPurgeLogDays);
	Logfn(" - Adresse ecoute config           : %s", (LPCTSTR)g_sConfigAddr);
	Logfn(" - Port d'acces a la configuration : %d", g_nConfigPort);
	Logfn(" - LogMode                         : %d / %d", g_dwLogMode, g_dwLogModeCS);
	Logfn(" - ExecOnAccept                    : %s [%s]", g_sExecOnAccept, g_sExecOnAcceptRunTime);
	g_fBusy = FALSE;
	return TRUE;
}
#endif

void PurgeLogs()
{
	//	Recalcul nom du log
	CreateTraceFileName();
	CreateLogFileName();

	SYSTEMTIME stLocalTime;
	GetLocalTime(&stLocalTime);
	CDateRef DatePurge(stLocalTime.wYear, stLocalTime.wMonth, stLocalTime.wDay);
	DatePurge -= g_uPurgeLogDays * (24 * 60 * 60);

	Logfn("%s Purge fichiers log (< � %02d/%02d/%04d)", GetDateTimeStr(), DatePurge.GetDayOfMonth(), DatePurge.GetMonth(), DatePurge.GetYear());

	char szFindMask[MAX_PATH + 1];
	HANDLE hFFF;
	WIN32_FIND_DATA Win32FD;

	wsprintf(szFindMask, "%s\\*.log", (LPCTSTR)GetLogDir(), SZSERVICENAME);
	//wsprintf(szPurgeMask1, "%s.%04d%02d%02d.log", SZSERVICENAME, DatePurge.GetYear(), DatePurge.GetMonth(), DatePurge.GetDayOfMonth());

	hFFF = FindFirstFile(szFindMask, &Win32FD);
	if (hFFF != INVALID_HANDLE_VALUE)
	{
		for (;;)
		{
			if (DatePurge > Win32FD.ftLastWriteTime)
			{
				CString strFileName;
				strFileName = ConcatFileNameComposant(GetLogDir(), Win32FD.cFileName);
				Logfn(" Suppression du fichier log '%s'", (const char*)strFileName);
#ifndef _DEBUG
				if (!DeleteFile(strFileName))
				{
					Logfn(" !!Erreur!!: Suppression du fichier log '%s' impossible.", (const char*)strFileName);
				}
#endif
			}
			if (!FindNextFile(hFFF, &Win32FD)) break;
		}
	}
}

#if defined _NATLOAD
int ComputeLoad(CNatLoad& natload)
{
	int nIndexLoad = -1;
	DWORD dwConnexionsMin = 0xFFFFFFFF;
	for (int i = 0; i < MAX_LOAD_ADDR; i++)
	{
		if (natload.m_loadaddr[i].IsUsed() && natload.m_loadaddr[i].m_fActivated)
		{
			DWORD dwCurConnex = natload.GetActiveConnectionsCount(i) / natload.m_loadaddr[i].m_wWeight;
			LOG2F("ComputeLoad: Addr=%s i=%d dwCurConnex=%d\n",
				natload.m_loadaddr[i].m_sAddrConnect, i, dwCurConnex);
			if (dwCurConnex < dwConnexionsMin)
			{
				if (natload.m_loadaddr[i].m_fListenUp)
				{
					dwConnexionsMin = dwCurConnex;
					LOG2F("Index:%d dwConnexionsMin=%d (reelles:%d, weight:%d)\n",
						i, dwConnexionsMin, natload.GetActiveConnectionsCount(i), natload.m_loadaddr[i].m_wWeight);
					nIndexLoad = i;
				}
				else
				{
					LOG2F("iLoad=%d ListenUp=FALSE\n", i);
				}
			}
		}
	}
	LOG2F("Index Load: %d\n", nIndexLoad);
	return nIndexLoad;
}

void CheckListenUp(CNatLoad& natload)
{
	for (int i = 0; i < MAX_LOAD_ADDR; i++)
	{
		if (natload.m_loadaddr[i].IsUsed() && natload.IsEnabled())
		{
			//LDA1F("CheckListenUp: Addr=%s i=%d SNatLoad.dwConnexionsCount[i]=%d\n", SNatLoad.strAddrConnect[i], i, SNatLoad.m_dwConnexionsCount[i]);
			CSock Sock;
			CSockAddr SockAddrConnect(natload.m_loadaddr[i].m_sAddrConnect);
			CSockPort SockPortConnect(natload.m_loadaddr[i].m_sPortConnect);
			if (Sock.CreateTCPConnect(SockAddrConnect, SockPortConnect))
			{
				LDA1F("%s Connexion sur l'adresse '%s:%s' Ok!!\n", GetDateTimeStr(), natload.m_loadaddr[i].m_sAddrConnect, natload.m_loadaddr[i].m_sPortConnect);
				natload.m_loadaddr[i].m_fListenUp = TRUE;
				Sleep(1000);	//	Attente 2 secondes pour maintenir la connexion un petit moment
				Sock.Close();
			}
			else
			{
				LDA1F("%s Connexion sur l'adresse '%s:%s' impossible\n", GetDateTimeStr(), natload.m_loadaddr[i].m_sAddrConnect, natload.m_loadaddr[i].m_sPortConnect);
				Logfn("%s Connexion sur l'adresse '%s:%s' impossible", GetDateTimeStr(), natload.m_loadaddr[i].m_sAddrConnect, natload.m_loadaddr[i].m_sPortConnect);
				natload.m_loadaddr[i].m_fListenUp = FALSE;
			}
		}
		Sleep(1);
	}
}

DWORD WINAPI CheckListenUpTCP(LPVOID)
{
	for (;;)
	{
		if (!g_fBusy)
		{
			g_fBusy = TRUE;
			for (int nIndex = 0; nIndex < MAX_CONFIG; nIndex++)
			{
				if (g_nat[nIndex].IsStarted())
				{
					CheckListenUp(g_nat[nIndex]);
				}
			}
			g_fBusy = FALSE;
		}
		Sleep(g_dwCheckListenUpTime);
	}
	return 0;
}
#endif

#define IsPrintable(c)	((c>=32)&&(c<=127))

#define IS_BLOCK_SEPARATOR(_c) (((_c) > 0)&&(((_c) % (nBlockBreak)) == 0))

void DumpLine(CFileText& ftLog, int nColCount, int nBlockBreak, const char* bufline, int nLenBuffer)
{
	char Buf[300];

	Buf[0] = '\0';
	if (g_dwLogMode == 2)
	{
		strcat(Buf, "\t\t\t");

	}
	if (g_dwLogMode == 5)
	{
		sprintf(Buf + strlen(Buf), "%04X ", nLenBuffer);
	}
	for (int n = 0; n < nColCount; n++)
	{
		if (IS_BLOCK_SEPARATOR(n))
		{
			if (g_dwLogMode == 0 || g_dwLogMode == 1 || g_dwLogMode == 2)
			{
				strcat(Buf, " ");
			}
		}
		if (n < nLenBuffer)
		{
			if (g_dwLogMode == 0)
				sprintf(Buf + strlen(Buf), "%02X ", ((int)(bufline[n])) & 0xff);
			else if (g_dwLogMode == 1)
			{
				if (n == (nLenBuffer - 1))
					sprintf(Buf + strlen(Buf), "%02X", ((int)(bufline[n])) & 0xff);
				else
					sprintf(Buf + strlen(Buf), "%02X,", ((int)(bufline[n])) & 0xff);
			}
			else if (g_dwLogMode == 2)
				sprintf(Buf + strlen(Buf), "0x%02X,", ((int)(bufline[n])) & 0xff);
			else if (g_dwLogMode == 5)
				sprintf(Buf + strlen(Buf), "%02X", ((int)(bufline[n])) & 0xff);
		}
		else
		{
			if (g_dwLogMode == 0)
				strcat(Buf, "   ");
			else if (g_dwLogMode == 2)
				strcat(Buf, "     ");
			else if (g_dwLogMode == 5)
				strcat(Buf, "  ");
		}
	}
	if ((g_dwLogMode == 0) || (g_dwLogMode == 2))
	{
		if (g_dwLogMode == 2)
		{
			strcat(Buf, "  // ");
		}

		strcat(Buf, " |");
		for (int n = 0; n < nColCount; n++)
		{
			if (IS_BLOCK_SEPARATOR(n)) strcat(Buf, " ");
			if (n < nLenBuffer)
			{
				int nLenBuf = (int)strlen(Buf);
				Buf[nLenBuf] = IsPrintable(bufline[n]) ? bufline[n] : '.';
				Buf[nLenBuf + 1] = '\0';
			}
			else
				strcat(Buf, " ");
		}
		strcat(Buf, "|");
	}
	else if (g_dwLogMode == 5)
	{
		strcat(Buf, "  #  ");
		for (int n = 0; n < nColCount; n++)
		{
			if (IS_BLOCK_SEPARATOR(n)) strcat(Buf, " ");
			if (n < nLenBuffer)
			{
				sprintf(Buf + strlen(Buf), "%02X ", ((int)(bufline[n])) & 0xff);
			}
			else
				strcat(Buf, "   ");
		}
		strcat(Buf, " '");
		//		
		for (int n = 0; n < nColCount; n++)
		{
			if (IS_BLOCK_SEPARATOR(n)) strcat(Buf, " ");
			if (n < nLenBuffer)
			{
				int nLenBuf = (int)strlen(Buf);
				Buf[nLenBuf] = IsPrintable(bufline[n]) ? bufline[n] : '.';
				Buf[nLenBuf + 1] = '\0';
			}
			else
				strcat(Buf, " ");
		}
		strcat(Buf, "'");
	}
	ftLog.Puts(Buf);
}

void DumpFrame(const char* szTitle, CFileText& ftLog, const char* szBuffer, int nLenBuffer2, BOOL fServerOut, int nColCount, int nBlockBreak)
{
	int nLen = nLenBuffer2;
	if (g_dwLogMode == 0)
	{
		ftLog.Putsf("%s - %s", GetDateTimeStr(), szTitle);
		ftLog.Putsf("******************** %4d octets ********************", nLenBuffer2);
	}
	else if (g_dwLogMode == 1)
	{
		ftLog.Putsf("# %s - %s", GetDateTimeStr(), szTitle);
		ftLog.Putsf("# ******************** %4d octets ********************", nLenBuffer2);
	}
	else if (g_dwLogMode == 2)
	{
		ftLog.Putsf("\t//	%s - %s", GetDateTimeStr(), szTitle);
		ftLog.Putsf("\t//	******************** %4d octets ********************", nLenBuffer2);
		ftLog.Putsf("\tBYTE buf_%s[] = {", szTitle);
	}
	for (;;)
	{
		if (nLen <= 0) break;
		DumpLine(ftLog, nColCount, nBlockBreak, szBuffer, nLen < nColCount ? nLen : nColCount);
		nLen -= nColCount;
		szBuffer += nColCount;
	}
	if (g_dwLogMode == 0)
	{
		ftLog.Putsf("*****************************************************");
	}
	else if (g_dwLogMode == 1)
	{
		ftLog.Putsf("# *****************************************************");
	}
	else if (g_dwLogMode == 2)
	{
		ftLog.Putsf("\t\t};");
		ftLog.Putsf("\t// buf_%s **************** %4d octets ***************************", szTitle, nLenBuffer2);
		ftLog.Putsf("");
		if (fServerOut)
			ftLog.Putsf("\tSERVER_OUT(buf_%s, sizeof(buf_%s));", szTitle, szTitle);
		else
			ftLog.Putsf("\tCLIENT_OUT(buf_%s, sizeof(buf_%s));", szTitle, szTitle);
		ftLog.Putsf("");
	}
}

DWORD WINAPI NatTcpThread(LPVOID lpParameter)
{
	int nComID = ++g_nComID;

	SACCEPT& sAccept = *((SACCEPT*)lpParameter);
	GetLocalTime(&sAccept.m_stStartTime);
	/*
		addrPeerClient ---> addrLocalClient (NAT) addrLocalServer ---> addrPeerServer
	*/
	CSock& sockClient = *sAccept.m_psockAccept;
	CSock& sockConnect = sAccept.m_sockConnect;

	CSockAddr addrLocalClient, addrPeerClient, addrLocalServer, addrPeerServer;
	CSockPort portLocalClient, portPeerClient, portLocalServer, portPeerServer;

	sAccept.m_psockAccept->GetSockName(addrLocalClient, portLocalClient);
	sAccept.m_psockAccept->GetPeerName(addrPeerClient, portPeerClient);

	CString sTitle;
	sTitle.Format("Accept NatTCP %s:%u peer %s:%u --> %s:%s",
		(LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.HostNumber(),
		(LPCTSTR)addrPeerClient.String(), (LPCTSTR)portPeerClient.HostNumber(),
#if defined _NATLOAD
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sAddrConnect,
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sPortConnect);
#elif defined _NAT
		g_nat[sAccept.m_nSNatIndex].m_sAddrConnect,
		g_nat[sAccept.m_nSNatIndex].m_sPortConnect);
#else
		PP_ERROR
#endif
		sAccept.m_psockAccept->SetSockTitle(sTitle);

#ifdef _DEBUG
	printf("%s [%d] NAT: %s:%s peer %s:%s (E:%c F:%c)\n", (LPCTSTR)GetDateTimeStr(), nComID,
		(LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.String(), (LPCTSTR)addrPeerClient.String(), (LPCTSTR)portPeerClient.String(),
		sAccept.m_fLogExchanges ? 'Y' : 'N', sAccept.m_fDumpFrames ? 'Y' : 'N');
#endif
	Logfn("%s [%d] NAT: starting thread with %s:%s peer %s:%s (E:%c F:%c)", (LPCTSTR)GetDateTimeStr(), nComID,
		(LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.String(), (LPCTSTR)addrPeerClient.String(), (LPCTSTR)portPeerClient.String(),
		sAccept.m_fLogExchanges ? 'Y' : 'N', sAccept.m_fDumpFrames ? 'Y' : 'N');

	//	Connection
	sTitle.Format("Connect TCP %s:%s --> %s:%s out:%s",
		(LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.String(),
#if defined _NATLOAD
		(LPCTSTR)g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sAddrConnect,
		(LPCTSTR)g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sPortConnect,
#elif defined _NAT
		(LPCTSTR)g_nat[sAccept.m_nSNatIndex].m_sAddrConnect,
		(LPCTSTR)g_nat[sAccept.m_nSNatIndex].m_sPortConnect,
#else
		PP_ERROR,
#endif
		g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : (LPCTSTR)g_nat[sAccept.m_nSNatIndex].m_sAddrOut);
	sockConnect.SetSockTitle(sTitle);

	sockConnect.Create();
	if (g_nat[sAccept.m_nSNatIndex].m_sAddrOut != "" || g_nat[sAccept.m_nSNatIndex].IsBindPrivatePort())
	{
		WORD portbind = 0;
		CString addrbind = "0.0.0.0";
		if (g_nat[sAccept.m_nSNatIndex].IsBindPrivatePort())
		{
			Logfn("%s [%d] NAT: Binding port forced.", (LPCTSTR)GetDateTimeStr(), nComID);
			portbind = 1023;
		}
		if (g_nat[sAccept.m_nSNatIndex].m_sAddrOut != "")
		{
			Logfn("%s [%d] NAT: Binding address forced on %s...", GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sAddrOut);
			addrbind = (const char*)g_nat[sAccept.m_nSNatIndex].m_sAddrOut;
		}
		for (;;)
		{
			if (sockConnect.Bind(CSockAddr(addrbind), CSockPort(portbind))) break;
			if (sockConnect.GetLastError() != WSAEADDRINUSE || !g_nat[sAccept.m_nSNatIndex].IsBindPrivatePort())
			{
				Logfn("%s [%d] NAT: Error while binding on out:%s (%s)", GetDateTimeStr(), nComID,
					addrbind, sockConnect.GetLastErrorString());
				sAccept.Close();
				return 1;
			}
			portbind--;
		}
		Logfn("%s [%d] NAT: Bind on %s:%d ok !", GetDateTimeStr(), nComID, addrbind, portbind);
	}
	Logfn("%s [%d] NAT: Trying to connect from  %s:%s to %s:%s out:%s...", GetDateTimeStr(), nComID,
		addrLocalClient.String(), portLocalClient.String(),
#if defined _NATLOAD
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sAddrConnect,
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sPortConnect,
#elif defined _NAT
		g_nat[sAccept.m_nSNatIndex].m_sAddrConnect,
		g_nat[sAccept.m_nSNatIndex].m_sPortConnect,
#else
		PP_ERROR,
#endif
		g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : g_nat[sAccept.m_nSNatIndex].m_sAddrOut);
	if (!sockConnect.Connect(CSockAddr(
#if defined _NATLOAD
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sAddrConnect
#elif defined _NAT
		g_nat[sAccept.m_nSNatIndex].m_sAddrConnect
#else
		PP_ERROR,
#endif
	), CSockPort(
#if defined _NATLOAD
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sPortConnect
#elif defined _NAT
		g_nat[sAccept.m_nSNatIndex].m_sPortConnect
#else
		PP_ERROR,
#endif
	)))
	{
		Logfn("%s [%d] NAT: Error while connecting from %s:%s to %s:%s out:%s (%s)", GetDateTimeStr(), nComID, addrLocalClient.String(), portLocalClient.String(),
#if defined _NATLOAD
			g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sAddrConnect,
			g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sPortConnect,
#elif defined _NAT
			g_nat[sAccept.m_nSNatIndex].m_sAddrConnect,
			g_nat[sAccept.m_nSNatIndex].m_sPortConnect,
#else
			PP_ERROR,
#endif
			g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : g_nat[sAccept.m_nSNatIndex].m_sAddrOut,
			sockConnect.GetLastErrorString());
		sAccept.Close();
		return 1;
	}
	Logfn("%s [%d] NAT: Connection from %s:%s to %s:%s out:%s ok!", GetDateTimeStr(), nComID, addrLocalClient.String(), portLocalClient.String(),
#if defined _NATLOAD
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sAddrConnect,
		g_nat[sAccept.m_nSNatIndex].m_loadaddr[sAccept.m_nLoadIndex].m_sPortConnect,
#elif defined _NAT
		g_nat[sAccept.m_nSNatIndex].m_sAddrConnect,
		g_nat[sAccept.m_nSNatIndex].m_sPortConnect,
#else
		PP_ERROR,
#endif
		g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : g_nat[sAccept.m_nSNatIndex].m_sAddrOut);

	sockConnect.GetSockName(addrLocalServer, portLocalServer);
	sockConnect.GetPeerName(addrPeerServer, portPeerServer);
	sTitle.Format("Connect TCP %s:%s peer %s:%s", (LPCSTR)addrLocalClient.String(), (LPCSTR)portLocalClient.String(), (LPCSTR)addrPeerServer.String(), (LPCSTR)portPeerServer.String());
	sockConnect.SetSockTitle(sTitle);

	if (sAccept.m_fLogExchanges)
	{
		if (g_dwLogMode == 5)
		{
			sAccept.m_logfile.Putsf("^SOF - %s", GetDateTimeStr());
		}
	}
	DWORD bufSize = g_nat[sAccept.m_nSNatIndex].m_dwBufferSize;
	BYTE* bufferFrame = new BYTE[bufSize];
	int nLenRead, nLenWrite;

	//	Comptabilisation du nombre de connexions totales
#if defined _NATLOAD
	g_nat[sAccept.m_nSNatIndex].IncTotalConnections(sAccept.m_nLoadIndex);
#elif defined _NAT
	g_nat[sAccept.m_nSNatIndex].IncTotalConnections();
#else
	PP_ERROR,
#endif
		int nIndexBufIn = 1;
	int nIndexBufOut = 1;
	ASSERT(g_hEventServerWantToStop);
	LONG lRetEvent;

	sAccept.m_dwServerTotalBytesOut = sAccept.m_dwServerTotalBytesIn = sAccept.m_dwClientTotalBytesOut = sAccept.m_dwClientTotalBytesIn = 0;
	struct timeval tv = { 0, 10 };	// 10ms
	fd_set fdread;
	int sel;
	for (;;)
	{
		lRetEvent = WaitForSingleObject(g_hEventServerWantToStop, 0);
		if ((lRetEvent - WAIT_OBJECT_0) == 0)
		{
			Logfn("%s [%d] NAT: StopAll: Stopping connection from %s:%s to %s:%s out:%s", GetDateTimeStr(), nComID,
				addrLocalClient.String(), portLocalClient.String(),
				addrPeerServer.String(), portPeerServer.String(),
				g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : g_nat[sAccept.m_nSNatIndex].m_sAddrOut);
			break;
		}
		if (!sAccept.m_fStarted || !g_nat[sAccept.m_nSNatIndex].IsStarted())
		{
#ifdef _DEBUG
			Logfn("sAccept.m_fStarted=%d g_nat[sAccept.m_nSNatIndex].IsStarted()=%d", sAccept.m_fStarted, g_nat[sAccept.m_nSNatIndex].IsStarted());
#endif
#if defined _NATLOAD
			Logfn("%s [%d] NAT: (ConfID:%d:%d) Connexion de %s:%s sur %s:%s out:%s killed", GetDateTimeStr(), nComID,
				sAccept.m_nSNatIndex, sAccept.m_nLoadIndex,
#elif defined _NAT
			Logfn("%s [%d] NAT: (ConfID:%d) Connection from %s:%s to %s:%s out:%s killed", GetDateTimeStr(), nComID,
				sAccept.m_nSNatIndex,
#else
			PP_ERROR,
#endif
				addrLocalClient.String(), portLocalClient.String(),
				addrPeerServer.String(), portPeerServer.String(),
				g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : g_nat[sAccept.m_nSNatIndex].m_sAddrOut);
	break;
		}
		FD_ZERO(&fdread);
		FD_SET(sockClient.GetSocketID(), &fdread);
		FD_SET(sockConnect.GetSocketID(), &fdread);
		sel = select(0, &fdread, NULL, NULL, &tv);
		if (FD_ISSET(sockClient.GetSocketID(), &fdread))
		{
			if (!sockClient.Recv(bufferFrame, bufSize, nLenRead))
			{
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 1)
					{
						sAccept.m_logfile.Print("# ");
					}
					else if (g_dwLogMode == 2)
					{
						sAccept.m_logfile.Print("\t// ");
					}
					sAccept.m_logfile.Putsf("%s Error while reading socket '%s' (%s)", GetDateTimeStr(), sockClient.GetSockTitle(), sockClient.GetLastErrorString());
				}
				if (sockClient.GetLastError() != WSAEWOULDBLOCK)
				{
					Logfn("%s [%d] NAT: Error while reading socket '%s' (%s)", GetDateTimeStr(), nComID, sockClient.GetSockTitle(), sockClient.GetLastErrorString());
					break;
				}
				else
				{
					Logfn("%s [%d] NAT: socket '%s' (%s)", GetDateTimeStr(), nComID, sockClient.GetSockTitle(), sockClient.GetLastErrorString());
				}
			}
			else
			{
				if (nLenRead == 0)
				{
					if (sAccept.m_fLogExchanges)
					{
						if (g_dwLogMode == 1)
						{
							sAccept.m_logfile.Print("# ");
						}
						else if (g_dwLogMode == 2)
						{
							sAccept.m_logfile.Print("\t// ");
						}
						else if (g_dwLogMode == 5)
						{
							sAccept.m_logfile.Putsf("$EOF - %s", GetDateTimeStr());
							sAccept.m_logfile.Print("# ");
						}
						if (g_dwLogMode != 3 && g_dwLogMode != 4)
							sAccept.m_logfile.Putsf("%s Disconnection requested by '%s'", GetDateTimeStr(), sockClient.GetSockTitle());
					}
					Logfn("%s [%d] NAT: Disconnection requested by '%s'", GetDateTimeStr(), nComID, sockClient.GetSockTitle());
					break;	//	Sortie car fin de la comm.
				}
				sAccept.m_dwClientTotalBytesIn += nLenRead;
				sAccept.m_dwFramesIn++;
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 1)
					{
						sAccept.m_logfile.Print("# ");
					}
					else if (g_dwLogMode == 2)
					{
						sAccept.m_logfile.Print("\t// ");
					}
					if (g_dwLogMode < 3)
					{
						sAccept.m_logfile.Putsf(
							"%s (%d bytes) from "
							"%s.%s (local %s.%s) to "
							"%s.%s (local %s.%s)",
							GetDateTimeStr(), nLenRead,
							addrPeerClient.String(), portPeerClient.String(),
							addrLocalClient.String(), portLocalClient.String(),
							addrPeerServer.String(), portPeerServer.String(),
							addrLocalServer.String(), portLocalServer.String()
						);
					}
					else if (g_dwLogMode == 5)
					{
						sAccept.m_logfile.Putsf(
							"^SID %u bytes (Frame:%05u) dumped at %s - Client - from "
							"%s.%s (local %s.%s) to "
							"%s.%s (local %s.%s)",
							nLenRead,
							sAccept.m_dwFramesIn,
							GetDateTimeStr(),
							addrPeerClient.String(), portPeerClient.String(),
							addrLocalClient.String(), portLocalClient.String(),
							addrPeerServer.String(), portPeerServer.String(),
							addrLocalServer.String(), portLocalServer.String()
						);
					}
					Logfn(
						"%s [%d] NAT: %s (%d bytes) from "
						"%s.%s (local %s.%s) to "
						"%s.%s (local %s.%s)",
						GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead,
						addrPeerClient.String(), portPeerClient.String(),
						addrLocalClient.String(), portLocalClient.String(),
						addrPeerServer.String(), portPeerServer.String(),
						addrLocalServer.String(), portLocalServer.String()
					);
				}
				if (sAccept.m_fDumpFrames && (g_dwLogModeCS == 0 || g_dwLogModeCS == 1))
				{
					if (g_dwLogMode != 3 && g_dwLogMode != 4)
					{
						CString sTitle;
						sTitle.Format("ClientOut_%d", nIndexBufOut++);
						DumpFrame(sTitle, sAccept.m_logfile, (char*)bufferFrame, nLenRead, FALSE, g_dwLogColCount, g_dwLogBlockBreak);
					}
					else
					{
						if (g_dwLogMode == 4)
						{
							CString sTitle;
							sTitle.Format("\r\n==> ClientOut_%d <==\r\n", nIndexBufOut++);
							sAccept.m_logfile.Write((const char*)sTitle, sTitle.GetLength());
						}

						sAccept.m_logfile.Write(bufferFrame, nLenRead);
					}
				}
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 5)
					{
						sAccept.m_logfile.Putsf(
							"$EID %u bytes (Frame:%05u) dumped at %s - from "
							"%s.%s (local %s.%s) to "
							"%s.%s (local %s.%s)",
							nLenRead,
							sAccept.m_dwFramesIn,
							GetDateTimeStr(),
							addrPeerClient.String(), portPeerClient.String(),
							addrLocalClient.String(), portLocalClient.String(),
							addrPeerServer.String(), portPeerServer.String(),
							addrLocalServer.String(), portLocalServer.String()
						);
					}
				}
				sockConnect.Send(bufferFrame, nLenRead, nLenWrite);
				if (nLenRead != nLenWrite)
				{
					Logfn("%s [%d] NAT: %s ********* Read:%u Written:%u", GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead, nLenWrite);
				}
				sAccept.m_dwServerTotalBytesOut += nLenWrite;
			}
		}
		if (FD_ISSET(sockConnect.GetSocketID(), &fdread))
		{
			if (!sockConnect.Recv(bufferFrame, bufSize, nLenRead))
			{
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 1)
					{
						sAccept.m_logfile.Print("# ");
					}
					else if (g_dwLogMode == 2)
					{
						sAccept.m_logfile.Print("\t// ");
					}
					sAccept.m_logfile.Putsf("%s !Erreur: Probleme de lecture sur la socket '%s' (%s)", GetDateTimeStr(), sockConnect.GetSockTitle(), sockConnect.GetLastErrorString());
				}
				if (sockConnect.GetLastError() != WSAEWOULDBLOCK)
				{
					Logfn("%s [%d] NAT: !Erreur: Probleme de lecture sur la socket '%s' (%s)", GetDateTimeStr(), nComID, sockConnect.GetSockTitle(), sockConnect.GetLastErrorString());
					break;
				}
				else
				{
					Logfn("%s [%d] NAT: !Info: socket '%s' (%s)", GetDateTimeStr(), nComID, sockConnect.GetSockTitle(), sockConnect.GetLastErrorString());
				}
			}
			else
			{
				if (nLenRead == 0)
				{
					if (sAccept.m_fLogExchanges)
					{
						if (g_dwLogMode == 1)
						{
							sAccept.m_logfile.Print("# ");
						}
						else if (g_dwLogMode == 2)
						{
							sAccept.m_logfile.Print("\t// ");
						}
						else if (g_dwLogMode == 5)
						{
							sAccept.m_logfile.Putsf("$EOF - %s", GetDateTimeStr());
							sAccept.m_logfile.Print("# ");
						}
						sAccept.m_logfile.Putsf("%s Disconnection requested by '%s'", GetDateTimeStr(), sockConnect.GetSockTitle());
					}
					Logfn("%s [%d] NAT: Disconnection requested by '%s'", GetDateTimeStr(), nComID, sockConnect.GetSockTitle());
					break;	//	Sortie car fin de la comm.
				}
				sAccept.m_dwServerTotalBytesIn += nLenRead;
				sAccept.m_dwFramesOut++;
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 1)
					{
						sAccept.m_logfile.Print("# ");
					}
					else if (g_dwLogMode == 2)
					{
						sAccept.m_logfile.Print("\t// ");
					}
					if (g_dwLogMode < 3)
					{
						sAccept.m_logfile.Putsf(
							"%s (%d bytes) from "
							"%s.%s (local %s.%s) to "
							"%s.%s (local %s.%s)",
							GetDateTimeStr(), nLenRead,
							addrPeerServer.String(), portPeerServer.String(),
							addrLocalServer.String(), portLocalServer.String(),
							addrPeerClient.String(), portPeerClient.String(),
							addrLocalClient.String(), portLocalClient.String()
						);
					}
					else if (g_dwLogMode == 5)
					{
						sAccept.m_logfile.Putsf(
							"^SOD %u bytes (Frame:%05u) dumped at %s - Server - from "
							"%s.%s (local %s.%s) to "
							"%s.%s (local %s.%s)",
							nLenRead,
							sAccept.m_dwFramesOut,
							GetDateTimeStr(),
							addrPeerServer.String(), portPeerServer.String(),
							addrLocalServer.String(), portLocalServer.String(),
							addrPeerClient.String(), portPeerClient.String(),
							addrLocalClient.String(), portLocalClient.String()
						);
					}
					Logfn(
						"%s [%d] NAT: %s (%d bytes) from "
						"%s.%s (local %s.%s) to "
						"%s.%s (local %s.%s)",
						GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead,
						addrPeerServer.String(), portPeerServer.String(),
						addrLocalServer.String(), portLocalServer.String(),
						addrPeerClient.String(), portPeerClient.String(),
						addrLocalClient.String(), portLocalClient.String()
					);
				}
				if (sAccept.m_fDumpFrames && (g_dwLogModeCS == 0 || g_dwLogModeCS == 2))
				{
					if (g_dwLogMode != 3 && g_dwLogMode != 4)
					{
						CString sTitle;
						sTitle.Format("ServerOut_%d", nIndexBufIn++);
						DumpFrame(sTitle, sAccept.m_logfile, (char*)bufferFrame, nLenRead, TRUE, g_dwLogColCount, g_dwLogBlockBreak);
					}
					else
					{
						if (g_dwLogMode == 4)
						{
							CString sTitle;
							sTitle.Format("\r\n==> ServerOut_%d <==\r\n", nIndexBufIn++);
							sAccept.m_logfile.Write((const char *)sTitle, sTitle.GetLength());
						}
						sAccept.m_logfile.Write(bufferFrame, nLenRead);
					}
				}
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 5)
					{
						sAccept.m_logfile.Putsf(
							"$EOD %u bytes (Frame:%05u) dumped at %s - from "
							"%s.%s (local %s.%s) to "
							"%s.%s (local %s.%s)",
							nLenRead,
							sAccept.m_dwFramesOut,
							GetDateTimeStr(),
							addrPeerServer.String(), portPeerServer.String(),
							addrLocalServer.String(), portLocalServer.String(),
							addrPeerClient.String(), portPeerClient.String(),
							addrLocalClient.String(), portLocalClient.String()
						);
					}
				}
				sockClient.Send(bufferFrame, nLenRead, nLenWrite);
				if (nLenRead != nLenWrite)
				{
					Logfn("%s [%d] NAT: %s ********* Read:%u Written:%u", GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead, nLenWrite);
				}
				sAccept.m_dwClientTotalBytesOut += nLenWrite;
			}
		}
		if (g_nat[sAccept.m_nSNatIndex].m_nCommDelay)
			Sleep(g_nat[sAccept.m_nSNatIndex].m_nCommDelay);
	}
	CDateRef drStart(sAccept.m_stStartTime);
	CDateRef drEnd, drDiff;
	drEnd.SetCurrentLocalTime();
	drDiff = drEnd - drStart;
	double dCoeff = ((drEnd - drStart) > 0 ? (drEnd - drStart) : 1) * 1000;
	Logfn("%s [%d] NAT: statistics time  : [start:%s end:%s  duration:%s]", GetDateTimeStr(), nComID, drStart.ToString(CDateRef::DateAndTime), drEnd.ToString(CDateRef::DateAndTime), drDiff.ToString(CDateRef::Ellapsed));
	Logfn("%s [%d] NAT: statistics bytes : [Client: In:%u Out:%u]-[Server: In:%u Out:%u]", GetDateTimeStr(), nComID, sAccept.m_dwClientTotalBytesIn, sAccept.m_dwClientTotalBytesOut, sAccept.m_dwServerTotalBytesIn, sAccept.m_dwServerTotalBytesOut);
	Logfn("%s [%d] NAT: statistics frames: [Client: In:%u Server In:%u]", GetDateTimeStr(), nComID, sAccept.m_dwFramesIn, sAccept.m_dwFramesOut);
	Logfn("%s [%d] NAT: statistics kb/s  : [Client: In:%0.3lf Out:%0.3lf]-[Server: In:%0.3lf Out:%0.3lf]", GetDateTimeStr(), nComID, sAccept.m_dwClientTotalBytesIn / dCoeff, sAccept.m_dwClientTotalBytesOut / dCoeff, sAccept.m_dwServerTotalBytesIn / dCoeff, sAccept.m_dwServerTotalBytesOut / dCoeff);
	Logfn("%s [%d] NAT: end of thread", GetDateTimeStr(), nComID);
	SaveSession(nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName,
		addrPeerClient.String(), portPeerClient.String(),
		addrLocalClient.String(), portLocalClient.String(),
		addrLocalServer.String(), portLocalServer.String(),
		addrPeerServer.String(), portPeerServer.String(),
		g_nat[sAccept.m_nSNatIndex].m_sAddrOut,
		drStart.ToString(CDateRef::DateAndTime),
		drEnd.ToString(CDateRef::DateAndTime),
		drDiff.ToString(CDateRef::Ellapsed),
		sAccept.m_dwClientTotalBytesIn, sAccept.m_dwClientTotalBytesOut, sAccept.m_dwServerTotalBytesIn, sAccept.m_dwServerTotalBytesOut,
		sAccept.m_dwFramesIn, sAccept.m_dwFramesOut,
		sAccept.m_dwClientTotalBytesIn / dCoeff, sAccept.m_dwClientTotalBytesOut / dCoeff, sAccept.m_dwServerTotalBytesIn / dCoeff, sAccept.m_dwServerTotalBytesOut / dCoeff);
	sAccept.Close();
	return 0;
}

#ifdef _NAT
DWORD WINAPI SerialCommTcpThread(LPVOID lpParameter)
{
	int nComID = ++g_nComID;

	SACCEPT& sAccept = *((SACCEPT*)lpParameter);
	GetLocalTime(&sAccept.m_stStartTime);

	CTelnetServer sockClient(sAccept.m_psockAccept);
	CSockAddr addrLocalClient, addrPeerClient;
	CSockPort portLocalClient, portPeerClient;
	sAccept.m_psockAccept->GetSockName(addrLocalClient, portLocalClient);
	sAccept.m_psockAccept->GetPeerName(addrPeerClient, portPeerClient);

	CString sCommPort = g_nat[sAccept.m_nSNatIndex].m_sAddrConnect;
	CString sCommParams = g_nat[sAccept.m_nSNatIndex].m_sPortConnect;

	CString sMsg;
	CString sTitle;
	sTitle.Format("Accept SerialCommTCP %s:%u peer %s:%u --> %s:%s", 
		(LPCTSTR)addrLocalClient.String(), portLocalClient.HostNumber(),
		(LPCTSTR)addrPeerClient.String(), portPeerClient.HostNumber(),
		(LPCTSTR)sCommPort, (LPCTSTR)sCommParams);
	sAccept.m_psockAccept->SetSockTitle(sTitle);

#ifdef _DEBUG
	printf("%s [%d] SerialCommTCP: %s:%s peer %s:%s (E:%c F:%c)\n", (LPCTSTR)GetDateTimeStr(), nComID,
		(LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.String(), (LPCTSTR)addrPeerClient.String(), (LPCTSTR)portPeerClient.String(),
		sAccept.m_fLogExchanges ? 'Y' : 'N', sAccept.m_fDumpFrames ? 'Y' : 'N');
#endif
	Logfn("%s [%d] SerialCommTCP: starting thread with %s:%s peer %s:%s (E:%c F:%c)", (LPCTSTR)GetDateTimeStr(), nComID,
		addrLocalClient.String(), portLocalClient.String(), addrPeerClient.String(), portPeerClient.String(),
		sAccept.m_fLogExchanges ? 'Y' : 'N', sAccept.m_fDumpFrames ? 'Y' : 'N');

	Logfn("%s [%d] SerialCommTCP: starting telnet protocol negociation ", (LPCTSTR)GetDateTimeStr(), nComID);
	sockClient.Negociate();
	Logfn("%s [%d] SerialCommTCP: end of telnet protocol negociation ", (LPCTSTR)GetDateTimeStr(), nComID);
	CSerial serial;
	BYTE Parity, ByteSize, StopBits;
	DWORD BaudRate;
	serial.DecodeCommStateString(sCommParams, &BaudRate, &Parity, &ByteSize, &StopBits);

	sTitle.Format("Opening CommPort %s:%s --> %s:%s",
		(LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.String(),
		(LPCTSTR)sCommPort, (LPCTSTR)sCommParams);

	Logfn("%s [%d] SerialCommTCP: trying connection from %s:%s to %s:%s...", GetDateTimeStr(), nComID,
		addrLocalClient.String(), portLocalClient.String(),
		sCommPort, sCommParams);
	if (!serial.Open(sCommPort))
	{
		Logfn("%s [%d] SerialCommTCP: unable to open communication port %s:%s (port may be already use by another process)", GetDateTimeStr(), nComID,
			sCommPort, sCommParams);
		sMsg.Format("\x1B[1;31m!Error! unable to open comm port %s:%s (port may be already use by another process)\x1b[0m\r\n",
			(LPCTSTR)sCommPort, (LPCTSTR)sCommParams);
		sockClient.Send(sMsg, (int)strlen(sMsg));
		Sleep(500);
		sAccept.Close();
		return 1;
	}
	if (!serial.SetCommTimeouts())
	{
		Logfn("%s [%d] SerialCommTCP: SetCommTimeouts %s:%s (%s)", GetDateTimeStr(), nComID,
			sCommPort, sCommParams, serial.GetLastErrorString());
		Sleep(500);
		sAccept.Close();
		return 1;
	}
	DCB dcb;
	serial.GetCommState(dcb);
	if (!serial.SetCommState(BaudRate, Parity, ByteSize, StopBits))
	{
		Logfn("%s [%d] SerialCommTCP: SetCommState %s:%s (%s)", GetDateTimeStr(), nComID,
			sCommPort, sCommParams, serial.GetLastErrorString());
		Sleep(500);
		sAccept.Close();
		return 1;
	}
	if (!serial.SetupComm())
	{
		Logfn("%s [%d] SerialCommTCP: SetupComm %s:%s (%s)", GetDateTimeStr(), nComID,
			sCommPort, sCommParams, serial.GetLastErrorString());
		Sleep(500);
		sAccept.Close();
		return 1;
	}
	Logfn("%s [%d] SerialCommTCP: Connexion de %s:%s sur %s:%s ok!", GetDateTimeStr(), nComID,
		addrLocalClient.String(), portLocalClient.String(),
		sCommPort, sCommParams);

	sMsg.Format("\x1B[1;32m!Info! you are connected on the serial interface '%s' on '%s' via %s:%s\x1B[0m\r\n\r\n",
		(LPCTSTR)sCommPort, (LPCTSTR)GetComputerNameStr(), (LPCTSTR)addrLocalClient.String(), (LPCTSTR)portLocalClient.String());
	sockClient.Send(sMsg, (int)strlen(sMsg));

	char szBuffer[2000];
	int nLenRead, nLenWrite;

	//	Comptabilisation du nombre de connexions totales
	g_nat[sAccept.m_nSNatIndex].IncTotalConnections();
	int nIndexBufIn = 1;
	int nIndexBufOut = 1;
	ASSERT(g_hEventServerWantToStop);
	LONG lRetEvent;

	sAccept.m_dwServerTotalBytesOut = sAccept.m_dwServerTotalBytesIn = sAccept.m_dwClientTotalBytesOut = sAccept.m_dwClientTotalBytesIn = 0;
	struct timeval tv = { 0, 10 };	// 10ms
	fd_set fdread;
	int sel;
	for (;;)
	{
		lRetEvent = WaitForSingleObject(g_hEventServerWantToStop, 0);
		if ((lRetEvent - WAIT_OBJECT_0) == 0)
		{
			Logfn("%s [%d] SerialCommTCP: StopAll: arr�t de la connexion de %s:%s sur %s:%s out:%s", GetDateTimeStr(), nComID,
				addrLocalClient.String(), portLocalClient.String(),
				sCommPort, sCommParams,
				g_nat[sAccept.m_nSNatIndex].m_sAddrOut == "" ? "[Default]" : g_nat[sAccept.m_nSNatIndex].m_sAddrOut);
			break;
		}
		if (!sAccept.m_fStarted || !g_nat[sAccept.m_nSNatIndex].IsStarted())
		{
#ifdef _DEBUG
			Logfn("sAccept.m_fStarted=%d g_nat[sAccept.m_nSNatIndex].IsStarted()=%d", sAccept.m_fStarted, g_nat[sAccept.m_nSNatIndex].IsStarted());
#endif
			Logfn("%s [%d] SerialCommTCP: (ConfID:%d) Connexion de %s:%s sur %s:%s killed", GetDateTimeStr(), nComID,
				sAccept.m_nSNatIndex,
				addrLocalClient.String(), portLocalClient.String(),
				sCommPort, sCommParams);
			break;
		}
		FD_ZERO(&fdread);
		FD_SET(sockClient.GetSocketID(), &fdread);
		//FD_SET((int)serial.GetCommHandle(), &fdread);
		sel = select(0, &fdread, NULL, NULL, &tv);
		if (FD_ISSET(sockClient.GetSocketID(), &fdread))
		{
			nLenRead = sockClient.Receive((BYTE*)szBuffer, sizeof(szBuffer));
			if (nLenRead < 0)
			{
				if (nLenRead < -1)
				{
					if (sAccept.m_fLogExchanges)
					{
						if (g_dwLogMode == 1)
						{
							sAccept.m_logfile.Print("# ");
						}
						else if (g_dwLogMode == 2)
						{
							sAccept.m_logfile.Print("\t// ");
						}
						sAccept.m_logfile.Putsf("%s !Erreur: Probleme de lecture sur la socket '%s' (%s)", GetDateTimeStr(), sockClient.GetSockTitle(), sockClient.GetLastErrorString());
					}
					if (sockClient.GetLastError() != WSAEWOULDBLOCK)
					{
						Logfn("%s [%d] SerialCommTCP: !Erreur: Probleme de lecture sur la socket '%s' (%s)", GetDateTimeStr(), nComID, sockClient.GetSockTitle(), sockClient.GetLastErrorString());
						break;
					}
					else
					{
						Logfn("%s [%d] SerialCommTCP: !Info: socket '%s' (%s)", GetDateTimeStr(), nComID, sockClient.GetSockTitle(), sockClient.GetLastErrorString());
					}
				}
				else
				{
					if (sAccept.m_fLogExchanges)
					{
						if (g_dwLogMode == 1)
						{
							sAccept.m_logfile.Print("# ");
						}
						else if (g_dwLogMode == 2)
						{
							sAccept.m_logfile.Print("\t// ");
						}
						sAccept.m_logfile.Putsf("%s Deconnexion demandee par '%s'", GetDateTimeStr(), sockClient.GetSockTitle());
					}
					Logfn("%s [%d] SerialCommTCP: Deconnexion demandee par '%s'", GetDateTimeStr(), nComID, sockClient.GetSockTitle());
					break;	//	Sortie car fin de la comm.
				}
			}
			if (nLenRead > 0)
			{
				sAccept.m_dwClientTotalBytesIn += nLenRead;
				sAccept.m_dwFramesIn++;
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 1)
					{
						sAccept.m_logfile.Print("# ");
					}
					else if (g_dwLogMode == 2)
					{
						sAccept.m_logfile.Print("\t// ");
					}
					sAccept.m_logfile.Putsf(
						"%s (%d octets) de "
						"%s.%s (local %s.%s) vers "
						"%s:%s",
						GetDateTimeStr(), nLenRead,
						addrPeerClient.String(), portPeerClient.String(),
						addrLocalClient.String(), portLocalClient.String(),
						sCommPort, sCommParams
					);
					Logfn(
						"%s [%d] SerialCommTCP: %s (%d octets) de "
						"%s.%s (local %s.%s) vers "
						"%s:%s",
						GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead,
						addrPeerClient.String(), portPeerClient.String(),
						addrLocalClient.String(), portLocalClient.String(),
						sCommPort, sCommParams
					);
				}
				if (sAccept.m_fDumpFrames)
				{
					CString sTitle;
					sTitle.Format("ClientOut_%d", nIndexBufOut++);
					DumpFrame(sTitle, sAccept.m_logfile, szBuffer, nLenRead, FALSE, g_dwLogColCount, g_dwLogBlockBreak);
				}
				serial.Write(szBuffer, nLenRead, (DWORD*)& nLenWrite);
				if (nLenRead != nLenWrite)
				{
					Logfn("%s [%d] SerialCommTCP: %s ********* Read:%u Written:%u", GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead, nLenWrite);
				}
				sAccept.m_dwServerTotalBytesOut += nLenWrite;
			}
		}
		//	Serial
		//if (FD_ISSET(sockConnect.GetSocketID(), &fdread))
		{
			if (!serial.Read(szBuffer, sizeof(szBuffer), (DWORD*)& nLenRead, 10))
			{
				if (serial.GetLastError() != ERROR_SUCCESS)
				{
					if (sAccept.m_fLogExchanges)
					{
						if (g_dwLogMode == 1)
						{
							sAccept.m_logfile.Print("# ");
						}
						else if (g_dwLogMode == 2)
						{
							sAccept.m_logfile.Print("\t// ");
						}
						sAccept.m_logfile.Putsf("%s !Erreur: Probleme de lecture sur la serialcomm '%s' (%s)", GetDateTimeStr(), g_nat[sAccept.m_nSNatIndex].m_sAddrConnect, serial.GetLastErrorString());
					}
					Logfn("%s [%d] SerialCommTCP: !Erreur: Probleme de lecture sur la serialcomm '%s' (%s)", GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sAddrConnect, serial.GetLastErrorString());
					break;
				}
			}
			else
			{
				sAccept.m_dwServerTotalBytesIn += nLenRead;
				sAccept.m_dwFramesOut++;
				if (sAccept.m_fLogExchanges)
				{
					if (g_dwLogMode == 1)
					{
						sAccept.m_logfile.Print("# ");
					}
					else if (g_dwLogMode == 2)
					{
						sAccept.m_logfile.Print("\t// ");
					}
					sAccept.m_logfile.Putsf(
						"%s (%d octets) de "
						"%s:%s vers "
						"%s.%s (local %s.%s)",
						GetDateTimeStr(), nLenRead,
						sCommPort, sCommParams,
						addrPeerClient.String(), portPeerClient.String(),
						addrLocalClient.String(), portLocalClient.String()
					);
					Logfn(
						"%s [%d] SerialCommTCP: %s (%d octets) de "
						"%s:%s vers "
						"%s.%s (local %s.%s)",
						GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead,
						sCommPort, sCommParams,
						addrPeerClient.String(), portPeerClient.String(),
						addrLocalClient.String(), portLocalClient.String()
					);
				}
				if (sAccept.m_fDumpFrames)
				{
					CString sTitle;
					sTitle.Format("ServerOut_%d", nIndexBufIn++);
					DumpFrame(sTitle, sAccept.m_logfile, szBuffer, nLenRead, TRUE, g_dwLogColCount, g_dwLogBlockBreak);
				}
				sockClient.Send(szBuffer, nLenRead);
				/*if (nLenRead != nLenWrite)
				{
					Logfn("%s [%d] SerialCommTCP: %s ********* Read:%u Written:%u", GetDateTimeStr(), nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName, nLenRead, nLenWrite);
				}*/
				sAccept.m_dwClientTotalBytesOut += nLenRead;
			}
		}
		if (g_nat[sAccept.m_nSNatIndex].m_nCommDelay)
			Sleep(g_nat[sAccept.m_nSNatIndex].m_nCommDelay);
	}
	CDateRef drStart(sAccept.m_stStartTime);
	CDateRef drEnd, drDiff;
	drEnd.SetCurrentLocalTime();
	drDiff = drEnd - drStart;
	double dCoeff = (double)(((drEnd - drStart) > 0 ? (drEnd - drStart) : 1) * 1000);
	Logfn("%s [%d] SerialCommTCP: statistics time  : [start:%s end:%s  duration:%s]", GetDateTimeStr(), nComID, drStart.ToString(CDateRef::DateAndTime), drEnd.ToString(CDateRef::DateAndTime), drDiff.ToString(CDateRef::Ellapsed));
	Logfn("%s [%d] SerialCommTCP: statistics bytes : [Client: In:%u Out:%u]-[Server: In:%u Out:%u]", GetDateTimeStr(), nComID, sAccept.m_dwClientTotalBytesIn, sAccept.m_dwClientTotalBytesOut, sAccept.m_dwServerTotalBytesIn, sAccept.m_dwServerTotalBytesOut);
	Logfn("%s [%d] SerialCommTCP: statistics frames: [Client: In:%u Server In:%u]", GetDateTimeStr(), nComID, sAccept.m_dwFramesIn, sAccept.m_dwFramesOut);
	Logfn("%s [%d] SerialCommTCP: statistics kb/s  : [Client: In:%0.3lf Out:%0.3lf]-[Server: In:%0.3lf Out:%0.3lf]", GetDateTimeStr(), nComID, sAccept.m_dwClientTotalBytesIn / dCoeff, sAccept.m_dwClientTotalBytesOut / dCoeff, sAccept.m_dwServerTotalBytesIn / dCoeff, sAccept.m_dwServerTotalBytesOut / dCoeff);
	Logfn("%s [%d] SerialCommTCP: end of thread", GetDateTimeStr(), nComID);
	SaveSession(nComID, g_nat[sAccept.m_nSNatIndex].m_sConfigName,
		addrPeerClient.String(), portPeerClient.String(),
		addrLocalClient.String(), portLocalClient.String(),
		sCommPort, sCommParams,
		"", "",
		"",
		drStart.ToString(CDateRef::DateAndTime),
		drEnd.ToString(CDateRef::DateAndTime),
		drDiff.ToString(CDateRef::Ellapsed),
		sAccept.m_dwClientTotalBytesIn, sAccept.m_dwClientTotalBytesOut, sAccept.m_dwServerTotalBytesIn, sAccept.m_dwServerTotalBytesOut,
		sAccept.m_dwFramesIn, sAccept.m_dwFramesOut,
		sAccept.m_dwClientTotalBytesIn / dCoeff, sAccept.m_dwClientTotalBytesOut / dCoeff, sAccept.m_dwServerTotalBytesIn / dCoeff, sAccept.m_dwServerTotalBytesOut / dCoeff);
	sAccept.Close();
	return 0;
}
#endif

void DisplayVersion()
{
	_tprintf(_T("%s Ver %s"), (LPCTSTR)GetAppName(), (LPCTSTR)GetAppVersion());
}

DWORD WINAPI MainThread(LPVOID p)
{
	CNTService& ntserv = *((CNTService*)p);

	LoadOptions();

	CSock::Startup();
	g_hEventServerWantToStop = ntserv.m_hEventServerWantToStop;

#ifdef _DEBUG
	CreateTraceFileName();
	LDA1F("--------------------------\n");
#endif

	Logfn("\n------------------------------------\n");
	Logfn("%s Ver %s", (LPCTSTR)GetAppName(), (LPCTSTR)GetAppVersion());

	char szUserName[50];
	DWORD dwcbSizeUserName = sizeof(szUserName);
	GetUserName(szUserName, &dwcbSizeUserName);
	Logfn("%s [0] D�marrage du service avec le compte: '%s'", GetDateTimeStr(), szUserName);
	Logfn("%s [0] Thread principal lance", GetDateTimeStr());

	//	Rechargement parametres
	Logfn("---------------------------------------------------------------------");
	LoadNatConfigurations();
	Logfn("---------------------------------------------------------------------");
#ifdef _DEBUG
	//g_nConfigPort = 1857;
#endif
	printf("Log Dir          : %s\n", (LPCTSTR)GetLogDir());
	printf("Dump Dir         : %s\n", (LPCTSTR)GetDumpDir());
	printf("Dat Dir          : %s\n", (LPCTSTR)GetDatDir());
	printf("AddrConfig       : %s\n", (LPCTSTR)g_sConfigAddr);
	printf("PortConfig       : %d\n", g_nConfigPort);
	printf("LogMode          : %d / %d\n", g_dwLogMode, g_dwLogModeCS);
	printf("LogColCount      : %d / %d\n", g_dwLogColCount, g_dwLogBlockBreak);
	printf("DumpLogFileFormat: %s \n", (LPCTSTR)g_sDumpLogFileFormat);
	printf("ExecOnAccept     : %s [%s]\n", (LPCTSTR)g_sExecOnAccept, (LPCTSTR)g_sExecOnAcceptRunTime);

	//	Socket pour admin. config
	CString sTitle;
	sTitle.Format("Config TCP %s:%u", (LPCTSTR)g_sConfigAddr, g_nConfigPort);

	CSock sockConfig(sTitle);
	if (!sockConfig.CreateTCPListen(CSockAddr(g_sConfigAddr), CSockPort(g_nConfigPort)))
	{
		LDA1F("%s [0] Creation socket Config impossible %s (%s)\n", (LPCTSTR)GetDateTimeStr(), sockConfig.GetSockTitle(), sockConfig.GetLastErrorString());
		Logfn("%s [0] Creation socket Config impossible %s (%s)", (LPCTSTR)GetDateTimeStr(), sockConfig.GetSockTitle(), sockConfig.GetLastErrorString());
		printf("%s [0] Creation socket Config impossible %s (%s)\n", (LPCTSTR)GetDateTimeStr(), (LPCTSTR)sockConfig.GetSockTitle(), (LPCTSTR)sockConfig.GetLastErrorString());
	}
	else
	{
		LDA1F("%s [0] Creation socket Config %s ok!\n", GetDateTimeStr(), sockConfig.GetSockTitle());
		Logfn("%s [0] Creation socket Config %s ok!", GetDateTimeStr(), sockConfig.GetSockTitle());
		printf("%s [0] Creation socket Config %s ok!\n", (LPCTSTR)GetDateTimeStr(), (LPCTSTR)sockConfig.GetSockTitle());
	}

	//	Creation des sockets en listen
	int nIndex;
	CString sError;
	for (nIndex = 0; nIndex < sizeof(g_nat) / sizeof(g_nat[0]); nIndex++)
	{
		if (g_nat[nIndex].IsUsed())
		{
			if (g_nat[nIndex].IsEnabled())
			{
				if (!g_nat[nIndex].Start(sError))
				{
					Logfn("%s [0] Demarrage socket '%s' impossible '%s'", GetDateTimeStr(), g_nat[nIndex].m_sockListen.GetSockTitle(), g_nat[nIndex].m_sockListen.GetLastErrorString());
				}
				else
				{
					Logfn("%s [0] Demarrage socket '%s' ok!", GetDateTimeStr(), g_nat[nIndex].m_sockListen.GetSockTitle());
				}
			}
		}
	}

#ifdef _NATLOAD
	//	Thread pour verif si les connexions sont possibles.
	CloseHandle(NewThread(CheckListenUpTCP, 0));
#endif

	//	Boucle principale
	BOOL fPurgeChecked = FALSE;
	SYSTEMTIME SysTime;
	CSockAddr sockaddrLocal, sockaddrPeer;
	CSockPort sockportLocal, sockportPeer;

#ifdef _DEBUG
	g_uPurgeLogDays = 2;
#endif

	ASSERT(g_hEventServerWantToStop);
	LONG lRetEvent;
	for (;;)
	{
		lRetEvent = WaitForSingleObject(g_hEventServerWantToStop, 1);
		if ((lRetEvent - WAIT_OBJECT_0) == 0) break;
		GetLocalTime(&SysTime);
		//	Purge ?
#ifdef _DEBUG
		//if ((SysTime.wSecond%10) == 0)
		if (SysTime.wMinute == 0)
#else
		if (SysTime.wHour == 0)
#endif
		{
			if (!fPurgeChecked)
			{
				//ExportConfig();
				PurgeLogs();
				fPurgeChecked = TRUE;
			}
		}
		else
		{
			fPurgeChecked = FALSE;
		}

		//	Connexion admin est-elle arriv�e?
		if (sockConfig.IsReadReady())
		{
			Logfn("%s [0] Demande de connexion admin sur '%s'", GetDateTimeStr(), sockConfig.GetSockTitle());
			CSock* pSockAccept;
			if (!sockConfig.Accept(&pSockAccept, "AcceptConfig"))
			{
				Logfn("%s [0] Erreur Accept sur %s", GetDateTimeStr(), sockConfig.GetSockTitle());
			}
			CloseHandle(NewThread(CConfig::ConfigThreadTCP, pSockAccept));
		}

		//	Connexion NAT est-elle arriv�e ?
		for (nIndex = 0; nIndex < sizeof(g_nat) / sizeof(g_nat[0]); nIndex++)
		{
			if (g_nat[nIndex].IsStarted())
			{
				if (g_nat[nIndex].m_sockListen.IsReadReady())
				{
					CString sSockTitle;
					sSockTitle.Format("Accept %s:%s", (LPCTSTR)g_nat[nIndex].m_sAddrListen, (LPCTSTR)g_nat[nIndex].m_sPortListen);

					// Accept ici sinon generation de plusieurs thread.
					Logfn("%s [0] Accept sur Config '%s' (Adresse:port %s:%s)", GetDateTimeStr(), g_nat[nIndex].m_sConfigName, g_nat[nIndex].m_sAddrListen, g_nat[nIndex].m_sPortListen);
					LDA1F("%s [0] Accept sur Config '%s' (Adresse:port %s:%s)\n", GetDateTimeStr(), g_nat[nIndex].m_sConfigName, g_nat[nIndex].m_sAddrListen, g_nat[nIndex].m_sPortListen);
					CSock* psockAccept;
					if (!g_nat[nIndex].m_sockListen.Accept(&psockAccept, sSockTitle))
					{
						Logfn("%s [0] NatTcpThread: erreur sur Accept sur %s.%s", GetDateTimeStr(), g_nat[nIndex].m_sAddrListen, g_nat[nIndex].m_sPortListen);
					}
					else
					{
						if (g_nat[nIndex].IsEnabled())
						{
							//	Recupere addresse/port de l'hote voulant se connecter
							psockAccept->GetPeerName(sockaddrPeer, sockportPeer);
							if (!g_nat[nIndex].IsACLEnabled(sockaddrPeer.String()))
							{
								Logfn("%s [0] !Warning: Config '%s' ACL not authorized %s (connection refused)", GetDateTimeStr(), g_nat[nIndex].m_sConfigName, sockaddrPeer.String());
								g_nat[nIndex].m_nACLDropped++;
								delete psockAccept;
							}
							else
							{
								int nSubIndex;
								if (!g_nat[nIndex].GetFirstFreeSocketIndex(nSubIndex))
								{
									Logfn("%s [0] Erreur ajout socket accept dans tableau (plus de place %d)", GetDateTimeStr(), MAX_CONNECT);
									delete psockAccept;
								}
								else
								{
#ifdef _NATLOAD
									int nIndexLoad = ComputeLoad(g_nat[nIndex]);
									if (nIndexLoad == -1)
									{
										Logfn("%s [0] Config '%s' toutes les loads sont desactiv�s (connection refused)", GetDateTimeStr(), g_nat[nIndex].m_sConfigName);
										delete psockAccept;
									}
									else
									{
										g_nat[nIndex].m_sAccept[nSubIndex].m_nLoadIndex = nIndexLoad;
										Logfn("%s [0] Index de load choisi: %d", GetDateTimeStr(), g_nat[nIndex].m_sAccept[nSubIndex].m_nLoadIndex);
#endif
										g_nat[nIndex].m_sAccept[nSubIndex].Start(psockAccept, nIndex);
										CString sDumpFile;
										if (g_nat[nIndex].IsLogged())
										{
											//	Recupere addresse/port de l'hote local
											psockAccept->GetSockName(sockaddrLocal, sockportLocal);

											sDumpFile = ReplaceTokensFileName(g_sDumpLogFileFormat, sockaddrLocal.String(), sockportLocal.String(), sockaddrPeer.String(), sockportPeer.String());
											//sDumpFile.Format(_T("%s\\dump_%s.%s_%s.%s.log"),
											//	(LPCTSTR)GetDumpDir(),
											//	(LPCTSTR)sockaddrLocal.String(), (LPCTSTR)sockportLocal.String(),
											//	(LPCTSTR)sockaddrPeer.String(), (LPCTSTR)sockportPeer.String());
											Logfn("%s [0] Dump in file '%s'", GetDateTimeStr(), sDumpFile);
											g_nat[nIndex].m_sAccept[nSubIndex].StartLog(sDumpFile);
										}
										if (g_sExecOnAcceptRunTime != _T(""))
										{
											CString sCommand;
											sCommand.Format(_T("\"%s\" \"%s\""), (LPCSTR)g_sExecOnAcceptRunTime, (LPCSTR)sDumpFile);
											ExecuteModule(sCommand, GetModuleDirectoryStr(), false, SW_HIDE);
										}
#ifdef _NAT
										if (IsCommPort(g_nat[nIndex].m_sAddrConnect))
											CloseHandle(NewThread(SerialCommTcpThread, &g_nat[nIndex].m_sAccept[nSubIndex]));
										else
#endif
											CloseHandle(NewThread(NatTcpThread, &g_nat[nIndex].m_sAccept[nSubIndex]));
#ifdef _NATLOAD
									}
#endif										
								}
							}
						}
						else
						{
							Logfn("%s [0] Config '%s' disabled (connection refused)", GetDateTimeStr(), g_nat[nIndex].m_sConfigName);
							delete psockAccept;
						}
					}
				}
				Sleep(1);
			}
		}
		Sleep(1);
	}

	Logfn("%s [0] Thread principal termine", GetDateTimeStr());
	SetEvent(ntserv.m_hEventClientThreadStopped);
	return 0;
}
