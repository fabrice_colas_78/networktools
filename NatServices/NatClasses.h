// NatClasses.h: interface for the CNatClasses class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NATCLASSES_H__83B1FDE3_1C31_4CF0_A06E_A12616887615__INCLUDED_)
#define AFX_NATCLASSES_H__83B1FDE3_1C31_4CF0_A06E_A12616887615__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <LibSystem.h>
#include <fields.h>
#include <sock.h>
#include <filetext.h>

class SACCEPT
{
public:
	SACCEPT()
	{
		m_fStarted      = TRUE;
		m_fDumpFrames   = FALSE;
		m_fLogExchanges = FALSE;
		m_psockAccept   = NULL;
		m_dwFramesIn    = m_dwFramesOut = 0;
	}
	~SACCEPT()
	{
		Close();
	}
	BOOL IsFree()
	{
		return m_psockAccept == NULL;
	}
	BOOL IsUsed()
	{
		return !IsFree();
	}
	void Start(CSock * psockAccept, int nSNATIndex)
	{
		m_fStarted    = TRUE;
		m_dwFramesIn  = m_dwFramesOut = 0;
		m_psockAccept = psockAccept;
		m_nSNatIndex  = nSNATIndex;
	}
	void Close()
	{
		if (m_sockConnect.IsSocket())  m_sockConnect.Close();
		if (m_psockAccept)
		{
			m_psockAccept->Close();
			delete m_psockAccept;
			m_psockAccept = NULL;
		}
		if (m_logfile.IsOpened()) m_logfile.Close();
		m_sLogFile = "";
		m_dwFramesIn = m_dwFramesOut = 0;
	}
	BOOL StartLog(const char * szLogFile)
	{
		if (! m_logfile.OpenCreate(szLogFile, CFile::modeWrite | CFile::shareDenyNone))
		{
			return FALSE;
		}
		m_sLogFile      = szLogFile;
		m_fDumpFrames   = TRUE;
		m_fLogExchanges = TRUE;
		return TRUE;
	}
	void StopLog()
	{
		m_logfile.Close();
		m_sLogFile      = "";
		m_fDumpFrames   = FALSE;
		m_fLogExchanges = FALSE;
	}

	CSock * m_psockAccept;
	CSock m_sockConnect;

	BOOL m_fStarted;
	BOOL m_fDumpFrames;
	BOOL m_fLogExchanges;

	CString m_sLogFile;
	CFileText m_logfile;

	DWORD m_dwFramesIn, m_dwFramesOut;
	DWORD m_dwServerTotalBytesOut, m_dwServerTotalBytesIn;
	DWORD m_dwClientTotalBytesOut, m_dwClientTotalBytesIn;
	SYSTEMTIME m_stStartTime;

	int m_nSNatIndex;
#ifdef _NATLOAD
	int m_nLoadIndex;
#endif
};

#ifdef _NATLOAD
class SLoadAddr
{
public:
	SLoadAddr()
	{
		m_fStarted   = FALSE;
		m_fActivated = FALSE;
		m_fListenUp  = FALSE;
		m_wWeight    = 1;
		m_dwTotal    = 0;
	}
	void Reset()
	{
		m_fStarted     = FALSE;
		m_fActivated   = FALSE;
		m_fListenUp    = FALSE;
		m_sAddrConnect = "";
		m_sPortConnect = "";
		m_dwTotal      = 0;
	}
	void Set(const char * szAddress, const char * szPort)
	{
		m_fActivated   = TRUE;
		m_fListenUp    = TRUE;
		m_fStarted     = TRUE;
		m_sAddrConnect = szAddress;
		m_sPortConnect = szPort;
		m_dwTotal      = 0;
	}
	BOOL IsFree()
	{
		return m_sAddrConnect == "";
	}
	BOOL IsUsed()
	{
		return !IsFree();
	}
	BOOL IsActivated()
	{
		return m_fActivated;
	}
	void IncTotal()
	{
		m_dwTotal++;
	}
	DWORD GetTotal()
	{
		return m_dwTotal;
	}
	void ClearTotal()
	{
		m_dwTotal = 0;
	}
	CString m_sAddrConnect;
	CString m_sPortConnect;
	BOOL m_fActivated;
	BOOL m_fStarted;
	BOOL m_fListenUp;
	WORD m_wWeight;
private:
	DWORD m_dwTotal;
};
#endif

#if defined _NATLOAD
class CNatLoad
#elif defined _NAT
class CNat
#else
class CNatServices
#endif
{
public:
#if defined _NATLOAD
	CNatLoad()
#elif defined _NAT
	CNat()
#else
	CNatServices()
#endif
	{
		m_fEnabled     = FALSE;
		m_fStarted     = FALSE;
		m_fToBeDeleted = FALSE;
		m_fToBeLogged  = FALSE;
		m_nACLDropped  = 0;
		m_nCommDelay   = 0;
#ifdef _NAT
		m_nTotCon      = 0;
#endif
#ifdef _DEBUG
		m_nACLDropped  = 20;
#endif
		m_fBindPrivatePort = FALSE;
		m_dwBufferSize     = 2048;
	}
#if defined _NATLOAD
	DWORD GetAllActiveConnectionsCount()
	{
		DWORD dwTotal = 0;
		for(int j=0; j<MAX_LOAD_ADDR; j++)
		{
			if (m_loadaddr[j].m_sAddrConnect != "")
			{
				dwTotal += GetActiveConnectionsCount(j);
			}
		}
		return dwTotal;
	}
	DWORD GetAllTotalConnectionsCount()
	{
		DWORD dwTotal = 0;
		for(int j=0; j<MAX_LOAD_ADDR; j++)
		{
			if (m_loadaddr[j].m_sAddrConnect != "")
			{
				dwTotal += GetTotalConnectionsCount(j);
			}
		}
		return dwTotal;
	}
#endif
#if defined _NATLOAD
	DWORD GetActiveConnectionsCount(int nIndexLoad)
#elif defined _NAT
	DWORD GetActiveConnectionsCount()
#else
		ERROR,
#endif
	{
		DWORD dwActive = 0;
		for(int i=0; i<MAX_CONNECT; i++)
		{
			if (! m_sAccept[i].IsFree())
			{
#if defined _NATLOAD
				if (m_sAccept[i].m_nLoadIndex == nIndexLoad) dwActive++;
#elif defined _NAT
				dwActive++;
#else
				ERROR,
#endif
			}
		}
		return dwActive;
	}
	void ClearTotalConnectionsCount()
	{
#if defined _NATLOAD
		for(int i=0; i<MAX_LOAD_ADDR; i++)
		{
			m_loadaddr[i].ClearTotal();
		}
#elif defined _NAT
		m_nTotCon = 0;
#else
				ERROR,
#endif
	}
#if defined _NATLOAD
	DWORD GetTotalConnectionsCount(int nIndexLoad)
#elif defined _NAT
	DWORD GetTotalConnectionsCount()
#else
		ERROR,
#endif
	{
		DWORD dwTotal = 0;
		
#if defined _NATLOAD
		dwTotal = m_loadaddr[nIndexLoad].GetTotal();
#elif defined _NAT
		dwTotal = m_nTotCon;
#else
				ERROR,
#endif
		//printf("Return: %d\n", dwTotal);
		return dwTotal;
	}
#if defined _NATLOAD
	void IncTotalConnections(int nIndexLoad)
#elif defined _NAT
	void IncTotalConnections()
#else
		ERROR,
#endif
	{
#if defined _NATLOAD
		m_loadaddr[nIndexLoad].IncTotal();
#elif defined _NAT
		m_nTotCon++;
#else
		ERROR,
#endif
	}

#if defined _NATLOAD
	void DeleteLoad(int nIndexLoad)
	{
		ClearConnections(nIndexLoad);
		Sleep(200);	//	Delete Load: apres ClearConnections
		m_loadaddr[nIndexLoad].Reset();
	}		
#endif

#if defined _NATLOAD
	void ClearConnections(int nIndexLoad)
#elif defined _NAT
	void ClearConnections()
#else
				ERROR,
#endif
	{
		for(int i=0; i<MAX_CONNECT; i++)
		{
			if (! m_sAccept[i].IsFree())
			{
#if defined _NATLOAD
				if (m_sAccept[i].m_nLoadIndex == nIndexLoad) m_sAccept[i].m_fStarted = FALSE;
#elif defined _NAT
				m_sAccept[i].m_fStarted = FALSE;
#endif
			}
		}
	}

	BOOL AlterConfListen(const char * szAddrListen, const char * szPortListen)
	{
		if (IsStarted()) return FALSE;
		m_sAddrListen  = szAddrListen;
		m_sPortListen  = szPortListen;
		return TRUE;
	}
#if defined _NAT
	BOOL AlterConfConnect(const char * szAddrConnect, const char * szPortConnect)
	{
		if (IsStarted()) return FALSE;
		m_sAddrConnect = szAddrConnect;
		m_sPortConnect = szPortConnect;
		return TRUE;
	}
#endif
	BOOL AlterConfAddrOut(const char * szAddrOut)
	{
		if (IsStarted()) return FALSE;
		m_sAddrOut     = szAddrOut;
		return TRUE;
	}

	BOOL SetConf(const char * szConfigName, const char * szAddrPortListen, const char * szAddrPortConnect,  
#if defined _NATLOAD
		const char * szAddrConnectActivated, const char * szAddrConnectWeight, 
#endif
		const char * szAddrOut, int nCommDelay, BOOL fEnabled, BOOL fLogged)
	{
		if (IsUsed()) return FALSE;

		CFields parListen(szAddrPortListen, ':');
		m_fEnabled     = fEnabled;
		m_fToBeLogged  = fLogged;
		m_sConfigName  = szConfigName;
		m_sAddrListen  = parListen[0];
		m_sPortListen  = parListen[1];
		m_sAddrOut     = szAddrOut;
		m_nCommDelay   = nCommDelay;

#ifdef _NATLOAD
		ResetAllLoadAddresses();
		CFields fldaddrport(szAddrPortConnect, ','),
			fldactivated(szAddrConnectActivated, ','),
			fldweight(szAddrConnectWeight, ',');
		for(int i=0; i<fldaddrport.GetSize(); i++)
		{
			if (fldaddrport[i] != "")
			{
				CFields parConnect(fldaddrport[i], ':');
				AddLoadAddress(parConnect[0], parConnect[1], fldactivated[i] == "Y"?TRUE:FALSE, atoi(fldweight[i]));
			}
		}
#endif
#ifdef _NAT
		CFields parConnect(szAddrPortConnect, ':');
		m_sAddrConnect = parConnect[0];
		m_sPortConnect = parConnect[1];
#endif
		return TRUE;
	}
	void BindPrivatePort(BOOL fSet)
	{
		m_fBindPrivatePort = fSet;
	}
	BOOL IsBindPrivatePort() { return m_fBindPrivatePort; }
	BOOL IsACL() { return m_saACL.GetSize() != 0; }
	BOOL IsFree() { return m_sConfigName == "";	}
	BOOL IsUsed() {	return !IsFree(); }
	BOOL IsLogged() { return m_fToBeLogged; }
	void Logged(BOOL fSet)
	{
		m_fToBeLogged = fSet;
	}
	BOOL IsEnabled() { return m_fEnabled; }
	void Enable(BOOL fEnabled)
	{
		m_fEnabled = fEnabled;
	}
	BOOL IsStarted() { return m_sockListen.IsSocket(); }
	
	BOOL Start(CString& sError)
	{
		Logfn("%s Demarrage de la configuration '%s'...", GetDateTimeStr(), m_sConfigName);
		CString title;
		title.Format("SockListenLoad %s", m_sConfigName);
		m_sockListen.SetSockTitle(title);

		m_fStarted = FALSE;
		m_fStarted = m_sockListen.CreateTCPListenReUseAddr(CSockAddr(m_sAddrListen), CSockPort(m_sPortListen));
		/*if (! m_sockListen.Create(SOCK_STREAM))
		{
			sError = m_sockListen.GetLastErrorString();
			m_sockListen.Close();
			return FALSE;
		}
		Logfn("%s bind de la configuration '%s'...", GetDateTimeStr(), m_sConfigName);
		if (! Bind(SockAddrListen, SockPortListen))
		{
			sError = m_sockListen.GetLastErrorString();
			Close();
			return FALSE;
		}
		Logfn("%s bind de la configuration '%s'...", GetDateTimeStr(), m_sConfigName);
		BOOL fReuse=TRUE;
		if (! SetSockOpt(SO_REUSEADDR, (const char *)&fReuse, sizeof(fReuse), SOL_SOCKET))
		{
			sError = m_sockListen.GetLastErrorString();
			Close();
			return FALSE;
		}
		if (! Listen())
		{
			sError = m_sockListen.GetLastErrorString();
			Close();
			return FALSE;
		}
		m_fStarted = TRUE;*/
		if (! m_fStarted)
		{
			sError = m_sockListen.GetLastErrorString();
		}
		return m_fStarted;
	}
	BOOL Stop(CString& sError)
	{
		Logfn("%s Arret de la configuration '%s'...", GetDateTimeStr(), m_sConfigName);
		m_fStarted = !m_sockListen.Close();
		if (m_fStarted)
		{
			sError = m_sockListen.GetLastErrorString();
		}
		return !m_fStarted;
	}
	BOOL Delete(CString& sError)
	{
		Stop(sError);

		m_fStarted     = FALSE;
		m_fToBeDeleted = TRUE;
		m_sConfigName  = "";
		m_sAddrListen  = "";
		m_sPortListen  = "";
		m_sAddrOut     = "";

		m_nACLDropped  = 0;
#ifdef _DEBUG
		m_nACLDropped  = 30;
#endif	
#ifdef _NAT
		m_nTotCon      = 0;
#endif
#ifdef _NATLOAD
		ResetAllLoadAddresses();
#endif
#ifdef _NAT
		m_sAddrConnect = "";
		m_sPortConnect = "";
#endif
		ClearACL();
		return TRUE;
	}
	BOOL IsConnected(int nAId)
	{
		return m_sAccept[nAId].IsUsed();
	}
	void CloseConnection(int nAId)
	{
		m_sAccept[nAId].m_fStarted = FALSE;
	}
	BOOL GetFirstFreeSocketIndex(int& nIndex)
	{
		for(nIndex=0; nIndex<MAX_CONNECT; nIndex++)
		{
			if (m_sAccept[nIndex].IsFree())
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	int FindACL(const char * szAddress)
	{
		for(int i=0; i<m_saACL.GetSize(); i++)
			if (m_saACL[i] == szAddress) return i;
		return -1;
	}

	BOOL ClearACL()
	{
		m_saACL.RemoveAll();
		return TRUE;
	}
	BOOL AddACL(const char * szACLAddress)
	{
		if (FindACL(szACLAddress) != -1) return FALSE;
		m_saACL.Add(szACLAddress);
		return TRUE;
	}

	BOOL DeleteACL(int nIndex)
	{
		if (nIndex >= m_saACL.GetSize()) return FALSE;
		m_saACL.RemoveAt(nIndex);
		return TRUE;
	}
	BOOL IsACLEnabled(const char * szAddress)
	{
		if (m_saACL.GetSize() == 0) return TRUE;
		for(int i=0; i<m_saACL.GetSize(); i++)
			if (m_saACL[i] == szAddress) return TRUE;
		return FALSE;
	}

#ifdef _NATLOAD
	BOOL AddLoadAddress(const char * szLoadAddress, const char * szPort, BOOL fActivated, WORD wWeight)
	{
		for(int i=0; i<MAX_LOAD_ADDR; i++)
		{
			if (m_loadaddr[i].IsFree())
			{
				m_loadaddr[i].Set(szLoadAddress, szPort);
				m_loadaddr[i].m_fActivated = fActivated;
				m_loadaddr[i].m_wWeight = wWeight;
				break;
			}
		}
		return TRUE;
	}
	void ResetAllLoadAddresses()
	{
		for(int i=0; i<MAX_LOAD_ADDR; i++)
			m_loadaddr[i].Reset();
	}
	int GetLoadAddressCount()
	{
		int nLoadAddressCount = 0;
		for(int i=0; i<MAX_LOAD_ADDR; i++)
		{
			if (!m_loadaddr[i].IsFree())
			{
				nLoadAddressCount++;
			}
		}
		return nLoadAddressCount;
	}
	SLoadAddr m_loadaddr[MAX_LOAD_ADDR];
#endif
#ifdef _NAT
	CString m_sAddrConnect;
	CString m_sPortConnect;
#endif

	CString m_sConfigName;
	CSock m_sockListen;

	DWORD   m_dwBufferSize;

	CString m_sAddrListen, m_sPortListen;
   	CString m_sAddrOut;
	BOOL    m_fBindPrivatePort;

	CStringArray m_saACL;

	SACCEPT m_sAccept[MAX_CONNECT];
	int     m_nCommDelay;

	int m_nACLDropped;
private:
#ifdef _NAT
	int m_nTotCon;
#endif
	BOOL m_fToBeDeleted, m_fToBeLogged;
	BOOL m_fStarted, m_fEnabled;
};

#endif // !defined(AFX_NATCLASSES_H__83B1FDE3_1C31_4CF0_A06E_A12616887615__INCLUDED_)
