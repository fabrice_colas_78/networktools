// NatServices.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NatServices.h"
#include "ConfigFactoryNatServices.h"

#include <afxwin.h>
#include <conio.h>

#include <Fields.h>
#include <HistoryMakerHtml.h>
#include <log.h>
#include <LibSystem.h>
#include <MacrosDef.h>
#include <ntservice.h>
#include <registry.h>
#include <Res.h>

#include "Config.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

DWORD WINAPI MainThread(LPVOID lpParameter);

const char* GetServiceName()
{
	static char szServiceName[300];

	_stprintf(szServiceName, _T("%s"), (LPCTSTR)GetFileNameComposant(GetModuleFileNameStr(), GFNC_FILENAME));
	return szServiceName;
}

const char* GetDisplayName()
{
	static char szDisplayName[300];

#ifdef _NATLOAD
	wsprintf(szDisplayName, "NAT Load Service (%s)", GetServiceName());
#endif
#ifdef _NAT
	wsprintf(szDisplayName, "NAT Service (%s)", GetServiceName());
#endif
	return szDisplayName;
}

CString GetAppName();
CString GetAppVersion();

LPCTSTR GetCommandLineArgumentsHelp()
{
	return
		_T("Available command line arguments: \n"
			"                  -install\n"
			"                  -uninstall\n"
			"                  -changelog\n"
			"                  -c (console mode)\n"
			"                  -v (display version)\n"
			"                  -m (display current config)\n"
			"                  -f<config_file>]\n"
			"                  -a<config_addr>]\n"
			"                  -p<config_port>]\n"
			"                  -f<config_file>]\n"
			"                  -G<log_dir>]\n"
			"                  -l<log_mode (0,1,2,3,4)>]\n"
			"                  -s<log_mode_cs (0,1,2)>]\n"
			"                  -D<dump_dir>]\n"
			"                  -T<dat_dir>]\n"
			"                  -g<purge_log_interval>]\n"
			"                  -x<ExecOnAccept>]\n"
		);
}

BOOL GetDocInfos(CString& sHistoryTitle, CString& sHistoryText, CString& sHelpContent)
{
	sHistoryTitle.Format(_T("Command line usage for %s"), (LPCTSTR)GetDisplayName());
	sHelpContent.Format(
		_T("%s Version %s\n"
			"%s")
		, (LPCTSTR)GetServiceName(), VER_NATSERVICES_FULL, (LPCTSTR)GetCommandLineArgumentsHelp());

	CRes resHisto(IDR_HISTO, _T("HISTO"));
	if (!resHisto.Load()) return FALSE;

	TCHAR* p = new TCHAR[(UINT)(resHisto.GetSize() + 1)];
	resHisto.Lock();
	memcpy(p, resHisto.GetPtr(), resHisto.GetSize());
	p[resHisto.GetSize()] = '\0';
	resHisto.Unlock();
	CFields fld(p, '\n');
	delete p;
	//	Suppression Header et tailer
	//fld.RemoveAt(0, 5);
	//fld.RemoveAt(fld.GetSize()-1);
	BOOL fFilterHisto = TRUE;
	if (GAKS_LSHIFT) fFilterHisto = FALSE;
	for (int i = 0; i < fld.GetSize(); i++)
	{
		if (fld[i].GetLength() > 0)
		{
#ifndef _DEBUG
			if (fFilterHisto && fld[i].GetAt(0) == '!') continue;
#endif
			sHistoryText += fld[i];
			sHistoryText += '\n';
		}
	}
	return TRUE;
}

void GenerateDocumentation()
{
	_tprintf(_T("Generating documentation in HTML format...\n"));
	CString sHistoryHtmlFile;
	sHistoryHtmlFile.Format(_T("%s\\%s.html"), (LPCTSTR)GetEnv(_T("TEMP")), (LPCTSTR)GetServiceName());

	CHistoryMakerHtml hmh;
	DeleteFile(sHistoryHtmlFile);
	CString sHistoryTitle, sHistoryText, sHelpContent;
	if (!GetDocInfos(sHistoryTitle, sHistoryText, sHelpContent) ||
		!hmh.DoFromTextToFile(GetDisplayName(), sHistoryText, sHistoryHtmlFile, sHistoryTitle, sHelpContent))
	{
		::MessageBox(NULL, _T("Unable to create html documentation file."), _T("Error"), MB_ICONSTOP);
		return;
	}
	_tprintf(_T("Documentation should be displayed in a short time in your default HTML browser.\n"));
	if (ShellExecute(NULL, _T("open"), sHistoryHtmlFile, NULL, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		CString msg;
		msg.Format(_T("Unable to display versioning file (%s)"), GetLastErrorString());
		::MessageBox(NULL, msg, _T("Error"), MB_ICONSTOP);
		return;
	}
}

CString g_sLogDirRuntime, g_sDumpDirRuntime, g_sDatDirRuntime;

CString ReplaceTokensFileName(LPCTSTR szFilePathFormatted, LPCTSTR sockaddrLocal, LPCTSTR sockportLocal, LPCTSTR sockaddrPeer, LPCTSTR sockportPeer)
{
	CString sConfigPort;
	sConfigPort.Format("%d", g_nConfigPort);
	CString sFileName = TranslateEnv(szFilePathFormatted, false);
	sFileName.Replace("{EXEDIR}", GetModuleDirectoryStr());
	sFileName.Replace("{LOGDIR}", g_sLogDirRuntime);
	sFileName.Replace("{DUMPDIR}", g_sDumpDirRuntime);
	sFileName.Replace("{DATDIR}", g_sDatDirRuntime);
	sFileName.Replace("{CONFIGADDR}", g_sConfigAddr);
	sFileName.Replace("{CONFIGPORT}", sConfigPort);
	sFileName.Replace("{LISTENIP}", sockaddrLocal);
	sFileName.Replace("{LISTENPORT}", sockportLocal);
	sFileName.Replace("{HOSTIP}", sockaddrLocal);
	sFileName.Replace("{HOSTPORT}", sockportLocal);
	sFileName.Replace("{REMOTEIP}", sockaddrPeer);
	sFileName.Replace("{REMOTEPORT}", sockportPeer);
	sFileName.Replace("{DATE}", CDateRef::GetCurrentDateTimeString(CDateRef::YYYYMMDD_HHMMSS));
	sFileName.Replace("{TIMESTAMP}", CDateRef::GetCurrentDateTimeString(CDateRef::TimeStamp));
	return sFileName;
}

CString GetDatDir()
{
	return g_sDatDirRuntime;
}

CString GetLogDir()
{
	return g_sLogDirRuntime;
}

CString GetDumpDir()
{
	return g_sDumpDirRuntime;
}

CConfigFactoryNatServices * g_pConfig = NULL;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	SetEnv(_T("EXEDIR"), GetModuleDirectoryStr());
	SetEnv(_T("INSTALLDIR"), GetModuleDirectoryStr());
	SetEnv(STR_ENV_VAR_NATSERVICES_DIR, GetModuleDirectoryStr());

#ifdef DEBUG0
	CConfigFactoryNatServices::Test();
	return 0;
#endif

	g_pConfig = new CConfigFactoryNatServices();

	CNTService ntservice(SZSERVICENAME, SZSERVICEDISPLAYNAME, SZSERVICEDISPLAYNAME, MainThread);

//#define MySaveOptions()	SaveOptions(); _tprintf(_T("Options '%c' sucessfully changed\n"), argv[i][1])

	CString sConfigFilePath;
	sConfigFilePath = CFNC(GetEnv(_T("EXEDIR")), GFNC(GetModuleFileNameStr(), GFNC_FILENAME), _T("config.json"));
	
	BOOL fStartAsService = false,  fConsoleMode = false, fDisplayConfig = false, fSaveConfig = false;
	CString sOverrideConfigPort, sOverrideConfigAddr, sOverrideLogMode, sOverrideLogModeCS, sOverrideExecOnAccept, sOverridePurgeLogInterval, sOverrideLogDir, sOverrideDumpDir, sOverrideDatDir;
	if (argc > 1)
	{
		_tprintf(_T("%s Ver %s\n"), (LPCTSTR)GetAppName(), (LPCTSTR)GetAppVersion());;
		for (int i = 1; i < argc; i++)
		{
			if ((*argv[i] == '-') || (*argv[i] == '/'))
			{
				if (!_stricmp("install", argv[i] + 1))
				{
					ntservice.InstallService();
				}
				else if (!_stricmp("uninstall", argv[i] + 1))
				{
					ntservice.RemoveService();
				}
				else if (!_tcsicmp("changelog", argv[i] + 1) || !_tcsicmp("histo", argv[i] + 1))
				{
					GenerateDocumentation();
				}
				else if (!strcmp("c", argv[i] + 1))
				{
					fConsoleMode = true;
				}
				//	Overrides
				else if (!strncmp("f", argv[i] + 1, 1))
				{
					sConfigFilePath = argv[i] + 2;
				}
				else if (!strncmp("p", argv[i] + 1, 1))
				{
					sOverrideConfigPort = argv[i] + 2;
				}
				else if (!strncmp("a", argv[i] + 1, 1))
				{
					sOverrideConfigAddr = argv[i] + 2;
				}
				else if (!strncmp("g", argv[i] + 1, 1))
				{
					sOverridePurgeLogInterval = argv[i] + 2;
				}
				else if (!strncmp("l", argv[i] + 1, 1))
				{
					sOverrideLogMode = argv[i] + 2;
				}
				else if (!strncmp("s", argv[i] + 1, 1))
				{
					sOverrideLogModeCS = argv[i] + 2;
				}
				else if (!strncmp("G", argv[i] + 1, 1))
				{
					sOverrideLogDir = argv[i] + 2;
				}
				else if (!strncmp("D", argv[i] + 1, 1))
				{
					sOverrideDumpDir = argv[i] + 2;
				}
				else if (!strncmp("T", argv[i] + 1, 1))
				{
					sOverrideDatDir = argv[i] + 2;
				}
				else if (!strncmp("x", argv[i] + 1, 1))
				{
					sOverrideExecOnAccept = argv[i] + 2;
				}
				//	~Overrides
				else if (!strcmp("v", argv[i] + 1))
				{
					DisplayVersion();
				}
				else if (!strcmp("S", argv[i] + 1))
				{
					fSaveConfig = true;
				}
				else if (!strcmp("m", argv[i] + 1))
				{
					fDisplayConfig = true;
				}
				else
				{
					puts(GetCommandLineArgumentsHelp());
					break;
				}
			}
		}
	}
	else
	{
		fStartAsService = true;
	}

	g_config.Prepare(sConfigFilePath);

#define _SAVE_OPTIONS(_method, _optvalue, _convert) \
		if (_optvalue != "") { \
			LoadOptions(); \
			_method(_convert(_optvalue)); \
			if (fSaveConfig) { SaveOptions(); } \
		}

	LoadOptions();

	_SAVE_OPTIONS(_SetConfigPort, sOverrideConfigPort, _tstoi);
	_SAVE_OPTIONS(_SetConfigAddr, sOverrideConfigAddr, );
	_SAVE_OPTIONS(_SetPurgeLogDays, sOverridePurgeLogInterval, _tstoi);
	_SAVE_OPTIONS(_SetLogMode, sOverrideLogMode, _tstoi);
	_SAVE_OPTIONS(_SetLogModeCS, sOverrideLogModeCS, _tstoi);
	_SAVE_OPTIONS(_SetLogDir, sOverrideLogDir, );
	_SAVE_OPTIONS(_SetDumpDir, sOverrideDumpDir, );
	_SAVE_OPTIONS(_SetDatDir, sOverrideDatDir, );
	_SAVE_OPTIONS(_SetExecOnAccept, sOverrideExecOnAccept, );

	g_sLogDirRuntime = ReplaceTokensFileName(g_sLogDir, "", "", "", "");
	g_sDumpDirRuntime = ReplaceTokensFileName(g_sDumpDir, "", "", "", "");
	g_sDatDirRuntime = ReplaceTokensFileName(g_sDatDir, "", "", "", "");

	if (fDisplayConfig)
	{
		printf("Exe Directory : %s\n", (LPCTSTR)GetModuleDirectoryStr());
		printf("Config file   : %s\n", (LPCTSTR)sConfigFilePath);
		printf("Config address: %s\n", (LPCTSTR)g_sConfigAddr);
		printf("Config port   : %d\n", g_nConfigPort);
		printf("Log Directory : %s [%s]\n", (LPCTSTR)g_sLogDir, (LPCTSTR)GetLogDir());
		printf("Log mode      : %d / %d\n", g_dwLogMode, g_dwLogModeCS);
		printf("Dump Directory: %s [%s]\n", (LPCTSTR)g_sDumpDir, (LPCTSTR)GetDumpDir());
		printf("Purge Log Int : %d\n", g_uPurgeLogDays);
		printf("Dat Directory : %s [%s]\n", (LPCTSTR)g_sDatDir, (LPCTSTR)GetDatDir());
		printf("ExecOnAccept  : %s\n", (LPCTSTR)g_sExecOnAccept);
	}

	CreateDirectoryAndSub(GetDatDir());
	CreateDirectoryAndSub(GetLogDir());
	CreateDirectoryAndSub(GetDumpDir());
	CreateLogFileName();

	if (fConsoleMode)
	{
		ntservice.ConsoleMode();
	}
	else if (fStartAsService)
	{
		//	Demarrage du service
		ntservice.StartServiceCtrlDispatcher();
	}

	delete g_pConfig;
	return 0;
}
