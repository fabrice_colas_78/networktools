#pragma once

#include <Arguments.h>

#define STR_ENV_VAR_NATSERVICES_DIR	_T("NatServiceDir")

#define COLOR_DEFAULT		_T("\x1B[0m")
#define COLOR_BRIGHTNESS	_T("\x1B[1m")
#define COLOR_UNDERSCORE	_T("\x1B[4m")
#define COLOR_REVERSE		_T("\x1B[7m")
#define COLOR_BLACK			_T("\x1B[30m")
#define	COLOR_RED			_T("\x1B[31m")
#define COLOR_GREEN			_T("\x1B[32m")
#define COLOR_YELLOW		_T("\x1B[33m")
#define COLOR_BLUE			_T("\x1B[34m")
#define COLOR_MAGENTA		_T("\x1B[35m")
#define COLOR_CYAN			_T("\x1B[36m")
#define COLOR_GRAY			_T("\x1B[37m")

#define COLOR_LIGHT_GREEN	COLOR_BRIGHTNESS _T("") COLOR_GREEN
#define COLOR_LIGHT_BLUE	COLOR_BRIGHTNESS _T("") COLOR_BLUE
#define COLOR_LIGHT_CYAN	COLOR_BRIGHTNESS _T("") COLOR_CYAN
#define COLOR_ERROR			COLOR_BRIGHTNESS _T("") COLOR_RED

#define ST_ID		0
#define ST_NAME		1
#define ST_LOCADDR	2
#define ST_CCON		3
#define ST_TCON		4

#include <telnetserver.h>

class CCmdParam
{
public:
	CCmdParam(CTelnetServer& sock, CArguments& args) : m_sock(sock),m_args(args)
	{
		m_szCommand = NULL;
		m_szHelp = NULL;
	}
	CTelnetServer& m_sock;
	CArguments& m_args;
	const char * m_szCommand, * m_szHelp;
};

struct tt_cfgcmd
{										    
	const char * szCommand;
	const char * szHelp;
	//BOOL (* func)(CSock& sock, CArguments& Args);
	BOOL (* func)(CCmdParam& cmdparam);
};

#define CHECK_ID(id)	\
	if (id<0 || id>(MAX_CONFIG-1))  \
	{  \
		SockPrintf(cmdparam.m_sock, COLOR_ERROR"Invalid config ID %d\r\n"COLOR_DEFAULT, id);  \
		return FALSE; \
	}

#define CHECK_ID2(id)	\
	if (id<0 || id>(MAX_CONFIG-1))  \
	{  \
		SockPrintf(sock, COLOR_ERROR"Invalid config ID %d\r\n"COLOR_DEFAULT, id);  \
		return FALSE; \
	}

#ifdef _NATLOAD
#define CHECK_SID(id)	\
	if (id<0 || id>(MAX_LOAD_ADDR-1)) \
	{ \
		SockPrintf(cmdparam.m_sock, "Invalid sub ID %d\r\n", id); \
		return FALSE; \
	} \
	else if (g_nat[nId].m_loadaddr[id].IsFree()) \
	{ \
		SockPrintf(cmdparam.m_sock, "Invalid sub ID %d\r\n", id); \
		return FALSE; \
	}
#define CHECK_WEIGHT(weight)	\
	if (weight<1 || weight>999) \
	{ \
		SockPrintf(cmdparam.m_sock, "Invalid weight value %d (must be in 1 to 999)\r\n", weight); \
		return FALSE; \
	}
#endif


class CConfig
{
public:
	static void SockPrintf(CTelnetServer& sock, const char * szFormat, ...);
	static void SockWrite(CTelnetServer& sock, const BYTE * datas, int nLenBuf);
	static void SockWrite(CTelnetServer& sock, const char * datas, int nLenBuf);
	
	static BOOL CheckBadParameters(CCmdParam& cmdparam, int nMin, int nMax);
	
	static BOOL ConfirmAction(CTelnetServer& sock, int nId, const char * szAction);
	
	static BOOL CfgQuit(CCmdParam& cmdparam);
	static BOOL CfgList(CCmdParam& cmdparam);

	static BOOL CfgLogMode(CCmdParam& cmdparam);
	static BOOL CfgSetLogMode(CCmdParam& cmdparam);
	static BOOL CfgSetLogColCount(CCmdParam& cmdparam);

	static BOOL CfgSetExecOnAccept(CCmdParam& cmdparam);

	static BOOL CfgAddConf(CCmdParam& cmdparam);
	static BOOL CfgDeleteConf(CCmdParam& cmdparam);
	static BOOL CfgRenameConf(CCmdParam& cmdparam);
	static BOOL CfgAlterConfListen(CCmdParam& cmdparam);
	static BOOL CfgAlterConfAddrOut(CCmdParam& cmdparam);
	static BOOL CfgStartConf(CCmdParam& cmdparam);
	static BOOL CfgStopConf(CCmdParam& cmdparam);
	static BOOL CfgEnableConf(CCmdParam& cmdparam);
	static BOOL CfgDisableConf(CCmdParam& cmdparam);
	static BOOL CfgLogConf(CCmdParam& cmdparam);
	static BOOL CfgUnlogConf(CCmdParam& cmdparam);
	static BOOL CfgBindPrivPortConf(CCmdParam& cmdparam);
	static BOOL CfgUnbindPrivPortConf(CCmdParam& cmdparam);

#ifdef _NAT
	static BOOL CfgAlterConfConnect(CCmdParam& cmdparam);
#endif
	
	static BOOL CfgClearACL(CCmdParam& cmdparam);
	static BOOL CfgAddACL(CCmdParam& cmdparam);
	static BOOL CfgDeleteACL(CCmdParam& cmdparam);
	static void ListOneACL(CTelnetServer& sock, int nId);
	static BOOL CfgListACL(CCmdParam& cmdparam);
	static BOOL CfgClearACLCounter(CCmdParam& cmdparam);
	static BOOL CfgConnectionsCounter(CCmdParam& cmdparam);
	static BOOL CfgDelayConf(CCmdParam& cmdparam);
	static BOOL CfgBufSizConf(CCmdParam& cmdparam);

	static BOOL CfgClearConnection(CCmdParam& cmdparam);

#if defined _NATLOAD
	static BOOL CfgAddLoad(CCmdParam& cmdparam);
	static BOOL CfgDisableLoad(CCmdParam& cmdparam);
	static BOOL CfgClearLoad(CCmdParam& cmdparam);
	static BOOL CfgEnableLoad(CCmdParam& cmdparam);
	static BOOL CfgWeightLoad(CCmdParam& cmdparam);
	static BOOL CfgDeleteLoad(CCmdParam& cmdparam);
#endif

	static BOOL CfgConnect(CCmdParam& cmdparam);
	static BOOL CfgSerialComList(CCmdParam& cmdparam);
	static BOOL CfgInterfaceList(CCmdParam& cmdparam);
	static BOOL CfgVer(CCmdParam& cmdparam);
	static BOOL CfgForceSave(CCmdParam& cmdparam);
	static BOOL CfgHelp(CCmdParam& cmdparam);
	static BOOL CfgConfig(CCmdParam& cmdparam);

	static BOOL ListPeer(CTelnetServer& sock, int nId);
	static void ListDisplay(CTelnetServer& sock, int * ListID, int nCountID);
	static void DisplayNoFormat(CTelnetServer& sock);
	static int  ListSort(CTelnetServer& sock, int * ListID, int nSortType);
	static BOOL ConfigCommand(CTelnetServer& sock, char * szCommand);
	static void CfgSendPrompt(CTelnetServer& sock);
	static BOOL IsGoodPassword(const CString& sPassword, const SYSTEMTIME& lt);
	static DWORD WINAPI ConfigThreadTCP(LPVOID lpParameter);
};
