#pragma once

#include <ConfigFactory.h>

#include <LibSystem.h>

#include "NatClasses.h"

#ifdef _NAT
#define STR_REGISTRY_ENTRY			_T("NatSvc")
#define STR_XML_ENTRY				_T("NatSvc")
#endif

#ifdef _NATLOAD
#define STR_REGISTRY_ENTRY			_T("NatLoadSvc")
#define STR_XML_ENTRY				_T("NatLoadSvc")
#endif

class CNatServicesOptions
{
public:
	CNatServicesOptions();

	CString _configAddr;
	int _configPort;
	UINT _purgeLogDays;
	BOOL _logMode;
	DWORD _logModeCS;
	DWORD _logColCount;
	DWORD _logBlockBreak;
	CString _dumpLogFileFormat;
	CString _logDir;
	CString _dumpDir;
	CString _datDir;
	CString _execOnAccept;

	//	Calculated
	CString _execOnAcceptRunTime;
};

class CConfigFactoryNatServices : public CConfigFactory
{
public:
	CConfigFactoryNatServices();

	bool Prepare(LPCTSTR szConfigFilePath, CConfigFactory::ConfigFormat configFormat = CConfigFactory::JSonFile);

	bool ReadOptions(CNatServicesOptions& options);
	bool WriteOptions(const CNatServicesOptions& options);

#if defined _NATLOAD
	bool ReadNatConfigurations(CNatLoad* nat, int size = MAX_CONFIG);
	bool WriteNatConfigurations(const CNatLoad* nat, int size = MAX_CONFIG);
#endif
#if defined _NAT
	bool ReadNatConfigurations(CNat* nat, int size = MAX_CONFIG);
	bool WriteNatConfigurations(const CNat* nat, int size = MAX_CONFIG);
#endif

	//
	static void Test();

public:
};


#define _SetConfigAddr(_v)		g_natServicesOptions._configAddr = _v
#define _SetConfigPort(_v)		g_natServicesOptions._configPort = _v
#define _SetPurgeLogDays(_v)	g_natServicesOptions._purgeLogDays = _v
#define _SetLogMode(_v)			g_natServicesOptions._logMode = _v
#define _SetLogModeCS(_v)		g_natServicesOptions._logModeCS = _v
#define _SetLogColCount(_v)		g_natServicesOptions._logColCount = _v
#define _SetLogBlockBreak(_v)	g_natServicesOptions._logBlockBreak = _v
#define _SetDumpLogFileFormat(_v)	g_natServicesOptions._dumpLogFileFormat = _v
#define _SetLogDir(_v)			g_natServicesOptions._logDir = _v
#define _SetDumpDir(_v)			g_natServicesOptions._dumpDir = _v
#define _SetDatDir(_v)			g_natServicesOptions._datDir = _v
#define _SetExecOnAccept(_v)	g_natServicesOptions._execOnAccept = _v; g_natServicesOptions._execOnAcceptRunTime = TranslateEnv(g_natServicesOptions._execOnAccept, false)


extern CConfigFactoryNatServices * g_pConfig;
extern CNatServicesOptions g_natServicesOptions;

#if defined _NATLOAD
extern CNatLoad g_nat[MAX_CONFIG];
#elif defined _NAT
extern CNat g_nat[MAX_CONFIG];
#endif

#define g_config				(*g_pConfig)

#define LoadOptions()		\
	if (g_config.Open(false, _T("LoadOptions"))) \
	{ \
		g_config.ReadOptions(g_natServicesOptions); \
		g_config.Close(); \
	}

#define SaveOptions()			\
	if (g_config.Open(true, _T("SaveOptions"))) \
	{ \
		g_config.WriteOptions(g_natServicesOptions); \
		g_config.Close(); \
	}

#define LoadNatConfigurations()		\
	if (g_config.Open(false, _T("LoadNatConfigurations"))) \
	{ \
		g_config.ReadNatConfigurations(g_nat); \
		g_config.Close(); \
	}

#define SaveNatConfigurations()		\
	if (g_config.Open(true, _T("SaveNatConfigurations"))) \
	{ \
		g_config.WriteNatConfigurations(g_nat); \
		g_config.Close(); \
	}

#define g_sConfigAddr			g_natServicesOptions._configAddr
#define g_nConfigPort			g_natServicesOptions._configPort
#define g_dwLogMode				g_natServicesOptions._logMode
#define g_dwLogModeCS			g_natServicesOptions._logModeCS
#define g_dwLogColCount			g_natServicesOptions._logColCount
#define g_dwLogBlockBreak		g_natServicesOptions._logBlockBreak
#define g_sDumpLogFileFormat	g_natServicesOptions._dumpLogFileFormat
#define g_sLogDir				g_natServicesOptions._logDir
#define g_sDumpDir				g_natServicesOptions._dumpDir
#define g_sDatDir				g_natServicesOptions._datDir
#define g_uPurgeLogDays			g_natServicesOptions._purgeLogDays
#define g_sExecOnAccept			g_natServicesOptions._execOnAccept
#define g_sExecOnAcceptRunTime	g_natServicesOptions._execOnAcceptRunTime
