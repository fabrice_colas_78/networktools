
/*
	MACRO (Reg<Get|Set><Module><param>)
	Lecture/ecriture des variables dans le registre
    - Si lecture dans registre global impossible, renvoi de la valeur par defaut

*/
#include <MacrosDef.h>

#define TRACE_REGGETSET_PARAM	LOG2F

#define HKLM	HKEY_LOCAL_MACHINE
#define HKCU	HKEY_CURRENT_USER

#define DECLARE_REGGETSET_PARAM(module_name,func_param_name,ret_type)	\
ret_type RegGetHKLM##module_name##func_param_name(); \
BOOL     RegGetHKLM##module_name##func_param_name(ret_type& _value); \
BOOL     RegSetHKLM##module_name##func_param_name(const ret_type& _value); \
BOOL     RegDelHKLM##module_name##func_param_name(); \
ret_type RegGetHKCU##module_name##func_param_name(); \
BOOL     RegGetHKCU##module_name##func_param_name(ret_type& _value); \
BOOL     RegSetHKCU##module_name##func_param_name(const ret_type& _value); \
BOOL     RegDelHKCU##module_name##func_param_name(); \
ret_type RegGetUser##module_name##func_param_name(); \
BOOL     RegGetVar##module_name##func_param_name(HKEY hKey, LPCTSTR szRegKey, LPCTSTR szVar, ret_type& sVal); \
CString  RegGetAllVar##module_name();

#define IMPLEMENT_REGGETSET_PARAM(module_name,func_param_name,ret_type,_cast,reg_param,def_val)	\
ret_type RegGetHKLM##module_name##func_param_name() \
{ \
	CRegistry reg(HKLM, _VAL_HKLM, NULL, KEY_READ); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKLM\\%s\n"), _VAL_HKLM); \
		return def_val; \
	} \
\
	ret_type var##func_param_name; \
	if (! reg.QueryValue(reg_param, (_cast *)&var##func_param_name)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKLM\\%s\\%s\n"), _VAL_HKLM, reg_param); \
		return def_val; \
	} \
\
	return var##func_param_name; \
}  \
BOOL RegGetHKLM##module_name##func_param_name(ret_type& _value) \
{ \
	CRegistry reg(HKLM, _VAL_HKLM, NULL, KEY_READ); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKLM\\%s\n"), _VAL_HKLM); \
		return false; \
	} \
\
	if (! reg.QueryValue(reg_param, (_cast* )&_value)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKLM\\%s\\%s\n"), _VAL_HKLM, reg_param); \
		return false; \
	} \
\
	return true; \
}  \
BOOL RegSetHKLM##module_name##func_param_name(const ret_type& _value) \
{ \
	CRegistry reg(HKLM, _VAL_HKLM, NULL, KEY_ALL_ACCESS); \
	if (! reg.Open(true)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegSetHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKLM\\%s\n"), _VAL_HKLM); \
		return false; \
	} \
\
	if (! reg.SetValue(reg_param, (_cast)_value)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegSetHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKLM\\%s\\%s\n"), _VAL_HKLM, reg_param); \
		return false; \
	} \
\
	return true; \
}  \
BOOL RegDelHKLM##module_name##func_param_name() \
{ \
	CRegistry reg(HKLM, _VAL_HKLM, NULL, KEY_ALL_ACCESS); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegDelHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKLM\\%s\n"), _VAL_HKLM); \
		return false; \
	} \
\
	if (! reg.DeleteValue(reg_param)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegDelHKLM") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKLM\\%s\\%s\n"), _VAL_HKLM, reg_param); \
		return false; \
	} \
\
	return true; \
}  \
ret_type RegGetHKCU##module_name##func_param_name() \
{ \
	CRegistry reg(HKCU, _VAL_HKCU, NULL, KEY_READ); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKCU\\%s\n"), _VAL_HKCU); \
		return def_val; \
	} \
\
	ret_type var##func_param_name; \
	if (! reg.QueryValue(reg_param, (_cast *)&var##func_param_name)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKCU\\%s\\%s\n"), _VAL_HKCU, reg_param); \
		return def_val; \
	} \
\
	return var##func_param_name; \
}  \
BOOL RegGetHKCU##module_name##func_param_name(ret_type& _value) \
{ \
	CRegistry reg(HKCU, _VAL_HKCU, NULL, KEY_READ); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKCU\\%s\n"), _VAL_HKCU); \
		return false; \
	} \
\
	if (! reg.QueryValue(reg_param, (_cast* )&_value)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKCU\\%s\\%s\n"), _VAL_HKCU, reg_param); \
		return false; \
	} \
\
	return true; \
}  \
BOOL RegSetHKCU##module_name##func_param_name(const ret_type& _value) \
{ \
	CRegistry reg(HKCU, _VAL_HKCU, NULL, KEY_ALL_ACCESS); \
	if (! reg.Open(true)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegSetHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKCU\\%s\n"), _VAL_HKCU); \
		return false; \
	} \
\
	if (! reg.SetValue(reg_param, (_cast)_value)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegSetHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKCU\\%s\\%s\n"), _VAL_HKCU, reg_param); \
		return false; \
	} \
\
	return true; \
}  \
BOOL RegDelHKCU##module_name##func_param_name() \
{ \
	CRegistry reg(HKCU, _VAL_HKCU, NULL, KEY_ALL_ACCESS); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegDelHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKCU\\%s\n"), _VAL_HKCU); \
		return false; \
	} \
\
	if (! reg.DeleteValue(reg_param)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegDelHKCU") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKCU\\%s\\%s\n"), _VAL_HKCU, reg_param); \
		return false; \
	} \
\
	return true; \
}  \
ret_type RegGetUser##module_name##func_param_name() \
{ \
	CRegistry reg(HKCU, _VAL_HKCU, NULL, KEY_READ); \
	if (! reg.Open(false)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetUser") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to open HKCU\\%s\n"), _VAL_HKCU); \
		return RegGetHKLM##module_name##func_param_name(); \
	} \
\
	ret_type var##func_param_name; \
	if (! reg.QueryValue(reg_param, (_cast* )&var##func_param_name)) \
	{ \
		TRACE_REGGETSET_PARAM(_CNV("RegGetUser") _CNV2(module_name) _CNV2(func_param_name) _CNV(": unable to query value HKCU\\%s\\%s\n"), _VAL_HKCU, reg_param); \
		return RegGetHKLM##module_name##func_param_name(); \
	} \
\
	return var##func_param_name; \
}  \
BOOL RegGetVar##module_name##func_param_name(HKEY hKey, LPCTSTR szRegKey, LPCTSTR szVar, ret_type& sVal) \
{ \
	CRegistry reg(hKey, szRegKey, NULL, KEY_READ); \
	if (! reg.Open(false)) return false; \
	if (! reg.QueryValue(szVar, (_cast *)&sVal)) return false; \
	return true; \
}

