// HTTPServerSvc.cpp : d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"
#include "HTTPServerSvc.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <mswsock.h>

#include <DateRef.h>
#include <EventLog.h>
#include <fields.h>
#include <HistoryMakerHtml.h>
#include <LibSystem.h>
#include <log.h>
#include <MacrosDef.h>
#include <MD5Hash.h>
#include <NTService.h>
#include <Registry.h>
#include <res.h>
#include <sock.h>
#include <Winerr.h>

#include "CriticalSection.h"
#include "MacrosCommon.h"
#include "SockHTTPServer.h"
#include "HTTPServerSvcConfig.h"
#include "SockHTTPServer.h"
#include "HTTPServerCommon.h"

#include <Iphlpapi.h>
#include <Assert.h>
#pragma comment(lib, "iphlpapi.lib")

LPCTSTR GetServiceName();
LPCTSTR GetDisplayName();
LPCTSTR GetDescription();
BOOL GetOSDisplayString(CString& sOSInfos);
BOOL GetDocInfos(CString& sHistoryTitle, CString& sHistoryText, CString& sHelpContent);

//----------------------------------
//	Versionning
//----------------------------------
#define _COPYRIGHT					_T("Copyright (C) http://fcsoft.fcolas.com 2010-2021")

#ifdef _WIN64
#define _PROCESSOR_ARCH				_T("64 bits Edition")
#else
#define _PROCESSOR_ARCH				_T("32 bits Edition")
#endif

#define COPYRIGHT					_COPYRIGHT _T(" (") _PROCESSOR_ARCH _T(")")

#define PRODUCT_NAME				_T("HTTPServer")
#define MAIN_TITLE					_T("HTTP Server")

#define VER_HTTPSERVER_MAJOR		"1.2"
#define VER_HTTPSERVER_REVISION		".0033"

#define VER_HTTPSERVER_FULL			_T(VER_HTTPSERVER_MAJOR) _T(VER_HTTPSERVER_REVISION)

#define STR_ENV_VAR_HTTPSERVERSVC_LOG_DIR			_T("HTTPSERVERSVCLOGDIR")

//	CR/LF for static configuration
#define  S_LF	_T("\r\n")

HANDLE g_hEventServerWantToStop = NULL;

//CString g_logFile;
CDateRef g_drServerLastUpdate;

//CLogger * g_loggerMAIN = NULL;
#define g_loggerMAIN	M_LOGGER
#define MAIN_LOGDebugf	if (g_loggerMAIN && g_loggerMAIN->GetThresholdPriority()>=LP_Debug) g_loggerMAIN->Debugf
#define MAIN_LOGInfof	if (g_loggerMAIN && g_loggerMAIN->GetThresholdPriority()>=LP_Info) g_loggerMAIN->Infof
#define MAIN_LOGWarnf	if (g_loggerMAIN && g_loggerMAIN->GetThresholdPriority()>=LP_Warn) g_loggerMAIN->Warnf
#define MAIN_LOGErrorf	if (g_loggerMAIN && g_loggerMAIN->GetThresholdPriority()>=LP_Error) g_loggerMAIN->Errorf

//#undef M_LOGGER
//#define M_LOGGER	g_logger

//CLogger * g_loggerZTHD = NULL;
#define g_loggerZTHD	M_LOGGER
#define ZTHD_LOGDebugf	if (g_loggerZTHD && g_loggerZTHD->GetThresholdPriority()>=LP_Debug) g_loggerZTHD->Debugf
#define ZTHD_LOGInfof	if (g_loggerZTHD && g_loggerZTHD->GetThresholdPriority()>=LP_Info) g_loggerZTHD->Infof
#define ZTHD_LOGWarnf	if (g_loggerZTHD && g_loggerZTHD->GetThresholdPriority()>=LP_Warn) g_loggerZTHD->Warnf
#define ZTHD_LOGErrorf	if (g_loggerZTHD && g_loggerZTHD->GetThresholdPriority()>=LP_Error) g_loggerZTHD->Errorf

class CSockHTTPServerHTTPServer : public CSockHTTPServer
{
public:
	virtual BOOL HandleGetPostHead();

	CSockHTTPServerHTTPServer(LPCTSTR szServerName = _T("")) : CSockHTTPServer(szServerName)
	{
	}
	void SetDirectoryIndex(const CString& sDirectoryIndex)
	{
		CFields indexes(sDirectoryIndex, ';');
		CString sIndex;
		for (int i = 0; i < indexes.GetSize(); i++)
		{
			if (indexes[i] == _T("")) continue;
			if (indexes[i].GetAt(0) == '/')
			{
				sIndex = indexes[i];
			}
			else
			{
				sIndex.Format(_T("/%s"), indexes[i]);
			}
			m_saDirectoryIndexes.Add(sIndex);
		}
	}

	BOOL IsDirectoryIndex(LPCTSTR szURL)
	{
		for (int i = 0; i < m_saDirectoryIndexes.GetCount(); i++)
		{
			if (_tcsncmp(m_saDirectoryIndexes[i], szURL, m_saDirectoryIndexes[i].GetLength()) == 0) return true;
		}
		return false;
	}

	CString m_sVirtualDirectory;
	static CString m_sDocumentRootDirectory;
private:
	BOOL CreateResultFile(LPCTSTR szLocalFileName, LPCTSTR szTmpFile, CString& sMimeType, const CString& sURL, const CHTTPRequestParams& params, BOOL fConfig, BOOL fParams, BOOL fHeaders, BOOL fChangeLog, BOOL fLogHTTPInfos);
	void GetBodyContent(CString& sBodyContent, const CString& sURL, const CHTTPRequestParams& params, BOOL fConfig, BOOL fParams, BOOL fHeaders, BOOL fChangeLog, BOOL fLogHTTPInfos);
	void GetHtmlContent(CString& sHtmlContent, const CString& sURL, const CHTTPRequestParams& params, BOOL fConfig, BOOL fParams, BOOL fHeaders, BOOL fChangeLog, BOOL fLogHTTPInfos);

	static void AddAStyle(CStringArray& styles, const CHtmlAppender::CHtmlStyle& style);
	CString GetStyles();
	CStringArray m_saDirectoryIndexes;
};

#define BORDER_COLOR	"E7E7CE"

void CSockHTTPServerHTTPServer::AddAStyle(CStringArray& styles, const CHtmlAppender::CHtmlStyle& style)
{
	CString sStyle;
	sStyle += (LPCTSTR)style.m_sName;
	sStyle += " {";
	for (int i = 0; i < style.m_properties.GetSize(); i++)
	{
		sStyle += style.m_properties[i];
		sStyle += "; ";
	}
	sStyle += " }";
	styles.Add(sStyle);
}

#define AddStyle(_style)	AddAStyle(styles,_style)

CString CSockHTTPServerHTTPServer::GetStyles()
{
	CHtmlAppender::CHtmlStyle style;
	CStringArray styles;

	style.Init(_T("body,table,td,div"));
	style.AddProperty(_T("font-family"), _T("Verdana, Arial, sans-serif"));
	style.AddProperty(_T("font-size"), _T("9pt"));
	style.AddProperty(_T("background-color"), _T("#FEFEFE" /*"#ece9d8"*/));
	style.AddProperty(_T("margin"), _T("5px"));
	AddStyle(style);

	style.Init(_T("a:link"));
	style.AddProperty(_T("color"), _T("#000"));
	style.AddProperty(_T("text-decoration"), _T("underline"));
	AddStyle(style);
	style.Init(_T("a:visited"));
	style.AddProperty(_T("color"), _T("#000"));
	style.AddProperty(_T("text-decoration"), _T("underline"));
	AddStyle(style);
	style.Init(_T("a:hover"));
	style.AddProperty(_T("color"), _T("#4040C0"));
	style.AddProperty(_T("text-decoration"), _T("underline"));
	AddStyle(style);

	style.Init(_T("table"));
	//style.AddProperty(_T("background-color"), _T("#F3F7FD"));
	//style.AddProperty(_T("empty-cells"), _T("show"));
	style.AddProperty(_T("border-collapse"), _T("collapse"));
	style.AddProperty(_T("border-color"), _T("#" BORDER_COLOR));
	style.AddProperty(_T("border"), _T("solid 1 px #" BORDER_COLOR));
	AddStyle(style);

	style.Init(_T("th"));
	style.AddProperty(_T("font-size"), _T("10pt"));
	style.AddProperty(_T("color"), _T("#000"));
	style.AddProperty(_T("background-color"), _T("#CECF9C"));
	style.AddProperty(_T("border"), _T("solid 1px #" BORDER_COLOR));
	style.AddProperty(_T("padding"), _T("2 px"));
	style.AddProperty(_T("margin-left"), _T("4 px"));
	style.AddProperty(_T("margin-top"), _T("2 px"));
	style.AddProperty(_T("margin-bottom"), _T("2 px"));
	AddStyle(style);

	style.Init(_T("td"));
	//style.AddProperty(_T("background-color"), _T("#F3F7FD"));
	style.AddProperty(_T("border"), _T("solid 1px #" BORDER_COLOR));
	style.AddProperty(_T("padding"), _T("2 px"));
	style.AddProperty(_T("margin-left"), _T("4 px"));
	style.AddProperty(_T("margin-top"), _T("2 px"));
	style.AddProperty(_T("margin-bottom"), _T("2 px"));
	AddStyle(style);

	CString sStyle;
	for (int i = 0; i < styles.GetSize(); i++)
		sStyle += styles[i];
	return sStyle;
}

CString MakeHRef(const CString& sDirectoryIndexes)
{
	CString hrefs;

	CFields dirindexes(sDirectoryIndexes, ';');
	for (int i = 0; i < dirindexes.GetSize(); i++)
	{
		hrefs += "<A target=\"_blank\" HREF=\"";
		hrefs += dirindexes[i];
		hrefs += "\">";
		hrefs += dirindexes[i];
		hrefs += "</A>";
		if (i < (dirindexes.GetSize() - 1))
		{
			hrefs += ";";
		}
	}

	return hrefs;
}

void CSockHTTPServerHTTPServer::GetBodyContent(CString& sBody, const CString& sURL, const CHTTPRequestParams& params, BOOL fConfig, BOOL fParams, BOOL fHeaders, BOOL fChangeLog, BOOL fLogHTTPInfos)
{
#define TABLE_HEADER_2COL(_s1,_s2)	sBody+="<tr><th>"; sBody+=_s1; sBody+="</th><th>"; sBody+=_s2; sBody+="</th></tr>"
#define TABLE_ROW_2COL(_s1,_s2)		sBody+="<tr><td>"; sBody+=_s1; sBody+="</td><td>"; sBody+=_s2; sBody+="</td></tr>"

	sBody = _T("");
	if (fConfig == false && fChangeLog == false)
	{
		CString s;
		sBody += "<h3>HTTP Request:</h3>";
		sBody += "<table>";
		TABLE_HEADER_2COL("Item", "Value");
		TABLE_ROW_2COL("Verb", m_Request.m_sVerb);
		s.Format(_T("%u.%u"), HIWORD(m_Request.m_dwHttpVersion), LOWORD(m_Request.m_dwHttpVersion));
		TABLE_ROW_2COL("HTTP version", s);
		TABLE_ROW_2COL("URL", sURL);
		TABLE_ROW_2COL("Host", m_Request.m_sHost);
		TABLE_ROW_2COL("User agent", m_Request.m_sUserAgent);
		//TABLE_ROW_2COL("Virtual directory", m_sVirtualDirectory);
		TABLE_ROW_2COL("Keep alive", m_bResponseKeepAlive ? "Yes" : "No");
		sBody += "</table>";
	}
	if (fParams)
	{
		sBody += "<h3>Query string parameters</h3>";
		sBody += "<table>";
		TABLE_HEADER_2COL("Parameter", "Value");
		CString key, value;
		for (int qsparam = 0; qsparam < params.GetCount(); qsparam++)
		{
			TABLE_ROW_2COL(params[qsparam].m_sParam, params[qsparam].m_sValue == _T("") ? _T("[None]") : params[qsparam].m_sValue);
			if (fLogHTTPInfos)
				LOGInfof(_T("URLParam[%s]: %s"), params[qsparam].m_sParam, params[qsparam].m_sValue);
		}
		sBody += "</table>";
	}

	if (fHeaders)
	{
		sBody += "<h3>HTTP headers</h3>";
		sBody += "<table>";
		TABLE_HEADER_2COL("Parameter", "Value");
		CString key, value;
		POSITION nextpos = m_Request.m_HeaderMap.GetStartPosition();
		while (nextpos != NULL)
		{
			m_Request.m_HeaderMap.GetNextAssoc(nextpos, key, value);
			TABLE_ROW_2COL(key, value);
			if (fLogHTTPInfos)
				LOGInfof(_T("HTTPHeader[%s]: %s"), key, value);
		}
		sBody += "</table>";
	}

	if (fConfig)
	{
		sBody += "<h3>"; sBody += GetDisplayName(); sBody += " configuration</h3>";
		sBody += "<table>";
		TABLE_HEADER_2COL("Parameter", "Value");
		TABLE_ROW_2COL("Version", VER_HTTPSERVER_FULL);
		TABLE_ROW_2COL("Build", GetModuleDateTimeStr(NULL, false));
		TABLE_ROW_2COL("Installation path", GetModuleFileNameStr());
		TABLE_ROW_2COL("&nbsp;", "&nbsp;");
		CString sOSInfos;
		GetOSDisplayString(sOSInfos);
		TABLE_ROW_2COL("OS infos", sOSInfos);
		TABLE_ROW_2COL("Computer name", (LPCTSTR)GetComputerNameStr());
		TABLE_ROW_2COL("Avail. IP address(es)", CSockAddr::GetLocalHostIPAddress());
		TABLE_ROW_2COL("Service name", GetServiceName());
		TABLE_ROW_2COL("Service display name", GetDisplayName());
		TABLE_ROW_2COL("Service description", GetDescription());
		TABLE_ROW_2COL("&nbsp;", "&nbsp;");
		TABLE_ROW_2COL("Listening address", RegGetUserHTTPServerSvcHTTPDListenAddr());
		TABLE_ROW_2COL("Listening port", RegGetUserHTTPServerSvcHTTPDListenPort());
		TABLE_ROW_2COL("Log directory", RegGetUserHTTPServerSvcLogDir());
		TABLE_ROW_2COL("Document root directory", RegGetUserHTTPServerSvcWWWDir());
		TABLE_ROW_2COL("Directory index(es)", MakeHRef(RegGetUserHTTPServerSvcDirectoryIndex()));
		TABLE_ROW_2COL("Allowed networks", RegGetUserHTTPServerSvcAllowedNetworks());
		sBody += "</table>";
	}

	if (fChangeLog)
	{
		CHistoryMakerHtml hmh;
		hmh.SetHtmlAppenderClass(CHtmlMaker::String);

		CString sHistoryTitle, sHistoryText, sHelpContent, sHistoryHtml;
		if (!GetDocInfos(sHistoryTitle, sHistoryText, sHelpContent) ||
			!hmh.DoFromTextToString(GetDisplayName(), sHistoryText, sHistoryHtml, sHistoryTitle, sHelpContent))
		{
			sBody += "Unable to create html documentation file.";
		}
		else
		{
			sBody += sHistoryHtml;
		}
	}

	sBody += "<br>";
	if (!fConfig)
		sBody += "<li><a href=\"?config\">Config</a></li>";
	if (!fChangeLog)
		sBody += "<li><a href=\"?changelog\">Change log</a></li>";
	if (fConfig || fChangeLog)
		sBody += "<li><a href=\"?\">Back to home</a></li>";
	sBody += "<br>";
}

void CSockHTTPServerHTTPServer::GetHtmlContent(CString& sHTMLContent, const CString& sURL, const CHTTPRequestParams& params, BOOL fConfig, BOOL fParams, BOOL fHeaders, BOOL fChangeLog, BOOL fLogHTTPInfos)
{
	CString sBody;

	GetBodyContent(sBody, sURL, params, fConfig, fParams, fHeaders, fChangeLog, fLogHTTPInfos);
	sHTMLContent = _T("");
	sHTMLContent += "<html>";
	sHTMLContent += "<head>";
	sHTMLContent += "<style>";
	sHTMLContent += GetStyles();
	sHTMLContent += "</style>";
	sHTMLContent += "</head>";
	sHTMLContent += "<body>";
	sHTMLContent += sBody;
	sHTMLContent += "</body>";
	sHTMLContent += "</html>";
}

BOOL CSockHTTPServerHTTPServer::CreateResultFile(LPCTSTR szLocalFileName, LPCTSTR szTmpFile, CString& sMimeType, const CString& sURL, const CHTTPRequestParams& params, BOOL fConfig, BOOL fParams, BOOL fHeaders, BOOL fChangeLog, BOOL fLogHTTPInfos)
{
	CFileEx fileSrc, fileDst;
	if (!fileSrc.Open(szLocalFileName, CFile::modeRead | CFile::shareDenyNone))
	{
		LOGErrorf(_T("Unable to open '%s' (%s)"), szLocalFileName, ::GetLastErrorString());
		return false;
	}
	if (!fileDst.Open(szTmpFile, CFile::modeCreate | CFile::modeWrite))
	{
		LOGErrorf(_T("Unable to create '%s' (%s)"), szTmpFile, ::GetLastErrorString());
		return false;
	}
	DWORD dwFileSizeH, dwFileSizeL;
	fileSrc.GetFileSize(dwFileSizeH, dwFileSizeL);
	char * buf = new char[dwFileSizeL];
	fileSrc.Read(buf, dwFileSizeL);
	CStringA content(buf, dwFileSizeL);
	//	Replace tokens
	CString sHTMLHead;
	sHTMLHead += "<html>";
	sHTMLHead += "<head>";
	sHTMLHead += "<style>";
	sHTMLHead += GetStyles();
	sHTMLHead += "</style>";
	sHTMLHead += "</head>";
	sHTMLHead += "<body>";

	CString sBody;
	GetBodyContent(sBody, sURL, params, fConfig, fParams, fHeaders, fChangeLog, fLogHTTPInfos);
	int nMimeTypeReplaced = content.Replace("{MIME_TYPE_HTML}", "");
	if (nMimeTypeReplaced > 0)
	{
		sMimeType = CMimeType::GetMimeTypeFromExtension(_T("htm"));
	}
	int nHTTPHeadReplaced = content.Replace("{HTTP_HEAD}", sHTMLHead);
	content.Replace("{HTTP_INFOS_HTML}", sBody);
	if (nHTTPHeadReplaced > 0)
	{
		content += "</body>";
		content += "</html>";

	}
	fileDst.Write((const BYTE *)(const char *)content, content.GetLength());
	delete buf;
	return true;
	}

BOOL CSockHTTPServerHTTPServer::HandleGetPostHead()
{
	CString sURL = m_Request.m_sURL;
	IFLOGInfof(_T("Requesting %s %s HTTP/%d.%d - Host:%s - UserAgent:%s..."), m_Request.m_sVerb, sURL, HIWORD(m_Request.m_dwHttpVersion), LOWORD(m_Request.m_dwHttpVersion), m_Request.m_sHost, m_Request.m_sUserAgent);

#if 0
	//X-FORWARDED-FOR
	CString sXForwardedFor;
	if (m_Request.m_HeaderMap.Lookup("X-FORWARDED-FOR"), sXForwardedFor))
	{
	IFLOGInfof(_T("X-FORWARDED-FOR:%s"), sXForwardedFor);
	}
#endif

	CHTTPRequestParams params;
	m_Request.GetQueryStringParams(params);

	BOOL fParams = true;
	BOOL fHeaders = true;
	BOOL fConfig = false;
	BOOL fChangeLog = false;
	BOOL fLogHTTPInfos = false;

	CString sActions;
	for (int i = 0; i < params.GetCount(); i++)
	{
		if (sActions != _T("")) sActions += _T(",");
		sActions += params[i].m_sParam;
		if (params[i].m_sParam.CompareNoCase(_T("config")) == 0)
		{
			fConfig = true;
			fParams = false;
			fHeaders = false;
		}
		else if (params[i].m_sParam.CompareNoCase(_T("changelog")) == 0)
		{
			fChangeLog = true;
			fParams = false;
			fHeaders = false;
		}
		else
		{
			IFLOGWarnf(_T("Unknown action '%s'"), params[i].m_sParam);
		}
	}

	if (sURL == "/" || IsDirectoryIndex(sURL))
	{
		//	Send response
		CString sHTMLContent;
		GetHtmlContent(sHTMLContent, sURL, params, fConfig, fParams, fHeaders, fChangeLog, fLogHTTPInfos);
		CHTTPResponseHeader hdr;
		int httpCode = 200;
		hdr.MakeCommonHeader(httpCode, sHTMLContent.GetLength(), m_bResponseKeepAlive, GetServerName());
		SendHeaderAndDatas(hdr.GetData(), sHTMLContent);
		IFLOGInfof(_T("resource '%s' [InternalResource-Action:%s] %u bytes sent (HttpCode:%d)"), m_Request.m_sURL, sActions, sHTMLContent.GetLength(), httpCode);
}
	else
	{
		fLogHTTPInfos = true;
		CString sLocalFileName;
		sLocalFileName.Format(_T("%s%s"), (LPCTSTR)m_sDocumentRootDirectory, (LPCTSTR)sURL);
		sLocalFileName.Replace(_T("/"), _T("\\"));
		if (IsFileExist(sLocalFileName))
		{
			TCHAR szTmpFile[MAX_PATH];
			GetTempFileName(GetEnv(_T("TEMP")), _T("hss-"), 0, szTmpFile);
			CString sExt = GFNC(sLocalFileName, GFNC_EXT);
			if (sExt != _T(""))
			{
				_tcscat(szTmpFile, _T("."));
				_tcscat(szTmpFile, sExt);
			}
			CString mimeType = (LPCTSTR)CMimeType::GetMimeTypeFromExtension(GFNC(szTmpFile, GFNC_EXT));
			CreateResultFile(sLocalFileName, szTmpFile, mimeType, sURL, params, fConfig, fParams, fHeaders, fChangeLog, fLogHTTPInfos);
			CFile file;
			if (!file.Open(szTmpFile, CFile::modeRead | CFile::shareDenyNone))
			{
				CString sError;
				sError.Format(_T("unable to open resource file '%s' (%s)"), szTmpFile, ::GetLastErrorString());
				IFLOGErrorf(_T("%s"), sError);
				CString sBody;
				sBody = sError;
				CHTTPResponseHeader hdr;
				hdr.MakeCommonHeader(500, sBody.GetLength(), m_bResponseKeepAlive, GetServerName());
				SendHeaderAndDatas(hdr.GetData(), sBody);
			}
			else
			{
				ULONGLONG dwFileLength = file.GetLength();
				IFLOGInfof(_T("resource '%s' [%s] exists (MimeType:'%s', FileLength:%u)."), m_Request.m_sURL, szTmpFile, mimeType, dwFileLength);
				CHTTPResponseHeader hdr;
				hdr.AddStatusCode(200);
				SYSTEMTIME st;
				GetSystemTime(&st);
				hdr.AddDate(st);
				hdr.AddServer(GetServerName());
				//responseHdr.AddW3MfcAllowFields(pSettings->m_bAllowDeleteRequest);
				if (m_bResponseKeepAlive)
					hdr.AddKeepAlive();
				hdr.AddContentLength((DWORD)dwFileLength);
				hdr.AddContentType(mimeType);
				if (M_LOGGER && M_LOGGER->GetThresholdPriority() >= LP_Debug)
				{
					LOGDump(LP_Debug, _T("ResponseHeader"), (LPBYTE)(LPCSTR)hdr.GetData(), (int)strlen(hdr.GetData()));
				}
				//	Prepare TFB
				TRANSMIT_FILE_BUFFERS tfb;
				CStringA sData(hdr.GetData());
				tfb.HeadLength = sData.GetLength();
				tfb.Head = sData.GetBuffer();
				tfb.Tail = NULL;
				tfb.TailLength = 0;
				//Call the TransmitFile function
				if (!TransmitFile(m_socketID, file.m_hFile, (DWORD)file.GetLength(), 0, NULL, &tfb, TF_USE_KERNEL_APC))
				{
					IFLOGErrorf(_T("Error while transmitting file '%s' (%s)"), szTmpFile, ::GetLastErrorString());
				}
				else
				{
					IFLOGInfof(_T("resource '%s' [%s] %u bytes sent (HttpCode:%d)"), m_Request.m_sURL, szTmpFile, file.GetLength(), 200);
				}
				file.Close();
				DeleteFile(szTmpFile);
			}
		}
		else
		{
			//[18/Jan/2014:17:16:59 +0100] 127.0.0.1       - - "GET /e HTTP/1.0" 404 199
			IFLOGErrorf(_T("resource '%s' [%s] not found (HttpCode:%d)"), m_Request.m_sURL, sLocalFileName, 404);
			return false;
		}
	}
	return true;
}

CString CSockHTTPServerHTTPServer::m_sDocumentRootDirectory = _T("www");

BOOL CHTTPServerSvc::CreateListeningTCPSocket(LPCTSTR name, CSock& sock, DWORD dwThreadID, LPCTSTR szListenAddr, LPCTSTR szListenPort)
{
	CString sTitle;
	sTitle.Format(_T("%s-TCP"), name);
	MAIN_LOGInfof(_T("[%u] Creating listening socket on tcp:%s:%s..."), dwThreadID, szListenAddr, szListenPort);
	sock.SetSockTitle(sTitle);
#ifdef _DEBUG
	_tprintf(_T("Creating listening socket tcp:%s:%s...\n"), szListenAddr, szListenPort);
#endif
	if (!sock.CreateTCPListen(CSockAddr(szListenAddr), CSockPort(szListenPort)))
	{
		MAIN_LOGErrorf(_T("[%u] Unable to create listening socket on tcp:%s:%s (%s)"), dwThreadID, szListenAddr, szListenPort, sock.GetLastErrorString());
		_tprintf(_T("Unable to create TCP listenning socket %s:%s (%s)\n"), szListenAddr, szListenPort, (LPCTSTR)sock.GetLastErrorString());
		return false;
	}
	MAIN_LOGInfof(_T("[%u] TCP socket created and now listening on %s:%s."), dwThreadID, szListenAddr, szListenPort);
	_tprintf(_T("TCP socket created and now listening on %s:%s.\n"), szListenAddr, szListenPort);
	return true;
}
#if 0
BOOL CHTTPServerSvc::CreateListeningUDPSocket(LPCTSTR name, CSock& sock, DWORD dwThreadID, LPCTSTR szListenAddr, LPCTSTR szListenPort)
{
	CString sTitle;
	sTitle.Format(_T("%s-UDP"), name);
	MAIN_LOGInfof(_T("[%u] Creating listening socket on udp:%s:%s..."), dwThreadID, szListenAddr, szListenPort);
	sock.SetSockTitle(sTitle);
#ifdef _DEBUG
	_tprintf(_T("Creating listening socket udp:%s:%s...\n"), szListenAddr, szListenPort);
#endif
	if (!sock.CreateUDP(CSockAddr(szListenAddr), CSockPort(szListenPort)))
	{
		MAIN_LOGErrorf(_T("[%u] Unable to create listening socket on udp:%s:%s (%s)"), dwThreadID, szListenAddr, szListenPort, sock.GetLastErrorString());
#ifdef _DEBUG
		_tprintf(_T("Unable to create listening socket udp:%s:%s (%s)\n"), szListenAddr, szListenPort, sock.GetLastErrorString());
#endif
		return false;
	}
	MAIN_LOGInfof(_T("[%u] TCP socket created on %s:%s."), dwThreadID, szListenAddr, szListenPort);
#ifdef _DEBUG
	_tprintf(_T("Now listening on udp:%s:%s.\n"), szListenAddr, szListenPort);
#endif
	return true;
}
#endif

struct tt_thread_param
{
	UINT uID;
	CSock * pSock;
};

UINT CHTTPServerSvc::GetNewThreadID()
{
	static UINT s_id = 0;
	return s_id++;
}

DWORD WINAPI CHTTPServerHTTPD::HTTPServerHTTPDThread(void * p)
{
	tt_thread_param * pParam = (tt_thread_param *)p;
	UINT uID = pParam->uID;

	SOCKET socket;
	if (!pParam->pSock->Detach(socket))
	{
		LOGErrorf(_T("[%u] unable detach socket (%s)"), uID, pParam->pSock->GetLastErrorString());
		delete pParam->pSock;
		delete pParam;
		return 0;
	}
	//	free allocation pour socket depuis MainThread
	delete pParam->pSock;

	CString sServerName;
	sServerName.Format(_T("HTTPServerSvc/%s%s at %s"), VER_HTTPSERVER_MAJOR, VER_HTTPSERVER_REVISION, GetComputerNameStr());
	CSockHTTPServerHTTPServer sock(sServerName);
	sock.SetExchangeID(uID);
	sock.Attach(socket, false);
	sock.SetDirectoryIndex(RegGetUserHTTPServerSvcDirectoryIndex());

	//	Affichage infos de connexion locale et distante
	CSockAddr addrLocal, addrPeer;
	CSockPort portLocal, portPeer;
	sock.GetSockName(addrLocal, portLocal);
	sock.GetPeerName(addrPeer, portPeer);
	LOGDebugf(_T("[%u] Starting a new HTTPD thread %s:%u from %s:%u"), uID, addrLocal.String(), portLocal.HostNumber(), addrPeer.String(), portPeer.HostNumber());

	//	Traitement principal
	if (!sock.Run(g_hEventServerWantToStop))
	{
		LOGErrorf(_T("[%u] HTTP server error (%s)"), uID, sock.GetLastErrorString());
	}

	//	Fermeture socket
	sock.Close();

	LOGDebugf(_T("[%u] Ending HTTPD thread - disconnection: local:%s:%u remote:%s:%u"), uID, addrLocal.String(), portLocal.HostNumber(), addrPeer.String(), portPeer.HostNumber());
	//	free allocation pour structure parametres depuis MainThread
	//delete pParam->pSock;
	delete pParam;
	return 0;
}

IMPLEMENT_DEBUG_APPENDER

INITLogger(CHTTPServerSvc::M_LOGGER);

INITLogger(CHTTPServerHTTPD::M_LOGGER);

CString GetLogConfig()
{
	CString config;
	config += _T("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>") S_LF;
	config += _T("<logger:configuration replace=\"false\">") S_LF;
	config += S_LF;
	//	Appenders
	config += _T("    <appender name=\""); config += L4C_APPENDER(HTTPSERVER_SVCMAIND_APPENDER); config += _T("\" class=\"RollingFileAppender\">") S_LF;
	config += _T("        <param name=AutoStart value=\"false\" />") S_LF;
	config += _T("        <param name=File value=\"*[LogFileNameNotSet]*\"/>") S_LF;
	config += _T("        <param name=MaxFileSize value=\"1000000\" />") S_LF;
	config += _T("        <param name=MaxBackupIndex value=\"7\" />") S_LF;
	config += _T("        <layout class=\"PatternLayout\">") S_LF;
	config += _T("           <param name=ConversionPattern value=\"%d [%-5p] [%7t] %m%n\"/>") S_LF;
	config += _T("        </layout>") S_LF;
	config += _T("    </appender>") S_LF;
	config += S_LF;
	config += _T("    <appender name=\""); config += L4C_APPENDER(HTTPSERVER_HTTPD_APPENDER); config += _T("\" class=\"RollingFileAppender\">") S_LF;
	config += _T("        <param name=AutoStart value=\"false\" />") S_LF;
	config += _T("        <param name=File value=\"*[LogFileNameNotSet]*\"/>") S_LF;
	config += _T("        <param name=MaxFileSize value=\"1000000\" />") S_LF;
	config += _T("        <param name=MaxBackupIndex value=\"7\" />") S_LF;
	config += _T("        <layout class=\"PatternLayout\">") S_LF;
	config += _T("           <param name=ConversionPattern value=\"[%d{MMM/dd/yyyy HH:mm:ss} %7t %-5p] %m%n\"/>") S_LF;
	config += _T("        </layout>") S_LF;
	config += _T("    </appender>") S_LF;
	config += S_LF;
	//	Loggers
	config += _T("    <category name=\""); config += L4C_LOGGER(HTTPSERVER_SVC_MAIND_LOGGER); config += _T("\" additivity=\"false\">") S_LF;
	config += _T("        <priority value=\"Info\" />") S_LF;
	config += _T("        <appender-ref ref=\""); config += L4C_APPENDER(HTTPSERVER_SVCMAIND_APPENDER); config += _T("\"/>") S_LF;
	config += _T("    </category>") S_LF;
	config += S_LF;
	config += _T("    <category name=\""); config += L4C_LOGGER(HTTPSERVER_SVC_HTTPD_LOGGER); config += _T("\" additivity=\"false\">") S_LF;
	config += _T("        <priority value=\"Info\" />") S_LF;
	config += _T("        <appender-ref ref=\""); config += L4C_APPENDER(HTTPSERVER_HTTPD_APPENDER); config += _T("\"/>") S_LF;
	config += _T("    </category>") S_LF;
	config += S_LF;
	config += _T("    <category name=\""); config += L4C_LOGGER(SOCKHTTPSERVER_LOGGER); config += _T("\" additivity=\"false\">") S_LF;
	config += _T("        <priority value=\"Info\" />") S_LF;
	config += _T("        <appender-ref ref=\""); config += L4C_APPENDER(HTTPSERVER_HTTPD_APPENDER); config += _T("\"/>") S_LF;
	config += _T("    </category>") S_LF;
	config += S_LF;
	config += _T("    <category name=\""); config += L4C_LOGGER(BASE_PRODUCT_LOGGER); config += _T("\" additivity=\"false\">") S_LF;
	config += _T("        <priority value=\"Info\" />") S_LF;
	config += _T("        <appender-ref ref=\""); config += L4C_APPENDER(HTTPSERVER_SVCMAIND_APPENDER); config += _T("\"/>") S_LF;
	config += _T("    </category>") S_LF;
	config += S_LF;
	config += _T("    <root>") S_LF;
	config += _T("        <priority value=\"error\" />") S_LF;
	config += _T("        <appender-ref ref=\""); config += L4C_APPENDER(HTTPSERVER_SVCMAIND_APPENDER); config += _T("\"/>") S_LF;
	config += _T("    </root>") S_LF;
	config += S_LF;
	config += _T("</logger:configuration>") S_LF;
	return config;
}

class CAllowedNetwork
{
public:
	CAllowedNetwork()
	{
		dwSubnet = dwMask = 0;
	}
	DWORD   dwSubnet;
	DWORD   dwMask;
};
class CAllowedNetworks : public CArray<CAllowedNetwork, const CAllowedNetwork&>
{
public:
	BOOL Load(LPCTSTR szAllowedNetworks);
	BOOL CheckForAddress(DWORD dwAddress);

private:
	static BOOL IsAddressInSubnet(DWORD dwAddress, DWORD dwSubnet, DWORD dwMask);
	static void GetSubnetAndMask(const CString& sSubnet, const CString& sMask, DWORD& dwSubnet, DWORD& dwMask);
};

BOOL CAllowedNetworks::Load(LPCTSTR szAllowedNetworks)
{
	CFields entries(szAllowedNetworks, ';');
	for (int nIndex = 0; nIndex < entries.GetSize(); nIndex++)
	{
		if (entries[nIndex] == "") continue;

		CFields fld(entries[nIndex], '/');
		CString sSubnet, sMask;
		sSubnet = fld[0];
		if (fld.GetSize() > 1)
		{
			sMask = fld[1];
		}

		CAllowedNetwork entry;
		entry.dwSubnet = 0;
		entry.dwMask = 0xffffffff;
		GetSubnetAndMask(sSubnet, sMask, entry.dwSubnet, entry.dwMask);
		Add(entry);
	}
	return entries.GetSize() > 0;
}

BOOL CAllowedNetworks::CheckForAddress(DWORD dwAddress)
{
	for (int i = 0; i < GetCount(); i++)
	{
		const CAllowedNetwork& net = GetAt(i);
		if (IsAddressInSubnet(dwAddress, net.dwSubnet, net.dwMask)) return true;
	}
	return false;
}

BOOL CAllowedNetworks::IsAddressInSubnet(DWORD dwAddress, DWORD dwSubnet, DWORD dwMask)
{
	return (dwAddress&dwMask) == (dwSubnet&dwMask);
}

void CAllowedNetworks::GetSubnetAndMask(const CString& sSubnet, const CString& sMask, DWORD& dwSubnet, DWORD& dwMask)
{
	dwSubnet = ntohl(CSockAddr::InetAddr(sSubnet));
	if (sMask != _T(""))
	{
		if (sMask.Find('.') != -1)
		{
			//	w.x.y.z format
			dwMask = ntohl(CSockAddr::InetAddr(sMask));
		}
		else
		{
			dwMask = 0;
			int nBits = _tstoi(sMask);
			DWORD dwBitIndex = 0x80000000;
			for (int i = 0; i < nBits; i++)
			{
				dwMask |= dwBitIndex;
				dwBitIndex >>= 1;
			}
		}
	}
}

DWORD WINAPI CHTTPServerSvc::MainThread(void * p)
{
	LPCTSTR szModuleName = _T("HTTPServerSvc");
	CSock::Startup();
	INIT_DEBUG_APPENDER(szModuleName);

#if 0
#ifdef _DEBUG
	CPatternLayout pl;
	CString outstring;
	CLoggingEvent myevent;
	myevent.m_appenderClass = "myappender";
	myevent.m_categoryName = "mycategory";
	myevent.m_ellapsedTime = 1234567;
	myevent.m_message = "The message";
	myevent.m_priority = CLogPriority::Info;
	myevent.m_threadName = "ThreadName";
	pl.SetParam(_T("ConversionPattern"), "%d [%-7p] [%-7t] %m%n");
	pl.GetFormattedMessage(myevent, outstring);
	LDA(_T("Outtring:'%s'"), outstring);
	myevent.m_message = "The message 2";
	myevent.m_priority = CLogPriority::Debug;
	pl.GetFormattedMessage(myevent, outstring);
	LDA(_T("Outtring:'%s'"), outstring);
	int nLenPadding = 5;
	LDA(_T("'%s'"), CPatternFormat::PadString("INFO"), CPatternFormat::PaddingRight, nLenPadding));
	LDA(_T("'%s'"), CPatternFormat::PadString("INFO"), CPatternFormat::PaddingLeft, nLenPadding));
	nLenPadding = 6;
	LDA(_T("'%s'"), CPatternFormat::PadString("INFO"), CPatternFormat::PaddingRight, nLenPadding));
	LDA(_T("'%s'"), CPatternFormat::PadString("INFO"), CPatternFormat::PaddingLeft, nLenPadding));
#endif
#endif

	CNTService& ntserv = *((CNTService *)p);
	g_hEventServerWantToStop = ntserv.m_hEventServerWantToStop;

	//	Chargement config depuis le registre
	CHTTPServerSvcConfig config;
	config.Load();

	SetEnv(_T("EXEDIR"), GetModuleDirectoryStr());
	SetEnv(_T("INSTALLDIR"), GetModuleDirectoryStr());
	SetEnv(STR_ENV_VAR_HTTPSERVERSVC_LOG_DIR, config.GetLogDir());

	//	Creation LogDir
	//CreateDirectoryAndSub(g_config.GetLogDir());

	//	Load log configuration
	CString eventMessage, text;
	eventMessage = _T("\n");
	CString errorLoad, errorStart;
	//	Starting factory before creating DLG objec
	BOOL loadFactory = g_logFactory.LoadFactory(
		szModuleName, GetLogConfig(), errorLoad, true);
	if (!loadFactory)
		text.Format(_T("Error while loading logging factory :\n%s"), errorLoad);
	else
		text.Format(_T("Info: Logging configuration successfully loaded"));
	eventMessage += text;
	eventMessage += _T("\n");

	if (!loadFactory)
	{
		LDAE(_T("Load factory error:%s\n"), errorLoad);
	}

	if (config.IsDebugMode())
	{
		LDA(_T("Setting debug mode for all loggers"));
		g_logFactory.SetLoggersThreshold(L4C_LOGGER(BASE_PRODUCT_LOGGER), LP_Debug, true);
	}

	CFileAppender * pFileAppender = (CFileAppender *)g_logFactory.GetAppenderPtr(L4C_APPENDER(HTTPSERVER_SVCMAIND_APPENDER));
	if (pFileAppender)
	{
		CString error;
		LDA(_T("Cur file name: '%s'"), pFileAppender->GetFileName());
		CString sLogFile;
		sLogFile.Format(_T("%%%s%%\\%sD.log"), STR_ENV_VAR_HTTPSERVERSVC_LOG_DIR, GetServiceName());
		pFileAppender->SetParam(_T("File"), sLogFile);
		if (!pFileAppender->StartAppender(error))
		{
			_tprintf(_T("Unable to open log file '%s' (%s)\n"), (LPCTSTR)pFileAppender->GetFileName(), (LPCTSTR)error);
		}
		else
		{
			_tprintf(_T("Main log file: '%s'\n"), (LPCTSTR)pFileAppender->GetFileName());
		}
	}
	pFileAppender = (CFileAppender *)g_logFactory.GetAppenderPtr(L4C_APPENDER(HTTPSERVER_HTTPD_APPENDER));
	if (pFileAppender)
	{
		CString error;
		LDA(_T("Cur file name: '%s'"), pFileAppender->GetFileName());
		CString sLogFile;
		sLogFile.Format(_T("%%%s%%\\%sAccess.log"), STR_ENV_VAR_HTTPSERVERSVC_LOG_DIR, GetServiceName());
		pFileAppender->SetParam(_T("File"), sLogFile);
		if (!pFileAppender->StartAppender(error))
		{
			_tprintf(_T("Unable to open log file '%s' (%s)\n"), (LPCTSTR)pFileAppender->GetFileName(), (LPCTSTR)error);
		}
		else
		{
			_tprintf(_T("Access log file: '%s'\n"), (LPCTSTR)pFileAppender->GetFileName());
		}
	}

	LDA(_T("LoadConfig:%s"), text);
	LDA(_T("Config:\n%s"), g_logFactory.GetDumpConfiguration());

	//	Starting logging configuration
	BOOL startFactory = g_logFactory.StartFactory(errorStart);
	if (!startFactory)
		text.Format(_T("Error while starting logging factory :\n%s"), errorStart);
	else
		text.Format(_T("Info: Logging factory successfully started"));
	eventMessage += text;
	eventMessage += _T("\n");

	if (!startFactory)
	{
		LDAE(_T("Start factory error:%s\n"), errorStart);
	}

	//	Save log config loading into event log
	CEventLog eventlog(_T("HTTPServer Service"));
	if (eventlog.OpenEventLog())
	{
		eventlog.WriteEventLog(eventMessage, (loadFactory&&startFactory) ? CEventLog::Info : CEventLog::Error);
		eventlog.CloseEventLog();
	}

	//	Get logger for main category
	CHTTPServerSvc::M_LOGGER = g_logFactory.GetLoggerPtr(L4C_LOGGER(HTTPSERVER_SVC_MAIND_LOGGER));

	CHTTPServerHTTPD::M_LOGGER = g_logFactory.GetLoggerPtr(L4C_LOGGER(HTTPSERVER_SVC_HTTPD_LOGGER));

	//CAppenderSkeleton& defaultAppender = g_logFactory.GetAppender(HTTPSERVER_DEFAULT_APPENDER);
	//g_logFile = defaultAppender.GetParam("File");

	//	Affichage infos pour l'execution courante
	MAIN_LOGInfof(_T("=========================================================="));
	MAIN_LOGInfof(_T("%s build %s"), GetProgramName(), GetModuleDateTimeStr(NULL, false));
	MAIN_LOGInfof(_T("Version %s - %s"), VER_HTTPSERVER_FULL, COPYRIGHT);

	TCHAR userName[200];
	ULONG cbUserName = sizeof(userName);
	if (GetUserName(userName, &cbUserName))
	{
		MAIN_LOGInfof(_T("%s started with account '%s'"), GetProgramName(), userName);
	}
	else
	{
		MAIN_LOGInfof(_T("Unable to retrieve user name (%s)"), GetLastErrorString());
	}

	MAIN_LOGDebugf(_T("Debug mode activated"));
	MAIN_LOGInfof(GetOSInfos());
	MAIN_LOGDebugf(_T("Logging configuration:"));
	MAIN_LOGDebugf(g_logFactory.GetConfiguration());
	LDA(g_logFactory.GetConfiguration());

	/*DWORD dwSockBufferSize;
	if (RegGetUserHTTPServerSocketBufferSize(dwSockBufferSize))
	{
	DWORD dwOldValue = CSockZ::SetRawBufferLength(dwSockBufferSize);
	LOGInfof("Socket buffer size overridden with %u bytes (old was %u bytes, Diff:%d bytes)"), dwSockBufferSize, dwOldValue, dwSockBufferSize-dwOldValue);
	}*/

	DWORD dwThreadID = GetNewThreadID();

	MAIN_LOGInfof(_T("[%u] Starting main thread.."), dwThreadID);

	IFLOGInfof(_T("Listening address     : %s"), RegGetUserHTTPServerSvcHTTPDListenAddr());
	IFLOGInfof(_T("Listening port        : %s"), RegGetUserHTTPServerSvcHTTPDListenPort());
	IFLOGInfof(_T("Log directory         : %s"), RegGetUserHTTPServerSvcLogDir());
	CSockHTTPServerHTTPServer::m_sDocumentRootDirectory = RegGetUserHTTPServerSvcWWWDir();
	IFLOGInfof(_T("DocumentRoot directory: %s"), CSockHTTPServerHTTPServer::m_sDocumentRootDirectory);
	IFLOGInfof(_T("Allowed networks      : %s"), RegGetUserHTTPServerSvcAllowedNetworks());
	IFLOGInfof(_T("Directory index       : %s"), RegGetUserHTTPServerSvcDirectoryIndex());

	if (!CreateDirectoryAndSub(CSockHTTPServerHTTPServer::m_sDocumentRootDirectory))
	{
		IFLOGErrorf(_T("Unable to create DocumentRoot directory '%s' (%s)"), CSockHTTPServerHTTPServer::m_sDocumentRootDirectory, GetLastErrorString());
	}

	CAllowedNetworks allowedNetworks;
	allowedNetworks.Load(RegGetUserHTTPServerSvcAllowedNetworks());

	CString sError;
	CSock sockListenHTTPServerHTTPD;
	//	Socket HTTPServerHTTPD
	CreateListeningTCPSocket(_T("HTTPServerHTTPD"), sockListenHTTPServerHTTPD, dwThreadID, config.GetHTTPServerHTTPDListenAddr(), config.GetHTTPServerHTTPDListenPort());

	//	Demarrage SGBD
	//	Register callback to be informed of db updates
	MAIN_LOGDebugf(g_logFactory.GetConfiguration());

	ASSERT(g_hEventServerWantToStop);
	LONG lRetEvent;
	for (;;)
	{
		//	Demande d'arret du service ????
		lRetEvent = WaitForSingleObject(g_hEventServerWantToStop, 1);
		if ((lRetEvent - WAIT_OBJECT_0) == 0) break;

		//	Une connexion HTTP est-elle arriv�e ?
		if (sockListenHTTPServerHTTPD.IsReadReady())
		{
			UINT uID = GetNewThreadID();
			CSock * pSockAccept;
			if (!sockListenHTTPServerHTTPD.Accept(&pSockAccept, _T("AcceptHTTPD")))
			{
				MAIN_LOGErrorf(_T("[%u] Error while accepting connection on %s (%s)"), dwThreadID, sockListenHTTPServerHTTPD.GetSockTitle(), sockListenHTTPServerHTTPD.GetLastErrorString());
			}
			else
			{
				CSockAddr addrpeer; CSockPort portpeer;
				pSockAccept->GetPeerName(addrpeer, portpeer);
				MAIN_LOGInfof(_T("[%u] new connection arrived on '%s' [Peer: %s:%d] (ID:%u)"),
					dwThreadID, sockListenHTTPServerHTTPD.GetSockTitle(), addrpeer.GetHostName(), portpeer.HostNumber(), uID);
				if (!allowedNetworks.CheckForAddress(ntohl(addrpeer.GetDWordNumber())))
				{
					LOGWarnf(_T("[%u] IP '%s' not authorized, connection refused."), dwThreadID, addrpeer.String());
					pSockAccept->Close();
					Sleep(100);
					delete pSockAccept;
				}
				else
				{
					LOGWarnf(_T("[%u] IP '%s' authorized, connection accepted."), dwThreadID, addrpeer.String());
					CString sSockTitle;
					sSockTitle.Format(_T("SocketID:%04X"), pSockAccept->GetSocketID());
					pSockAccept->SetSockTitle(sSockTitle);
					tt_thread_param * pThreadParam;
					pThreadParam = new tt_thread_param;
					pThreadParam->uID = uID;
					pThreadParam->pSock = pSockAccept;
					CloseHandle(NewThread(CHTTPServerHTTPD::HTTPServerHTTPDThread, pThreadParam));
				}
			}
		}
		Sleep(1);
	}

	MAIN_LOGInfof(_T("[%u] Main thread ending..."), dwThreadID);
	Sleep(2000);
	//	Attente pour que tous les threads soient termin�s
	MAIN_LOGInfof(_T("[%u] Main thread ended."), dwThreadID);
	SetEvent(ntserv.m_hEventClientThreadStopped);

	FREE_DEBUG_APPENDER;
	//LOGDESTROYALL(false);
	return 0;
		}

LPCTSTR GetServiceName()
{
	static TCHAR szServiceName[300];
	StringCchPrintf(szServiceName, TAB_SIZE(szServiceName), _T("%s"), GetFileNameComposant(GetModuleFileNameStr(), GFNC_FILENAME));
	return szServiceName;
}

LPCTSTR GetDisplayName()
{
	static TCHAR szDisplayName[300];
	StringCchPrintf(szDisplayName, TAB_SIZE(szDisplayName), _T("%s - HTTP Server"), GetServiceName());
	return szDisplayName;
}

LPCTSTR GetDescription()
{
	static TCHAR szDescription[300];
	StringCchPrintf(szDescription, TAB_SIZE(szDescription), _T("%s - HTTP Server"), GetServiceName());
	return szDescription;
}

void DisplayVersion()
{
	_tprintf(_T("%s version %s - %s\n"), (LPCTSTR)GetServiceName(), VER_HTTPSERVER_FULL, _PROCESSOR_ARCH);
}

#pragma warning(disable: 4996)
BOOL GetOSDisplayString(CString& sOSInfos)
{
	typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
	typedef BOOL(WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);

	OSVERSIONINFOEX osvi;
	SYSTEM_INFO si;
	PGNSI pGNSI;
	PGPI pGPI;
	BOOL bOsVersionInfoEx;
	DWORD dwType;

	ZeroMemory(&si, sizeof(SYSTEM_INFO));
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	bOsVersionInfoEx = GetVersionEx((OSVERSIONINFO*)&osvi);

	//if(bOsVersionInfoEx != NULL ) return 1;

	// Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

	pGNSI = (PGNSI)GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetNativeSystemInfo");
	if (NULL != pGNSI) pGNSI(&si);
	else GetSystemInfo(&si);

	if (VER_PLATFORM_WIN32_NT == osvi.dwPlatformId &&
		osvi.dwMajorVersion > 4)
	{
		sOSInfos = TEXT("Microsoft ");

		// Test for the specific product.

		if (osvi.dwMajorVersion == 6)
		{
			if (osvi.dwMinorVersion == 0)
			{
				if (osvi.wProductType == VER_NT_WORKSTATION)
					sOSInfos += TEXT("Windows Vista ");
				else
					sOSInfos += TEXT("Windows Server 2008 ");
			}

			if (osvi.dwMinorVersion == 1)
			{
				if (osvi.wProductType == VER_NT_WORKSTATION)
					sOSInfos += TEXT("Windows 7 ");
				else sOSInfos += TEXT("Windows Server 2008 R2 ");
			}

			pGPI = (PGPI)GetProcAddress(
				GetModuleHandle(TEXT("kernel32.dll")),
				"GetProductInfo");

			pGPI(osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

#ifndef PRODUCT_PROFESSIONAL
#define PRODUCT_PROFESSIONAL 0x00000030
#endif
#ifndef SM_SERVERR2
#define SM_SERVERR2 89
#endif
#ifndef VER_SUITE_WH_SERVER
#define VER_SUITE_WH_SERVER	0x00008000
#endif
			switch (dwType)
			{
			case PRODUCT_ULTIMATE:
				sOSInfos += TEXT("Ultimate Edition");
				break;
			case PRODUCT_PROFESSIONAL:
				sOSInfos += TEXT("Professional");
				break;
			case PRODUCT_HOME_PREMIUM:
				sOSInfos += TEXT("Home Premium Edition");
				break;
			case PRODUCT_HOME_BASIC:
				sOSInfos += TEXT("Home Basic Edition");
				break;
			case PRODUCT_ENTERPRISE:
				sOSInfos += TEXT("Enterprise Edition");
				break;
			case PRODUCT_BUSINESS:
				sOSInfos += TEXT("Business Edition");
				break;
			case PRODUCT_STARTER:
				sOSInfos += TEXT("Starter Edition");
				break;
			case PRODUCT_CLUSTER_SERVER:
				sOSInfos += TEXT("Cluster Server Edition");
				break;
			case PRODUCT_DATACENTER_SERVER:
				sOSInfos += TEXT("Datacenter Edition");
				break;
			case PRODUCT_DATACENTER_SERVER_CORE:
				sOSInfos += TEXT("Datacenter Edition (core installation)");
				break;
			case PRODUCT_ENTERPRISE_SERVER:
				sOSInfos += TEXT("Enterprise Edition");
				break;
			case PRODUCT_ENTERPRISE_SERVER_CORE:
				sOSInfos += TEXT("Enterprise Edition (core installation)");
				break;
			case PRODUCT_ENTERPRISE_SERVER_IA64:
				sOSInfos += TEXT("Enterprise Edition for Itanium-based Systems");
				break;
			case PRODUCT_SMALLBUSINESS_SERVER:
				sOSInfos += TEXT("Small Business Server");
				break;
			case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
				sOSInfos += TEXT("Small Business Server Premium Edition");
				break;
			case PRODUCT_STANDARD_SERVER:
				sOSInfos += TEXT("Standard Edition");
				break;
			case PRODUCT_STANDARD_SERVER_CORE:
				sOSInfos += TEXT("Standard Edition (core installation)");
				break;
			case PRODUCT_WEB_SERVER:
				sOSInfos += TEXT("Web Server Edition");
				break;
			}
			}

		if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2)
		{
			if (GetSystemMetrics(SM_SERVERR2))
				sOSInfos += TEXT("Windows Server 2003 R2, ");
			else if (osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER)
				sOSInfos += TEXT("Windows Storage Server 2003");
			else if (osvi.wSuiteMask & VER_SUITE_WH_SERVER)
				sOSInfos += TEXT("Windows Home Server");
			else if (osvi.wProductType == VER_NT_WORKSTATION &&
				si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
			{
				sOSInfos += TEXT("Windows XP Professional x64 Edition");
			}
			else sOSInfos += TEXT("Windows Server 2003, ");

			// Test for the server type.
			if (osvi.wProductType != VER_NT_WORKSTATION)
			{
				if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64)
				{
					if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
						sOSInfos += TEXT("Datacenter Edition for Itanium-based Systems");
					else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
						sOSInfos += TEXT("Enterprise Edition for Itanium-based Systems");
				}

				else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
				{
					if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
						sOSInfos += TEXT("Datacenter x64 Edition");
					else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
						sOSInfos += TEXT("Enterprise x64 Edition");
					else sOSInfos += TEXT("Standard x64 Edition");
				}

				else
				{
					if (osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER)
						sOSInfos += TEXT("Compute Cluster Edition");
					else if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
						sOSInfos += TEXT("Datacenter Edition");
					else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
						sOSInfos += TEXT("Enterprise Edition");
					else if (osvi.wSuiteMask & VER_SUITE_BLADE)
						sOSInfos += TEXT("Web Edition");
					else sOSInfos += TEXT("Standard Edition");
				}
			}
		}

		if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1)
		{
			sOSInfos += TEXT("Windows XP ");
			if (osvi.wSuiteMask & VER_SUITE_PERSONAL)
				sOSInfos += TEXT("Home Edition");
			else sOSInfos += TEXT("Professional");
		}

		if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0)
		{
			sOSInfos += TEXT("Windows 2000 ");

			if (osvi.wProductType == VER_NT_WORKSTATION)
			{
				sOSInfos += TEXT("Professional");
			}
			else
			{
				if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
					sOSInfos += TEXT("Datacenter Server");
				else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
					sOSInfos += TEXT("Advanced Server");
				else sOSInfos += TEXT("Server");
			}
		}

		// Include service pack (if any) and build number.

		if (_tcslen(osvi.szCSDVersion) > 0)
		{
			sOSInfos += TEXT(" ");
			sOSInfos += osvi.szCSDVersion;
		}

		TCHAR buf[80];

		StringCchPrintf(buf, 80, TEXT(" (build %d)"), osvi.dwBuildNumber);
		sOSInfos += buf;

		if (osvi.dwMajorVersion >= 6)
		{
			if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
				sOSInfos += TEXT(", 64-bit");
			else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_INTEL)
				sOSInfos += TEXT(", 32-bit");
		}

		return TRUE;
		}
	else
	{
		sOSInfos = ("This sample does not support this version of Windows.");
		return FALSE;
	}
	}

void UpdateValues(const CString& sCurValues, const CString& sOtherValues, CString& sNewValues, int operation)
{
	if (operation == 0)
	{
		sNewValues = sOtherValues;
	}
	else
	{
		CFields curValues(sCurValues, ';');
		CFields otherValues(sOtherValues, ';');

		for (int other = 0; other < otherValues.GetSize(); other++)
		{
			int indexFound = -1;
			for (int cur = 0; cur < curValues.GetSize(); cur++)
			{
				if (curValues[cur].Compare(otherValues[other]) == 0)
				{
					indexFound = cur;
					break;
				}
			}
			if (operation == 1)
			{
				if (indexFound == -1)
				{
					curValues.Add(otherValues[other]);
				}
			}
			else if (operation == 2)
			{
				if (indexFound != -1)
				{
					curValues.RemoveAt(indexFound);
				}
			}
		}
		//	rebuild indexes
		for (int i = 0; i < curValues.GetSize(); i++)
		{
			if (sNewValues != "") sNewValues += ";";
			sNewValues += curValues[i];
		}
	}
}

LPCTSTR GetCommandLineArgumentsHelp()
{
	return
		_T("Available command line arguments: \n"
			"                   -install         : install as a service\n"
			"                   -uninstall       : uninstall service\n"
			"                   -start           : start service\n"
			"                   -stop            : stop service\n"
			"                   -status          : status of service\n"
			"                   -f               : start in foreground mode\n"
			"                   -v               : display version\n"
			"                   -listaddr <addr> : set listening address\n"
			"                   -listport <port> : set listening port\n"
			"                   -logdir <dir>    : set log directory\n"
			"                   -docrootdir <dir>: set document root directory\n"
			"                   -dirindex <res.> : set directory index(es) (see multivalued parameter)\n"
			"                   -allownet <net.> : set allowed source networks (see multivalued parameter)\n"
			"                   -show            : Display configuration\n"
			"                   -docgen          : Generate documentation\n"
			" set '-' value to restore parameter to its default value.\n"
			" multivalued parameter:\n"
			"    each value is ';' separated (eg: <value1>;<value2>;...;<valueN>\n"
			"    +<value> for appending a new value\n"
			"    -<value> for removing an exsiting value\n"
			" for networks, supported format are:\n"
			"    w.x.y.z/CIDR (eg: 192.168.10.0/24)\n"
			"    w.x.y.z/mask (eg: 192.168.10.0/255.255.255.0)\n"
			"");
}

BOOL GetDocInfos(CString& sHistoryTitle, CString& sHistoryText, CString& sHelpContent)
{
	sHistoryTitle.Format(_T("Command line usage for %s"), (LPCTSTR)GetDisplayName());
	sHelpContent.Format(
		_T("%s Version %s.%s\n"
			"%s")
		, (LPCTSTR)GetServiceName(), VER_HTTPSERVER_MAJOR, VER_HTTPSERVER_REVISION, (LPCTSTR)GetCommandLineArgumentsHelp());

	CRes resHisto(IDR_HISTO, _T("HISTO"));
	if (!resHisto.Load()) return FALSE;

	TCHAR * p = new TCHAR[resHisto.GetSize() + 1];
	resHisto.Lock();
	memcpy(p, resHisto.GetPtr(), resHisto.GetSize());
	p[resHisto.GetSize()] = '\0';
	resHisto.Unlock();
	CFields fld(p, '\n');
	delete p;
	//	Suppression Header et tailer
	//fld.RemoveAt(0, 5);
	//fld.RemoveAt(fld.GetSize()-1);
	BOOL fFilterHisto = TRUE;
	if (GAKS_LSHIFT) fFilterHisto = FALSE;
	for (int i = 0; i < fld.GetSize(); i++)
	{
		if (fld[i].GetLength() > 0)
		{
#ifndef _DEBUG
			if (fFilterHisto && fld[i].GetAt(0) == '!') continue;
#endif
			sHistoryText += fld[i];
			sHistoryText += '\n';
		}
	}
	return TRUE;
}

void GenerateDocumentation()
{
	_tprintf(_T("Generating documentation in HTML format...\n"));
	CString sHistoryHtmlFile;
	sHistoryHtmlFile.Format(_T("%s\\%s.html"), (LPCTSTR)GetEnv(_T("TEMP")), (LPCTSTR)GetServiceName());

	CHistoryMakerHtml hmh;
	DeleteFile(sHistoryHtmlFile);
	CString sHistoryTitle, sHistoryText, sHelpContent;
	if (!GetDocInfos(sHistoryTitle, sHistoryText, sHelpContent) ||
		!hmh.DoFromTextToFile(GetDisplayName(), sHistoryText, sHistoryHtmlFile, sHistoryTitle, sHelpContent))
	{
		::MessageBox(NULL, _T("Unable to create html documentation file."), _T("Error"), MB_ICONSTOP);
		return;
	}
	_tprintf(_T("Documentation should be displayed in a short time in your default HTML browser.\n"));
	if (ShellExecute(NULL, _T("open"), sHistoryHtmlFile, NULL, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		CString msg;
		msg.Format(_T("Unable to display versioning file (%s)"), GetLastErrorString());
		::MessageBox(NULL, msg, _T("Error"), MB_ICONSTOP);
		return;
	}
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	CNTService ntservice(SZSERVICENAME, SZSERVICEDISPLAYNAME, SZSERVICEDESCRIPTION, CHTTPServerSvc::MainThread);

	CHTTPServerCommon::ms_sRegEntryServiceParameters.Format(_T("SYSTEM\\CurrentControlSet\\services\\%s\\Parameters"), GetServiceName());
	CHTTPServerCommon::ms_sRegEntryHKCU.Format(_T("%s%s"), STR_REGISTRY_BASE_ENTRY, GetServiceName());

#define CHECK_PARAM()	\
	if (i+1 >= argc) \
	{ \
		_tprintf(_T("Error : parameter not specified for '%s'\n"), argv[i]); \
		exit(1); \
	}

	if (argc > 1)
	{
		DisplayVersion();
		for (int i = 1; i < argc; i++)
		{
			if ((*argv[i] == '-') || (*argv[i] == '/'))
			{
				if (!_tcsicmp(_T("install"), argv[i] + 1))
				{
					ntservice.InstallService();
				}
				else if (!_tcsicmp(_T("uninstall"), argv[i] + 1))
				{
					ntservice.RemoveService();
				}
				else if (!_tcsicmp(_T("start"), argv[i] + 1))
				{
					ntservice.Start();
				}
				else if (!_tcsicmp(_T("stop"), argv[i] + 1))
				{
					ntservice.Stop();
				}
				else if (!_tcsicmp(_T("status"), argv[i] + 1))
				{
					ntservice.Status();
				}
				else if (!_tcsicmp(_T("c"), argv[i] + 1) || !_tcsicmp(_T("f"), argv[i] + 1))
				{
					ntservice.ConsoleMode();
				}
				else if (!_tcsicmp(_T("v"), argv[i] + 1) || !_tcsicmp(_T("version"), argv[i] + 1))
				{
				}
				else if (!_tcsicmp(_T("docgen"), argv[i] + 1))
				{
					GenerateDocumentation();
				}
				else if (!_tcsicmp(_T("show"), argv[i] + 1))
				{
					_tprintf(_T("Configuration:\n"));
					_tprintf(_T("--------------\n"));
					_tprintf(_T("  Installation path   : %s\n"), (LPCTSTR)GetModuleFileNameStr());
					_tprintf(_T("  Version             : %s\n"), VER_HTTPSERVER_FULL);
					_tprintf(_T("  Build               : %s\n"), (LPCTSTR)GetModuleDateTimeStr(NULL, false));
					CString sOSInfos;
					GetOSDisplayString(sOSInfos);
					_tprintf(_T("  OS infos            : %s\n"), (LPCTSTR)sOSInfos);
					_tprintf(_T("  Server name         : %s\n"), (LPCTSTR)GetComputerNameStr());
					_tprintf(_T("  Service name        : %s\n"), GetServiceName());
					_tprintf(_T("  Service display name: %s\n"), GetDisplayName());
					_tprintf(_T("  Service description : %s\n"), GetDescription());
					_tprintf(_T("  Listening address   : %s\n"), (LPCTSTR)RegGetUserHTTPServerSvcHTTPDListenAddr());
					_tprintf(_T("  Listening port      : %s\n"), (LPCTSTR)RegGetUserHTTPServerSvcHTTPDListenPort());
					_tprintf(_T("  Log directory       : %s\n"), (LPCTSTR)RegGetUserHTTPServerSvcLogDir());
					_tprintf(_T("  Document root dir.  : %s\n"), (LPCTSTR)RegGetUserHTTPServerSvcWWWDir());
					_tprintf(_T("  Directory index     : %s\n"), (LPCTSTR)RegGetUserHTTPServerSvcDirectoryIndex());
					_tprintf(_T("  Allowed networks    : %s\n"), (LPCTSTR)RegGetUserHTTPServerSvcAllowedNetworks());
					CString status, error;
					BOOL ret = ntservice.GetStatusString(status, error);
					_tprintf(_T("  Service status      : %s\n"), ret ? (LPCTSTR)status : (LPCTSTR)error);
#ifdef _DEBUG
					_tprintf(_T("  Service parameters  : %s\n"), (LPCTSTR)CHTTPServerCommon::ms_sRegEntryServiceParameters);
#endif
				}
				else if (!_tcsicmp(_T("showlogconf"), argv[i] + 1))
				{
					_tprintf(_T("Log configuration:\n"));
					_tprintf(_T("------------------\n"));
					_tprintf(_T("  Loggers:\n"));
					_tprintf(_T("  --------\n"));
					CString name, description;
					for (int index = 0; index < 500; index++)
					{
						if (!CHTTPServerCommon::GetLog4CppLoggerDef(index, name, description)) break;
						if (name == "")
						{
							_tprintf(_T("\n"));
							continue;
						}
						_tprintf(_T("  %-45s : %-20s\n"), (LPCTSTR)name, (LPCTSTR)description);
					}
					_tprintf(_T("  Appenders:\n"));
					_tprintf(_T("  ----------\n"));
					for (int index = 0; index < 500; index++)
					{
						if (!CHTTPServerCommon::GetLog4CppAppenderDef(index, name, description)) break;
						if (name == "")
						{
							_tprintf(_T("\n"));
							continue;
						}
						_tprintf(_T("  %-45s : %-20s\n"), (LPCTSTR)name, (LPCTSTR)description);
					}
				}

#define ISREMOVEVALUE()	(_tcscmp(argv[i], _T("-"))==0)

#define SET_PARAM(_text, _paramname)	\
	if (ISREMOVEVALUE()) \
	{ \
		RegDelHKLMHTTPServerSvc##_paramname(); \
		_tprintf(_T("%s has been reset to default value '%s'.\n"), _text, (LPCTSTR)RegGetUserHTTPServerSvc##_paramname()); \
	} \
	else \
	{ \
		_tprintf(_T("Current %s: '%s'.\n"), _text, (LPCTSTR)RegGetUserHTTPServerSvc##_paramname()); \
		_tprintf(_T("Updating %s with '%s'...\n"), _text, argv[i]); \
		if (!RegSetHKLMHTTPServerSvc##_paramname(argv[i])) { _tprintf(_T("ERROR: Unable to update registry value '%s'. Are you missing some rights? In the meantime, setting value in HKCU...\n"), _text); RegSetHKCUHTTPServerSvc##_paramname(argv[i]); } \
	}

#define SET_PARAMS(_text, _paramname)	\
	if (ISREMOVEVALUE()) \
	{ \
		RegDelHKLMHTTPServerSvc##_paramname(); \
		_tprintf(_T("%s has been reset to default value '%s'.\n"), _text, (LPCTSTR)RegGetUserHTTPServerSvc##_paramname()); \
	} \
	else \
	{ \
		CString sOtherValue = argv[i]; \
		CString sCurValue = RegGetUserHTTPServerSvc##_paramname(); \
		_tprintf(_T("Current %s: '%s'\n"), _text, (LPCTSTR)sCurValue); \
		_tprintf(_T("Updating %s with '%s'...\n"), _text, (LPCTSTR)sOtherValue); \
		CString sNewValues; \
		if (sOtherValue[0] == '+') \
		{ \
			sOtherValue = sOtherValue.Right(sOtherValue.GetLength()-1); \
			UpdateValues(sCurValue, sOtherValue, sNewValues, 1); \
		} \
		else if (sOtherValue[0] == '-') \
		{ \
			sOtherValue = sOtherValue.Right(sOtherValue.GetLength()-1); \
			UpdateValues(sCurValue, sOtherValue, sNewValues, 2); \
		} \
		else \
		{ \
			UpdateValues(sCurValue, sOtherValue, sNewValues, 0); \
		} \
		_tprintf(_T("New %s: '%s'\n"), _text, (LPCTSTR)sNewValues); \
		if (!RegSetHKLMHTTPServerSvc##_paramname(sNewValues)) {_tprintf(_T("ERROR: Unable to update registry value '%s'. Are you missing some rights? In the meantime, setting value in HKCU...\n"), _text); RegSetHKCUHTTPServerSvc##_paramname(argv[i]); } \
	}

				else if (!_tcsicmp(_T("listaddr"), argv[i] + 1))
				{
					CHECK_PARAM();
					i++;
					SET_PARAM(_T("listening address"), HTTPDListenAddr);
				}
				else if (!_tcsicmp(_T("listport"), argv[i] + 1))
				{
					CHECK_PARAM();
					i++;
					SET_PARAM(_T("listening port"), HTTPDListenPort);
				}
				else if (!_tcsicmp(_T("logdir"), argv[i] + 1))
				{
					CHECK_PARAM();
					i++;
					SET_PARAM(_T("log directory"), LogDir);
				}
				else if (!_tcsicmp(_T("docrootdir"), argv[i] + 1))
				{
					CHECK_PARAM();
					i++;
					SET_PARAM(_T("document directory"), WWWDir);
				}
				else if (!_tcsicmp(_T("dirindex"), argv[i] + 1))
				{
					CHECK_PARAM();
					i++;
					SET_PARAMS(_T("directory indexes"), DirectoryIndex);
				}
				else if (!_tcsicmp(_T("allownet"), argv[i] + 1))
				{
					CHECK_PARAM();
					i++;
					SET_PARAMS(_T("allowed networks"), AllowedNetworks);
				}
				else
				{
					_putts(GetCommandLineArgumentsHelp());
					break;
				}
			}
		}
	}
	else
	{
		//	Demarrage du service
		ntservice.StartServiceCtrlDispatcher();
	}
	return 0;
}
