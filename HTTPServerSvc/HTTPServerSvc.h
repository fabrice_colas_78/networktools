#pragma once

#include <sock.h>

#include "resource.h"

#define SZSERVICENAME			GetServiceName()
#define SZSERVICEDISPLAYNAME	GetDisplayName()
#define SZSERVICEDESCRIPTION	GetDescription()
#define SZDEPENDENCIES			""

class CHTTPServerSvc
{
public:
	static DWORD WINAPI MainThread(void * p);
	static BOOL CreateListeningTCPSocket(LPCTSTR name, CSock& sock, DWORD dwThreadID, LPCTSTR szListenAddr, LPCTSTR szListenPort);
	static BOOL CreateListeningUDPSocket(LPCTSTR name, CSock& sock, DWORD dwThreadID, LPCTSTR szListenAddr, LPCTSTR szListenPort);
	static UINT GetNewThreadID();

	DECLLogger(M_LOGGER);
};

class CHTTPServerHTTPD
{
public:
	static DWORD WINAPI HTTPServerHTTPDThread(void * p);

	DECLLogger(M_LOGGER);
};
