#include "StdAfx.h"
#include "HTTPServerCommon.h"

#include <LibSystem.h>
#include <Log.h>
#include <Registry.h>
#include <SpecifLibDefs.h>


CString CHTTPServerCommon::ms_sRegEntryServiceParameters;
CString CHTTPServerCommon::ms_sRegEntryHKCU;

#define _VAL_HKLM				CHTTPServerCommon::ms_sRegEntryServiceParameters	//STR_REG_HTTPSERVER_ROOT_HKLM_KEY
#define _VAL_HKCU				CHTTPServerCommon::ms_sRegEntryHKCU					//STR_REG_HTTPSERVER_ROOT_HKCU_KEY

IMPLEMENT_REGGETSET_PARAM(HTTPServer, SocketBufferSize, DWORD, DWORD, _T("SocketBufferSize"), 0);
IMPLEMENT_REGGETSET_PARAM(HTTPServerSvc, WWWDir, CString, CString, _T("DocumentRoot"), ConcatFileNameComposant(GetModuleDirectoryStr(), _T("www")));
IMPLEMENT_REGGETSET_PARAM(HTTPServerSvc, DirectoryIndex, CString, CString, _T("DirectoryIndex"), _T("default.htm;default.html;index.htm;index.html"));
IMPLEMENT_REGGETSET_PARAM(HTTPServerSvc, LogDir, CString, CString, _T("LogDir"), ConcatFileNameComposant(GetModuleDirectoryStr(), _T("logs")));
IMPLEMENT_REGGETSET_PARAM(HTTPServerSvc, HTTPDListenAddr, CString, CString, _T("HTTPDListenAddr"), _T("0.0.0.0"));
IMPLEMENT_REGGETSET_PARAM(HTTPServerSvc, HTTPDListenPort, CString, CString, _T("HTTPDListenPort"), _T("8080"));
IMPLEMENT_REGGETSET_PARAM(HTTPServerSvc, AllowedNetworks, CString, CString, _T("AllowedNetworks"), _T("0.0.0.0/0"));

struct tt_log4cpp_appenders
{
	Log4CppAppender appenderId;
	LPCTSTR appenderName;
	LPCTSTR appenderDescription;
} g_log4cppAppenders[] = 
{
	{ DEFAULT_APPENDER, _T("DefaultAppender"), _T("Default appender"), },
	{ NOID_APPENDER},
	//	HTTPServerSvc
	{ HTTPSERVER_SVCMAIND_APPENDER, _T("HTTPServerMainD"), _T("HTTPServer main appender"), },
	{ HTTPSERVER_HTTPD_APPENDER, _T("HTTPServerHTTPD"), _T("HTTPServer HTTPD appender"), },
	{ NOID_APPENDER},
};

LPCTSTR CHTTPServerCommon::GetLog4CppAppender(Log4CppAppender appenderId)
{
	for(int i=0; i<TAB_SIZE(g_log4cppAppenders); i++)
	{
		if (g_log4cppAppenders[i].appenderId == appenderId) return g_log4cppAppenders[i].appenderName;
	}
	ASSERT(FALSE);
	return _T("[NoAppenderDefined]");
}

BOOL CHTTPServerCommon::GetLog4CppAppenderDef(int index, CString& name, CString& description)
{
	if (index >= TAB_SIZE(g_log4cppAppenders)) return false;
	name = g_log4cppAppenders[index].appenderName?g_log4cppAppenders[index].appenderName:_T("");
	description = g_log4cppAppenders[index].appenderDescription?g_log4cppAppenders[index].appenderDescription:_T("");
	return true;
}

#define BASE_PRODUCT_CLASS		LOGGER_BASE_CLASS	_T(".HTTPServer")

#define HTTPSERVER_BASE_CLASS	BASE_PRODUCT_CLASS	_T(".Server")

struct tt_log4cpp_loggers
{
	Log4CppLogger loggerId;
	LPCTSTR loggerName;
	LPCTSTR loggerDescription;
} g_log4cppLoggers[]= 
{
	//	Base
	{ BASE_PRODUCT_LOGGER, BASE_PRODUCT_CLASS, _T("HTTPServer base logger"), },
	{ NOID_LOGGER,},
	//	ZeeConfSvc
	{ HTTPSERVER_SVC_MAIND_LOGGER, HTTPSERVER_BASE_CLASS _T(".MainD"), _T("HTTPServer service main logger"), },
	{ HTTPSERVER_SVC_HTTPD_LOGGER, HTTPSERVER_BASE_CLASS _T(".HTTPD"), _T("HTTPServer HTTPD logger"), },
	{ SOCKHTTPSERVER_LOGGER, LOGGER_COMMONLIB_BASE_CLASS _T(".SockHTTPServer"), _T("HTTPServer socket logger"), },
	{ NOID_LOGGER,},
};

LPCTSTR CHTTPServerCommon::GetLog4CppLogger(Log4CppLogger loggerId)
{
	for(int i=0; i<TAB_SIZE(g_log4cppLoggers); i++)
	{
		if (g_log4cppLoggers[i].loggerId == loggerId) return g_log4cppLoggers[i].loggerName;
	}
	ASSERT(FALSE);
	return _T("[NoLoggerDefined]");
}

BOOL CHTTPServerCommon::GetLog4CppLoggerDef(int index, CString& name, CString& description)
{
	if (index >= TAB_SIZE(g_log4cppLoggers)) return false;
	name = g_log4cppLoggers[index].loggerName?g_log4cppLoggers[index].loggerName:_T("");
	description = g_log4cppLoggers[index].loggerDescription?g_log4cppLoggers[index].loggerDescription:_T("");
	return true;
}