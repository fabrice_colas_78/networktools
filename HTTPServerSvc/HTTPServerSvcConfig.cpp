#include "StdAfx.h"
#include "HTTPServerSvcconfig.h"

#include "HTTPServerCommon.h"

#include <fields.h>
#include <registry.h>

CHTTPServerSvcConfig::CHTTPServerSvcConfig(void)
{
	Load();
}

CHTTPServerSvcConfig::~CHTTPServerSvcConfig(void)
{
}

void CHTTPServerSvcConfig::Load()
{
	m_fDebugMode                 = false;
	
	m_sWWWDir                    = RegGetUserHTTPServerSvcWWWDir();

	m_sLogDir                    = RegGetUserHTTPServerSvcLogDir();

	m_sHTTPServerHTTPDListenAddr = RegGetUserHTTPServerSvcHTTPDListenAddr();
	m_sHTTPServerHTTPDListenPort = RegGetUserHTTPServerSvcHTTPDListenPort();
}

const CString& CHTTPServerSvcConfig::GetWWWDir()
{
	return m_sWWWDir;
}

const CString& CHTTPServerSvcConfig::GetLogDir()
{
	return m_sLogDir;
}

CString CHTTPServerSvcConfig::GetHTTPServerHTTPDListenAddr()
{
	return m_sHTTPServerHTTPDListenAddr;
}

CString CHTTPServerSvcConfig::GetHTTPServerHTTPDListenPort()
{
	return m_sHTTPServerHTTPDListenPort;
}
