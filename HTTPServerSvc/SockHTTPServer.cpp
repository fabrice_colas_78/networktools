#include "StdAfx.h"
#include "SockHTTPServer.h"

#include <Fields.h>
#include <SpecifLibDefs.h>

#define VERSION_MAJOR		1
#define VERSION_MINOR		0
#define VERSION_REVISION	1

#define MY_LOGGER_CLASS	

CString CMimeType::GetMimeTypeFromExtension(LPCTSTR ext)
{
	LPCTSTR defaultMimeType = "application/octet-stream";
	static struct tt_mime_type
	{
		LPCTSTR ext;
		LPCTSTR mimeType;
	}
	mimeTypes[] = 
	{
		{ "AU", "audio/basic" },
		{ "BIN", "application/octet-stream" },
		{ "BMP", "image/bmp" },
		{ "DOC", "application/msword" },
		{ "EPS", "application/postscript" },
		{ "EXE", "application/octet-stream" },
		{ "GIF", "image/gif" },
		{ "HLP", "application/winhelp" },
		{ "HTM", "text/html" },
		{ "HTML", "text/html" },
		{ "JPG", "image/jpeg" },
		{ "JS", "application/x-javascript" },
		{ "MDB", "application/x-msaccess" },
		{ "MID", "audio/midi" },
		{ "MIDI", "audio/midi" },
		{ "MOV", "video/quicktime" },
		{ "MP3", "audio/x-mpeg" },
		{ "MPE", "video/mpeg" },
		{ "MPEG", "video/mpeg" },
		{ "MPG", "video/mpeg" },
		{ "PDF", "application/pdf" },
		{ "POT", "application/vnd.ms-powerpoint" },
		{ "PPS", "application/vnd.ms-powerpoint" },
		{ "PPT", "application/vnd.ms-powerpoint" },
		{ "PS", "application/postscript" },
		{ "PUB", "application/x-mspublisher" },
		{ "QT", "video/quicktime" },
		{ "RA", "audio/x-pn-realaudio" },
		{ "RAM", "audio/x-pn-realaudio" },
		{ "RPM", "audio/x-pn-realaudio" },
		{ "RTF", "application/rtf" },
		{ "SND", "audio/basic" },
		{ "TAR", "application/x-tar" },
		{ "TEX", "application/x-tex" },
		{ "TIF", "image/tiff" },
		{ "TIFF", "image/tiff" },
		{ "TXT", "text/plain" },
		{ "WAV", "audio/x-wave" },
		{ "WI", "image/wavelet" },
		{ "WMF", "application/x-msmetafile" },
		{ "WRI", "application/x-mswrite" },
		{ "XLS", "application/vnd.ms-excel" },
		{ "ZIP", "application/zip" },
	};
	for(int i=0; i<sizeof(mimeTypes)/sizeof(mimeTypes[0]); i++)
	{
		if (_tcsicmp(ext, mimeTypes[i].ext) == 0) return mimeTypes[i].mimeType;
	}
	return defaultMimeType;
}

INITLogger(CSockHTTPServer::m_logger);

LPCTSTR CSockHTTPServer::GetClassName()
{ 
	return LOGGER_COMMONLIB_BASE_CLASS _T(".SockHTTPServer"); 
}

CSockHTTPServer::CSockHTTPServer(LPCTSTR szServerName)
{
	GETLogger(M_LOGGER,GetClassName());

	m_bResponseKeepAlive = false;
	m_sServerName.Format(_T("SockHTTPServer/%u.%u.%u at %s"), VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION, GetComputerNameStr());
	if (_tcslen(szServerName) > 0)
		m_sServerName = szServerName;
}

CSockHTTPServer::~CSockHTTPServer(void)
{
}

CHTTPRequest::CHTTPRequest(int nHeaderHashSize)
{
	Init(nHeaderHashSize);
}

void CHTTPRequest::Init(int nHeaderHashSize)
{
	m_pRawRequest[0] = '\0';
	m_dwRawRequestLength = 0;

	m_pRawEntity = NULL;
	m_dwRawEntityLength = 0;

	m_dwHttpVersion = 0;

	m_nContentLength = 0;
	m_bKeepAlive = false;
	
	m_bIfModifiedSincePresent = false;
	memset(&m_IfModifiedSince, 0, sizeof(SYSTEMTIME));

	m_HeaderMap.InitHashTable(nHeaderHashSize);
}

void CHTTPRequest::GetQueryStringParams(CHTTPRequestParams& params)
{
	GetQueryStringParams(m_sExtra, params);
}

void CHTTPRequest::GetQueryStringParams(LPCTSTR queryString, CHTTPRequestParams& params)
{
	CFields qsparams(queryString, '&');
	if (qsparams.GetSize()==0 || qsparams[0] == "") return;
	//LDA("%u get params", qsparams.GetSize());
	for(int i=0; i<qsparams.GetSize(); i++)
	{
		if (qsparams[i] == "") continue;
		CHTTPRequestParam param;
		int nIndexEqualSign = qsparams[i].Find('=');
		if (nIndexEqualSign == -1) 
		{
			param.m_sParam = qsparams[i];
		}
		else
		{
			param.m_sParam = qsparams[i].Left(nIndexEqualSign);
			param.m_sValue = qsparams[i].Right(qsparams[i].GetLength()-nIndexEqualSign-1);
		}
		params.Add(param);
	}
}

void CSockHTTPServer::SetExchangeID(UINT uID)
{
	m_uID = uID;
}

BOOL CSockHTTPServer::SplitRequestLine(LPCTSTR szLine, CString& sParam, CString& sValue)
{
	CString line(szLine);
	int nColumn;
	if ((nColumn = line.Find(':')) == -1) return false;
	sParam = line.Left(nColumn);
	sValue = line.Right(line.GetLength()-nColumn-1);
	sValue = sValue.TrimLeft();
	return true;
}

BOOL CSockHTTPServer::Run(HANDLE hEventServerWantToStop)
{
	int nRecv;
	DWORD nTotalToReceive = (DWORD)-1;
	
	m_Request.m_dwRawRequestLength = 0;
	LONG lRetEvent;
	LPCSTR pszTerminator = "\r\n\r\n";
	LPCSTR pTerminatorFound;
	CString sParam, sValue;
	for(;;)
	{
		if (m_Request.m_dwRawRequestLength >= nTotalToReceive)
		{
			LOGDebugf("Request is ready.");
			break;
		}
		lRetEvent = WaitForSingleObject(hEventServerWantToStop, 10);
		if ((lRetEvent-WAIT_OBJECT_0) == 0) break;
		if (! Recv(m_Request.m_pRawRequest+m_Request.m_dwRawRequestLength, sizeof(m_Request.m_pRawRequest)-m_Request.m_dwRawRequestLength, nRecv))
		{
			LOGErrorf(_T("[%u] HTTPServer recv error %s"), m_uID, GetLastErrorString());
			break;
		}
		m_Request.m_dwRawRequestLength += nRecv;
		if (nRecv == 0)
		{
			// Connection closed by remote host
			LOGDebugf(_T("[%u] HTTPServer connection closed by remote host"), m_uID);
			break;
		}
		if (m_Request.m_dwRawRequestLength >= sizeof(m_Request.m_pRawRequest))
		{
			LOGErrorf(_T("[%u] HTTPServer frame size too long (%u bytes)"), m_uID, m_Request.m_dwRawRequestLength);
			break;
		}
		//	Make a null terminated string
		m_Request.m_pRawRequest[m_Request.m_dwRawRequestLength] = '\0';
		//	Search for entity terminator \r\n\r\n
		pTerminatorFound = strstr(m_Request.m_pRawRequest, pszTerminator);
		if (pTerminatorFound)
		{
			LOGDebugf("Entity terminator found.");
			LPCSTR szCurrentLine = m_Request.m_pRawRequest;
			LPCSTR szNextLine;
			size_t lineLength;
			DWORD dwContentLength = (DWORD)-1;
			for(;;)
			{
				szNextLine = strstr(szCurrentLine, "\r\n");
				lineLength = szNextLine==NULL?_tcslen(szCurrentLine):szNextLine-szCurrentLine;
				char * line = new char[lineLength+1];
				strncpy(line, szCurrentLine, lineLength);
				line[lineLength] = '\0';
				LOGDebugf("Line length:%u '%s'", lineLength, line);
				if (SplitRequestLine(line, sParam, sValue))
				{
					LOGDebugf("  Param:'%s', Value:'%s'", sParam, sValue);
				}
				delete line;
				if (sParam.CompareNoCase("Content-Length") == 0)
				{
					dwContentLength = _tstoi(sValue);
					break;
				}
				if (szNextLine == NULL)
				{
					break;
				}
				szCurrentLine = szNextLine+2;
			}
			if (dwContentLength != (DWORD)-1)
			{
				LOGDebugf("  ContentLength:%u", dwContentLength);
				nTotalToReceive = (DWORD)(pTerminatorFound-m_Request.m_pRawRequest)+(DWORD)_tcslen(pszTerminator)+dwContentLength;
			}
			else
			{
				LOGDebugf("  No ContentLength specified.");
				nTotalToReceive = (DWORD)(pTerminatorFound-m_Request.m_pRawRequest)+(DWORD)_tcslen(pszTerminator);
			}
			LOGDebugf("  TotalToReceive:%u, TotalReceived:%u", nTotalToReceive, m_Request.m_dwRawRequestLength);
		}
	}
	if (M_LOGGER && M_LOGGER->GetThresholdPriority() >= LP_Trace)
		LOGDump(LP_Trace, "Frame", (LPBYTE)m_Request.m_pRawRequest, m_Request.m_dwRawRequestLength);
	IFLOGDebugf("HTTPServer %u bytes received (request length:%u).", m_Request.m_dwRawRequestLength, _tcslen(m_Request.m_pRawRequest));
	if (! ParseRequest())
	{
		return false;
	}

	if (m_Request.m_Verb == CHTTPRequest::HTTP_VERB_GET || m_Request.m_Verb == CHTTPRequest::HTTP_VERB_HEAD || m_Request.m_Verb == CHTTPRequest::HTTP_VERB_POST)
	{
		if (!HandleGetPostHead()) //Allow derived classes to handle GET, HEAD or POST
		{
			CString sMsg;
			sMsg.Format("The requested URL %s was not found on this server.", m_Request.m_sURL); 
			ReturnErrorMessage(404, sMsg); //Not Found
		}
	}
	return true;
}

BOOL CSockHTTPServer::ParseRequest()
{
	LPCSTR pTerminatorHeader = "\r\n\r\n";
	LPCSTR pTerminatorHeaderFound = strstr(m_Request.m_pRawRequest, pTerminatorHeader);
	if (pTerminatorHeaderFound)
	{
		m_Request.m_pRawEntity = (LPBYTE)pTerminatorHeaderFound+4;
		m_Request.m_dwRawEntityLength = (DWORD)(m_Request.m_dwRawRequestLength - (m_Request.m_pRawEntity - (LPBYTE)m_Request.m_pRawRequest));
		IFLOGDebugf("RawEntityLength:%u", m_Request.m_dwRawEntityLength);
	}
	LPCSTR pCurrentLine = m_Request.m_pRawRequest;
	CString sField, sValue;
	BOOL fFirstLine = true;
	for(;;)
	{
		LPCSTR pNextLine = strstr(pCurrentLine, "\r\n");
		int lineLength = (int)(pNextLine==NULL?_tcslen(pCurrentLine):pNextLine-pCurrentLine);
		CString sLine(pCurrentLine, lineLength);
		IFLOGDebugf(" ParseRequest: CurrentLine:'%s'", sLine);
		if (fFirstLine)
		{
			m_Request.m_sRequest = sLine;
			ParseSimpleRequestLine(sLine);
			fFirstLine = false;
		}
		else
		{
			SplitRequestLine(sLine, sField, sValue);
			ParseRequestLine(sField, sValue);
		}
		if (pNextLine == pTerminatorHeaderFound || pCurrentLine == NULL) 
		{
			break;
		}
		pCurrentLine = pNextLine + 2;
	}
	LOGDebugf("Request result:\n"
		"Request       : '%s'\n"
		"Verb          : '%s'\n"
		"URL           : '%s'\n"
		"User-Agent    : '%s'\n"
		"Accept        : '%s'\n"
		"Host          : '%s'\n"
		"Extra         : '%s'\n"
		"Content-Type  : '%s'\n"
		"Content-Length: %u (EntityLength:%u)\n"
		"KeepAlive     : %u\n"
		, m_Request.m_sRequest
		, m_Request.m_sVerb
		, m_Request.m_sURL
		, m_Request.m_sUserAgent
		, m_Request.m_sAccept
		, m_Request.m_sHost
		, m_Request.m_sExtra
		, m_Request.m_sContentType
		, m_Request.m_nContentLength, m_Request.m_dwRawEntityLength
		, m_Request.m_bKeepAlive
		);
	return true;
}

BOOL CSockHTTPServer::ParseRequestLine(const CString& sField, const CString& sValue)
{
	LOGDebugf("    Parsing request line [%s]:[%s]...", sField, sValue);
	//Also add the header line to the header map
	CString sKey(sField);
	sKey.MakeUpper();
	m_Request.m_HeaderMap.SetAt(sKey, sValue);

	BOOL bSuccess = true;
	//Handle any other request headers  
	if (sField.CompareNoCase(_T("If-Modified-Since")) == 0)
	{
		//Handle the If-Modified-Since header
		SYSTEMTIME time;
		if (ParseDate(sValue, time))
		{
			m_Request.m_bIfModifiedSincePresent = TRUE; 
			memcpy(&m_Request.m_IfModifiedSince, &time, sizeof(SYSTEMTIME));
		}
	}
	else if (sField.CompareNoCase(_T("Authorization")) == 0)
	{
		//Handle the Authorization header
		bSuccess = ParseAuthorizationLine(sValue);
	}
	else if (sField.CompareNoCase(_T("Content-Type")) == 0)
		m_Request.m_sContentType = sValue;
	else if (sField.CompareNoCase(_T("Accept")) == 0)
		m_Request.m_sAccept = sValue;
	else if (sField.CompareNoCase(_T("Host")) == 0)
		m_Request.m_sHost = sValue;
	else if (sField.CompareNoCase(_T("User-Agent")) == 0)
		m_Request.m_sUserAgent = sValue;
	else if (sField.CompareNoCase(_T("Content-Length")) == 0)
		m_Request.m_nContentLength = _ttoi(sValue);
	else if ((sField.CompareNoCase(_T("Connection")) == 0) && (sValue.CompareNoCase(_T("Keep-Alive")) == 0))
		m_Request.m_bKeepAlive = TRUE;
	return bSuccess;
}

CString CSockHTTPServer::UTF8ToCString(const CString& sURL)
{
  //Get the length of the string to convert from UTF
  int nInputLen = sURL.GetLength();

  //Allocate memory to hold the Unicode string
  LPWSTR lpwszURL = new WCHAR[nInputLen * 4];

  //Convert the UTF input string to Unicode
  int nInputIndex = 0;
  int nOutputIndex = 0;
  while (nInputIndex < nInputLen)
  {
    BYTE cInputChar1 = static_cast<BYTE>(sURL.GetAt(nInputIndex));
    if (cInputChar1 <= 0x7f)
    {
      //Form the current output character
      lpwszURL[nOutputIndex] = cInputChar1;

      //Move onto the next input character
      nInputIndex++;
    }
    else if (nInputIndex < nInputLen-1 && cInputChar1 <= 0xdf)
    {
      //Form the current output character
      lpwszURL[nOutputIndex] = static_cast<wchar_t>(((cInputChar1 & 0x1f) << 6) + (sURL.GetAt(nInputIndex+1) & 0x3f));

      //Move onto the next input character
      nInputIndex += 2;
    }
    else if (nInputIndex < nInputLen-2)
    {

      //Form the current output character
      lpwszURL[nOutputIndex] = static_cast<wchar_t>(((cInputChar1 & 0x0f) << 12) + ((sURL.GetAt(nInputIndex+1) & 0x3f) << 6) + (sURL.GetAt(nInputIndex+2) & 0x3f));

      //Move onto the next input character
      nInputIndex += 3;
    }
    else
    {
      //skip illegal sequence
      nInputIndex++;
    }

    //Move onto the next output character
    nOutputIndex++;
  }

  //Don't forget to NULL terminate
  lpwszURL[nOutputIndex] = _T('\0');

  //Form a CString version of the Unicode string
  CString sDecodedURL(lpwszURL);

  //Tidy up the heap memory we have been using
  delete [] lpwszURL;

  //Return the string
  return sDecodedURL;
}

int CSockHTTPServer::HexToInt(TCHAR ch)
{
  //character is in range of 0-9 subtract ascii value of '0' to get integer
  if ((ch >= _T('0')) && (ch <= _T('9')))
    return ch - _T('0');

  //character is in range of a-z or A-Z subtract ascii value of 'a' and add 10 to get it converted into int
  if ((ch >= _T('a')) && (ch <= _T('f')))
    return ch - _T('a') + 10;
  else if ((ch >= _T('A')) && (ch <= _T('F')))
    return ch - _T('A') + 10;

  //Character is not a Hex Digit
  return -1;
}

TCHAR CSockHTTPServer::IntToHex(int Character)
{
  //This function only expects nibbles
  ASSERT(Character >= 0 && Character <= 15);
  
  if (Character <= 9)
    return static_cast<TCHAR>(Character + _T('0'));
  else
    return static_cast<TCHAR>(Character - 10 + _T('A'));
}

CString CSockHTTPServer::URLEncode(const CString& sURL)
{
  CString sEncodedURL;
  int nLength = sURL.GetLength();
  LPTSTR pszEncodedURL = sEncodedURL.GetBufferSetLength((nLength*3) + 1);
  int nOutputIndex = 0;
  for (int i=0; i<nLength; i++)
  {
    //Pull out the current character to evaluate
    BYTE CurrentChar = (BYTE) sURL.GetAt(i);

    //Should we encode the character or not? See RFC 1738 for the details.
    if ((CurrentChar >= '0' && CurrentChar <= '9') ||
        (CurrentChar >= 'a' && CurrentChar <= 'z') ||
        (CurrentChar >= 'A' && CurrentChar <= 'Z') ||
        (CurrentChar == '$') ||
        (CurrentChar == '-') ||
        (CurrentChar == '_') ||
        (CurrentChar == '.') ||
        (CurrentChar == '+') ||
        (CurrentChar == '!') ||
        (CurrentChar == '*') ||
        (CurrentChar == '\'') ||
        (CurrentChar == '(') ||
        (CurrentChar == ')') ||
        (CurrentChar == ','))
    {
      pszEncodedURL[nOutputIndex] = CurrentChar;
      ++nOutputIndex;
    }
    else 
    {
      pszEncodedURL[nOutputIndex] = _T('%');
      ++nOutputIndex;

      TCHAR nNibble = IntToHex((CurrentChar & 0xF0) >> 4);
      pszEncodedURL[nOutputIndex] = nNibble;
      ++nOutputIndex;

      nNibble = IntToHex(CurrentChar & 0x0F);  
      pszEncodedURL[nOutputIndex] = nNibble;
      ++nOutputIndex;
    }
  }
  //Don't forget to NULL terminate
  pszEncodedURL[nOutputIndex] = _T('\0');
  sEncodedURL.ReleaseBuffer();

  return sEncodedURL;
}

CString CSockHTTPServer::URLDecode(const CString& sURL)
{
  CString sDecodedURL;
  int nLength = sURL.GetLength();
  LPTSTR pszDecodedURL = sDecodedURL.GetBufferSetLength(nLength + 1);
  int nOutputIndex = 0;
  for (int i=0; i<nLength; i++)
  {
    TCHAR c1 = sURL[i];
    if (c1 != _T('%'))
    {
      if (c1 == '+')
        pszDecodedURL[nOutputIndex] = ' ';  
      else
        pszDecodedURL[nOutputIndex] = c1;
      nOutputIndex++;
    }
    else
    {
      if (i < nLength-2)
      {
        int msb = HexToInt(sURL[i+1]);
        int lsb = HexToInt(sURL[i+2]);
        if (msb != -1 && lsb != -1)
        {
          int nChar = (msb << 4) + lsb;
          pszDecodedURL[nOutputIndex] = TCHAR(nChar);
          nOutputIndex++;
          i += 2;
        }
        else
        {
          pszDecodedURL[nOutputIndex] = c1;
          nOutputIndex++;
        }
      }
      else
      {
        pszDecodedURL[nOutputIndex] = c1;
        nOutputIndex++;
      }
    }
  }
  //Don't forget to NULL terminate
  pszDecodedURL[nOutputIndex] = _T('\0');
  sDecodedURL.ReleaseBuffer();

  return sDecodedURL;
}

BOOL CSockHTTPServer::ParseSimpleRequestLine(const CString& sLine)
{
  //What will be the return value from this function (assume the worst)
  BOOL bSuccess = FALSE;

  //First parse out the VERB
  static TCHAR* pszTokens = _T(" ");
  int nTokenPosition = 0;
  CString sVerb(sLine.Tokenize(pszTokens, nTokenPosition));
  if (sVerb.GetLength())
  {
    m_Request.m_sVerb = sVerb;
    if (sVerb.CompareNoCase(_T("GET")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_GET;
    else if (sVerb.CompareNoCase(_T("POST")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_POST;
    else if (sVerb.CompareNoCase(_T("HEAD")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_HEAD;
    else if (sVerb.CompareNoCase(_T("PUT")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_PUT;
    else if (sVerb.CompareNoCase(_T("LINK")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_LINK;
    else if (sVerb.CompareNoCase(_T("DELETE")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_DELETE;
    else if (sVerb.CompareNoCase(_T("UNLINK")) == 0)
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_UNLINK;
    else
      m_Request.m_Verb = CHTTPRequest::HTTP_VERB_UNKNOWN;

    //Parse out the URL
    CString sURL(sLine.Tokenize(pszTokens, nTokenPosition));
    if (sURL.GetLength())
    {
      m_Request.m_sRawURL = sURL;
      m_Request.m_sURL = UTF8ToCString(URLDecode(sURL)); //Convert any embedded escape sequences to their unencoded format 
                                                         //as well as handling UTF8 input data

      //Handle the Search path i.e everything after the ?
      int nQuestion = m_Request.m_sURL.Find(_T('?'));
      if (nQuestion != -1)
      {
        m_Request.m_sExtra = m_Request.m_sURL.Right(m_Request.m_sURL.GetLength() - nQuestion - 1);
        m_Request.m_sURL = m_Request.m_sURL.Left(nQuestion);
      }
      nQuestion = m_Request.m_sRawURL.Find(_T('?'));
      if (nQuestion != -1)
        m_Request.m_sRawExtra = m_Request.m_sRawURL.Right(m_Request.m_sRawURL.GetLength() - nQuestion - 1);
      //Parse out the HTTP version
      CString sVersion(sLine.Tokenize(pszTokens, nTokenPosition));
      if (sVersion.GetLength())
      {
        if (sVersion.Find(_T("HTTP/")) == 0)
        {
          static TCHAR* pszVersionTokens = _T(".");
          CString sVersionToTokenize(sVersion.Right(sVersion.GetLength() - 5));
          int nVersionTokenPosition = 0;
          CString sMajorVersion(sVersionToTokenize.Tokenize(pszVersionTokens, nVersionTokenPosition));
          if (sMajorVersion.GetLength())
          {
            WORD wMajorVersion = static_cast<WORD>(_ttoi(sMajorVersion));
            CString sMinorVersion(sVersionToTokenize.Tokenize(pszVersionTokens, nVersionTokenPosition));
            if (sMinorVersion.GetLength())
            {
              WORD wMinorVersion = static_cast<WORD>(_ttoi(sMinorVersion));
              m_Request.m_dwHttpVersion = MAKELONG(wMinorVersion, wMajorVersion);
              bSuccess = TRUE;
            }
          }
        }
      }
      else
      {
        //No version included in the request, so set it to HTTP v0.9
        m_Request.m_dwHttpVersion = MAKELONG(9, 0);
        bSuccess = m_Request.m_Verb == CHTTPRequest::HTTP_VERB_GET; //"GET" is only allowed with HTTP v0.9
      }  
    }
  }

  return bSuccess;
}

BOOL CSockHTTPServer::ParseWeekDay(LPCTSTR pszToken, WORD& nWeekDay)
{
  BOOL bSuccess = TRUE;
  if (_tcsicmp(pszToken, _T("Sun")) == 0 || _tcsicmp(pszToken, _T("Sunday")) == 0)
    nWeekDay = 0;
  else if (_tcsicmp(pszToken, _T("Mon")) == 0 || _tcsicmp(pszToken, _T("Monday")) == 0)
    nWeekDay = 1;
  else if (_tcsicmp(pszToken, _T("Tue")) == 0 || _tcsicmp(pszToken, _T("Tuesday")) == 0)
    nWeekDay = 2;
  else if (_tcsicmp(pszToken, _T("Wed")) == 0 || _tcsicmp(pszToken, _T("Wednesday")) == 0)
    nWeekDay = 3;
  else if (_tcsicmp(pszToken, _T("Thu")) == 0 || _tcsicmp(pszToken, _T("Thursday")) == 0)
    nWeekDay = 4;
  else if (_tcsicmp(pszToken, _T("Fri")) == 0 || _tcsicmp(pszToken, _T("Friday")) == 0)
    nWeekDay = 5;
  else if (_tcsicmp(pszToken, _T("Sat")) == 0 || _tcsicmp(pszToken, _T("Saturday")) == 0)
    nWeekDay = 6;
  else
    bSuccess = FALSE;
  return bSuccess;
}

BOOL CSockHTTPServer::ParseMonth(LPCTSTR pszToken, WORD& nMonth)
{
  BOOL bSuccess = TRUE;
  if (_tcsicmp(pszToken, _T("Jan")) == 0)
    nMonth = 1;
  else if (_tcsicmp(pszToken, _T("Feb")) == 0)
    nMonth = 2;
  else if (_tcsicmp(pszToken, _T("Mar")) == 0)
    nMonth = 3;
  else if (_tcsicmp(pszToken, _T("Apr")) == 0)
    nMonth = 4;
  else if (_tcsicmp(pszToken, _T("May")) == 0)
    nMonth = 5;
  else if (_tcsicmp(pszToken, _T("Jun")) == 0)
    nMonth = 6;
  else if (_tcsicmp(pszToken, _T("Jul")) == 0)
    nMonth = 7;
  else if (_tcsicmp(pszToken, _T("Aug")) == 0)
    nMonth = 8;
  else if (_tcsicmp(pszToken, _T("Sep")) == 0)
    nMonth = 9;
  else if (_tcsicmp(pszToken, _T("Oct")) == 0)
    nMonth = 10;
  else if (_tcsicmp(pszToken, _T("Nov")) == 0)
    nMonth = 11;
  else if (_tcsicmp(pszToken, _T("Dec")) == 0)
    nMonth = 12;
  else
    bSuccess = FALSE;
  return bSuccess;
}

BOOL CSockHTTPServer::ParseDate(const CString& sField, SYSTEMTIME& time)
{
  //This method understands RFC 1123, RFC 850 and asctime formats

  //What will be the return value from this function (assume the worst)
  BOOL bSuccess = FALSE;

  //Http times never include a millisecond field, so just set it to zero
  time.wMilliseconds = 0;

  int nLength = sField.GetLength();
  if (nLength > 5)
  {
    TCHAR cThirdCharacter = sField.GetAt(3);
    if (cThirdCharacter == _T(',')) //Parsing a RFC 1123 format date
    {
      //First the weekday
      static TCHAR* pszTokens = _T(", :");
      int nTokenPosition = 0;
      CString sToken(sField.Tokenize(pszTokens, nTokenPosition));
      if (nTokenPosition == -1)
        return FALSE;
      bSuccess = ParseWeekDay(sToken, time.wDayOfWeek);

      //Then the day of the month
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wDay = static_cast<WORD>(_ttoi(sToken));

      //Then the month
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      bSuccess = bSuccess && ParseMonth(sToken, time.wMonth);

      //And the year
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wYear = static_cast<WORD>(_ttoi(sToken));

      //And the hour
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wHour = static_cast<WORD>(_ttoi(sToken));

      //And the minute
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wMinute = static_cast<WORD>(_ttoi(sToken));

      //And the second
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wSecond = static_cast<WORD>(_ttoi(sToken));
    }
    else if (cThirdCharacter == _T(' ')) //Parsing an asctime format date
    {
      //First the weekday
      static TCHAR* pszTokens = _T(", :");
      int nTokenPosition = 0;
      CString sToken(sField.Tokenize(pszTokens, nTokenPosition));
      if (nTokenPosition == -1)
        return FALSE;
      bSuccess = ParseWeekDay(sToken, time.wDayOfWeek);

      //Then the month
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      bSuccess = bSuccess && ParseMonth(sToken, time.wMonth);

      //Then the day of the month
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wDay = static_cast<WORD>(_ttoi(sToken));

      //And the hour
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wHour = static_cast<WORD>(_ttoi(sToken));

      //And the minute
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wMinute = static_cast<WORD>(_ttoi(sToken));

      //And the second
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wSecond = static_cast<WORD>(_ttoi(sToken));

      //And the year
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wYear = static_cast<WORD>(_ttoi(sToken));
    }
    else //Must be a RFC 850 format date
    {
      //First the weekday
      static TCHAR* pszTokens = _T(", :-");
      int nTokenPosition = 0;
      CString sToken(sField.Tokenize(pszTokens, nTokenPosition));
      if (nTokenPosition == -1)
        return FALSE;
      bSuccess = ParseWeekDay(sToken, time.wDayOfWeek);

      //Then the day of the month
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wDay = static_cast<WORD>(_ttoi(sToken));

      //Then the month
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      bSuccess = bSuccess && ParseMonth(sToken, time.wMonth);

      //And the year (2 Digits only, so make some intelligent assumptions)
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wYear = static_cast<WORD>(_ttoi(sToken));
      if (time.wYear < 50)
        time.wYear += 2000;
      else if (time.wYear < 100)
        time.wYear += 1900; 

      //And the hour
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wHour = static_cast<WORD>(_ttoi(sToken));

      //And the minute
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wMinute = static_cast<WORD>(_ttoi(sToken));

      //And the second
      sToken = sField.Tokenize(pszTokens, nTokenPosition);
      if (nTokenPosition == -1)
        return FALSE;
      time.wSecond = static_cast<WORD>(_ttoi(sToken));
    }
  }

  return bSuccess;
}

BOOL CSockHTTPServer::ParseAuthorizationLine(const CString& sField)
{
  //Validate our parameters
#if 0
   AFXASSUME(m_pServer);
  CW3MFCServerSettings* pSettings = m_pServer->GetSettings();
  AFXASSUME(pSettings);

  //What will be the return value from this function (assume the best)
  BOOL bSuccess = TRUE;

  static char* pszTokens = " ";
  int nTokenPosition = 0;
  CStringA sAsciiField(sField);
  CStringA sToken(sAsciiField.Tokenize(pszTokens, nTokenPosition));
  if (sToken.GetLength())
  {
    //Parse out the base64 encoded username and password if we are doing Basic authentication
    if ((sToken.CompareNoCase("basic") == 0) && pSettings->m_bAllowBasicAuthentication)
    {
      //Assume the worst
      bSuccess = FALSE;
    
      //Move to the base64 encoded data after the text "Basic"
      sToken = sAsciiField.Tokenize(pszTokens, nTokenPosition);
      if (sToken.GetLength())
      {
        //Decode the base64 string passed to us
        int nEncodedLength = sToken.GetLength();
        int nDecodedLength = ATL::Base64DecodeGetRequiredLength(nEncodedLength);
        ATL::CHeapPtr<BYTE> szOutput;
        if (szOutput.Allocate(nDecodedLength + 1))
        {
          int nOutputLength = nDecodedLength;
          if (ATL::Base64Decode(sToken, nEncodedLength, szOutput.m_pData, &nOutputLength))
          {
            //NULL terminate the decoded data
            szOutput.m_pData[nOutputLength] = '\0';

            //Extract the username and password from the decoded data
            CString sOutput(szOutput);
            int nColon = sOutput.Find(_T(":"));
            if (nColon != -1)
            {
              m_Request.m_AuthorizationType = CW3MFCRequest::HTTP_AUTHORIZATION_PLAINTEXT;
              m_Request.m_sUserName = sOutput.Left(nColon);
              m_Request.m_sPassword = sOutput.Right(sOutput.GetLength() - nColon - 1);
              bSuccess = TRUE;
            }
          }
        }
      }
    }
  #ifndef W3MFC_NO_SSPI_SUPPORT
    else if ((sToken.CompareNoCase("ntlm") == 0) && pSettings->m_bAllowNTLMAuthentication && !m_Request.m_bAuthenticationComplete)
    {
      //Assume the worst
      bSuccess = FALSE;
    
      //Move to the encoded data after the text "NTLM"
      sToken = sAsciiField.Tokenize(pszTokens, nTokenPosition);
      if (sToken.GetLength())
      {
        int nEncodedLength = sToken.GetLength();
        int nDecodedLength = ATL::Base64DecodeGetRequiredLength(nEncodedLength);
        ATL::CHeapPtr<BYTE> szInput;
        if (szInput.Allocate(nDecodedLength + 1))
        {
          int nInputLength = nDecodedLength;
          if (ATL::Base64Decode(sToken, nEncodedLength, szInput.m_pData, &nInputLength))
          {
            //Get SSPI to act on the received data

            //First setup the buffers
            SecBuffer secBufferIn[1];
            secBufferIn[0].BufferType = SECBUFFER_TOKEN;
            secBufferIn[0].cbBuffer = nInputLength;
            secBufferIn[0].pvBuffer = szInput.m_pData; 

            SecBufferDesc secbufDescriptorIn;
            secbufDescriptorIn.ulVersion = SECBUFFER_VERSION;
            secbufDescriptorIn.cBuffers = 1;
            secbufDescriptorIn.pBuffers = secBufferIn;

            SecBuffer secBufferOut[1];
            secBufferOut[0].BufferType = SECBUFFER_TOKEN;
            secBufferOut[0].cbBuffer = 0;
            secBufferOut[0].pvBuffer = NULL;

            SecBufferDesc secbufDescriptorOut;
            secbufDescriptorOut.ulVersion = SECBUFFER_VERSION;
            secbufDescriptorOut.cBuffers = 1;
            secbufDescriptorOut.pBuffers = secBufferOut;

            //Call SSPI
            ULONG nContextAttributes;
            SECURITY_STATUS ss = AcceptSecurityContext(m_pServer->GetCredentialHandle(), m_Request.m_bFirstAuthenticationRequest ? NULL : &m_Request.m_ContentHandle, 
                                                       &secbufDescriptorIn, ASC_REQ_STREAM | ASC_REQ_ALLOCATE_MEMORY, SECURITY_NETWORK_DREP, 
                                                       &m_Request.m_ContentHandle, &secbufDescriptorOut, &nContextAttributes, NULL);

            //It's no longer the first request
            m_Request.m_bFirstAuthenticationRequest = FALSE;

            //Setup the output data (if any)
            BOOL bAllocateOK = TRUE;
            if (secBufferOut[0].cbBuffer)
            {
              nEncodedLength = ATL::Base64EncodeGetRequiredLength(secBufferOut[0].cbBuffer, ATL_BASE64_FLAG_NOCRLF);
              
              ATL::CHeapPtr<char> szOutput;
              if (szOutput.Allocate(nEncodedLength + 1))
              {
                //Base 64 encode the data to be sent
                ATL::Base64Encode(static_cast<const BYTE*>(secBufferOut[0].pvBuffer), secBufferOut[0].cbBuffer, szOutput.m_pData, &nEncodedLength, ATL_BASE64_FLAG_NOCRLF);

                //Null terminate the data
                szOutput.m_pData[nEncodedLength] = '\0';

                //Set the data to return to the client
                m_Request.m_sAuthenticationResponse = szOutput;
              }
              else
                bAllocateOK = FALSE;
              
              //Also free the data allocated by SSPI
              FreeContextBuffer(secBufferOut[0].pvBuffer);
            }

            //Do we need more data from the client?
            m_Request.m_bAuthenticationComplete = (ss != SEC_I_CONTINUE_NEEDED);

            //Is the authentication handshake completed
            if (ss == SEC_E_OK)
              m_Request.m_AuthorizationType = CW3MFCRequest::HTTP_AUTHORIZATION_NTLM;

            bSuccess = (ss == SEC_E_OK || ss == SEC_I_CONTINUE_NEEDED);
            if (bSuccess && !bAllocateOK)
              bSuccess = FALSE;
          }
        }
      }
    }
  #endif //W3MFC_NO_SSPI_SUPPORT
  }
  return bSuccess;
#endif
  return true;
}

CString CSockHTTPServer::GetHTMLErrorPage(int nStatusCode, LPCTSTR szErrorMessage)
{
	CString sPage;
	sPage.Format("<HTML><title>Error %u</title><BODY><h1>Error %u</h1><p>%s</p><hr><address>%s</address></BODY></HTML>", nStatusCode, nStatusCode, szErrorMessage, m_sServerName);
	return sPage;
}

DWORD CSockHTTPServer::ReturnErrorMessage(int nStatusCode, LPCTSTR szErrorMessage)
{
	//Validate our parameters

	//Form the body of the response
	IFLOGDebugf("Sending error message status:%u [%s].",nStatusCode, szErrorMessage);
	CStringA sBody = GetHTMLErrorPage(nStatusCode, szErrorMessage);
	int nBodyLength = sBody.GetLength();

	if (m_Request.m_dwHttpVersion > MAKELONG(9, 0))
	{
		//Form the header of the response
		CHTTPResponseHeader responseHdr;
		responseHdr.MakeCommonHeader(nStatusCode, nBodyLength, m_bResponseKeepAlive, GetServerName());
		//Send the header and body all in one
		SendHeaderAndDatas(responseHdr.GetData(), sBody);
	}
	else
	{
		//No header sent for HTTP v0.9
		//so just send the body
#ifdef W3MFC_SSL_SUPPORT
		m_Socket.SendWithRetry(sBody, nBodyLength, pSettings->m_dwWritableTimeout, m_SSL);
#else    
		SendHeaderAndDatas(NULL, sBody);
#endif
	}

	//  Log the information
	PostLog(nStatusCode, nBodyLength);

	//  Return the body length
	return nBodyLength;
}

BOOL CSockHTTPServer::SendHeaderAndDatas(LPCTSTR szHeader, LPCTSTR szDatas)
{
	CString sFrame;
	if (szHeader)
	{
		if (M_LOGGER && M_LOGGER->GetThresholdPriority() >= LP_Debug)
			LOGDump(LP_Debug, "ResponseHeader", (LPBYTE)(LPCSTR)szHeader, (int)_tcslen(szHeader));
		sFrame += szHeader;
	}
	if (szDatas)
	{
		if (M_LOGGER && M_LOGGER->GetThresholdPriority() >= LP_Trace)
			LOGDump(LP_Trace, "Datas", (LPBYTE)(LPCSTR)szDatas, (int)_tcslen(szDatas));
		sFrame += szDatas;
	}
	return Send((LPBYTE)(LPCSTR)sFrame, sFrame.GetLength());
}

void CSockHTTPServer::PostLog(int nHTTPStatusCode, DWORD dwBodyLength)
{
	//The default is to just TRACE the details

	//Get the current date and time
	time_t now = time(NULL);
	tm localtm;
	localtime_s(&localtm, &now);
	tm* pNow = &localtm;  

	//Get the time zone information
	TIME_ZONE_INFORMATION tzi;
	memset(&tzi, 0, sizeof(tzi));
	int nTZBias = 0;
	if (GetTimeZoneInformation(&tzi) == TIME_ZONE_ID_DAYLIGHT)
		nTZBias = tzi.Bias + tzi.DaylightBias;
	else
		nTZBias = tzi.Bias;

	//Format the date and time appropiately
	TCHAR sDateTime[64];
	sDateTime[0] = _T('\0');
	_tcsftime(sDateTime, 64, _T("[%d/%b/%Y:%H:%M:%S"), pNow);

	//Display the connections to the console window
	CString sUser(m_Request.m_sUserName);
	if (sUser.IsEmpty()) sUser = _T("-");
	
	LOGDebugf(_T("%s - %s [%s %+.2d%.2d] \"%s\" %d %d\n"), "ClientAddress", 
		sUser.operator LPCTSTR(), sDateTime, -nTZBias/60, abs(nTZBias)%60, m_Request.m_sRequest.operator LPCTSTR(), nHTTPStatusCode, dwBodyLength);
}

CHTTPResponseHeader::CHTTPResponseHeader() : m_bEntitySeparator(TRUE)
{
}

void CHTTPResponseHeader::MakeCommonHeader(int nStatusCode, int nBodyLength, BOOL bResponseKeepAlive, LPCTSTR szServerName)
{
	AddStatusCode(nStatusCode);
	SYSTEMTIME st;
	GetSystemTime(&st);
	AddDate(st);
	AddServer(szServerName);
	//responseHdr.AddW3MfcAllowFields(pSettings->m_bAllowDeleteRequest);
	if (bResponseKeepAlive)
		AddKeepAlive();
	AddContentLength(nBodyLength);
	AddContentType(_T("text/html"));
}

void CHTTPResponseHeader::SetAddEntitySeparator(BOOL bSeparator)
{
  m_bEntitySeparator = bSeparator;
}

void CHTTPResponseHeader::AddStatus(LPCSTR pszStatusString)
{
  CStringA sLine;
  sLine.Format("HTTP/1.0 %s\r\n", pszStatusString);
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddExtraHeaders(LPCSTR pszHeaders)
{
  m_sHeader += pszHeaders;
}

void CHTTPResponseHeader::AddStatusCode(int nStatusCode)
{
  CStringA sLine;
  switch (nStatusCode)
  {
    case 200: sLine = "HTTP/1.0 200 OK\r\n";                    break;
    case 201: sLine = "HTTP/1.0 201 Created\r\n";               break;
    case 202: sLine = "HTTP/1.0 202 Accepted\r\n";              break;
    case 204: sLine = "HTTP/1.0 204 No Content\r\n";            break;
    case 300: sLine = "HTTP/1.0 300 Multiple Choices\r\n";      break;
    case 301: sLine = "HTTP/1.0 301 Moved Permanently\r\n";     break;
    case 302: sLine = "HTTP/1.0 302 Moved Temporarily\r\n";     break;
    case 304: sLine = "HTTP/1.0 304 Not Modified\r\n";          break;
    case 400: sLine = "HTTP/1.0 400 Bad Request\r\n";           break;
    case 401: sLine = "HTTP/1.0 401 Unauthorized\r\n";          break;
    case 403: sLine = "HTTP/1.0 403 Forbidden\r\n";             break;
    case 404: sLine = "HTTP/1.0 404 Not Found\r\n";             break;
    case 500: sLine = "HTTP/1.0 500 Internal Server Error\r\n"; break;
    case 501: sLine = "HTTP/1.0 501 Not Implemented\r\n";       break;
    case 502: sLine = "HTTP/1.0 502 Bad Gateway\r\n";           break;
    case 503: sLine = "HTTP/1.0 503 Service Unavailable\r\n";   break;
    default: sLine.Format("HTTP/1.0 %d\r\n", nStatusCode);      break;
  }
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddContentLength(int nSize)
{
  CStringA sLine;
  sLine.Format("Content-Length: %d\r\n", nSize);
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddContentType(LPCTSTR pszMediaType)
{
  CStringA sLine;
  sLine.Format("Content-Type: %s\r\n", CStringA(pszMediaType).operator LPCSTR());
  m_sHeader += sLine;
}

CStringA CHTTPResponseHeader::DateToStr(const SYSTEMTIME& st)
{
  static char* sMonth[] =  
  {
    "Jan",
    "Feb",
    "Mar", 
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  };

  static char* sDay[] =
  {
    "Sun",
    "Mon",
    "Tue", 
    "Wed",
    "Thu",
    "Fri",
    "Sat",
  };

  CStringA sDate;
  sDate.Format("%s, %02d %s %04d %02d:%02d:%02d GMT", sDay[st.wDayOfWeek], 
               st.wDay, sMonth[st.wMonth-1], st.wYear, st.wHour, st.wMinute, st.wSecond);
  return sDate;
}

void CHTTPResponseHeader::AddW3MfcAllowFields(BOOL bAllowDeleteRequest)
{
  if (bAllowDeleteRequest)
    m_sHeader += "Allow: GET, POST, HEAD, DELETE\r\n";
  else
    m_sHeader += "Allow: GET, POST, HEAD\r\n";
}

void CHTTPResponseHeader::AddDate(const SYSTEMTIME& st)
{
  CStringA sDate(DateToStr(st));
  CStringA sLine;
  sLine.Format("Date: %s\r\n", sDate.operator LPCSTR());
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddLastModified(const SYSTEMTIME& st)
{
  CStringA sDate(DateToStr(st));
  CStringA sLine;
  sLine.Format("Last-Modified: %s\r\n", sDate.operator LPCSTR());
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddExpires(const SYSTEMTIME& st)
{
  CStringA sDate(DateToStr(st));
  CStringA sLine;
  sLine.Format("Expires: %s\r\n", sDate.operator LPCSTR());
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddKeepAlive()
{
  m_sHeader += _T("Connection: Keep-Alive\r\n");
}

void CHTTPResponseHeader::AddWWWAuthenticateBasic(LPCTSTR pszRealm)
{
  CStringA sLine;
  sLine.Format("WWW-Authenticate: Basic realm=%s\r\n", CStringA(pszRealm).operator LPCSTR());
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddWWWAuthenticateNTLM(LPCTSTR pszMessage)
{
  CStringA sLine;
  if (_tcslen(pszMessage))
    sLine.Format("WWW-Authenticate: NTLM %s\r\n", CStringA(pszMessage).operator LPCSTR());
  else
    sLine = _T("WWW-Authenticate: NTLM\r\n");
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddLocation(LPCTSTR pszLocation)
{
  CStringA sLine;
  sLine.Format("Location: %s\r\n", CStringA(pszLocation).operator LPCSTR());
  m_sHeader += sLine;
}

void CHTTPResponseHeader::AddServer(LPCTSTR pszServer)
{
  CStringA sLine;
  sLine.Format("Server: %s\r\n", CStringA(pszServer).operator LPCSTR());
  m_sHeader += sLine;    
}

CStringA CHTTPResponseHeader::GetData()
{
  //What will be the return value from this function
  CStringA sHeader(m_sHeader);
  
  //Add the entity separator if required
  if (m_bEntitySeparator)
    sHeader += "\r\n";
    
  return sHeader;
}

