#pragma once
#include "sock.h"

#include <LibSystem.h>

class CMimeType
{
public:
	CMimeType();
	static CString GetMimeTypeFromExtension(LPCTSTR ext);
};

class CHTTPRequestParam
{
public:
	CHTTPRequestParam() {}
	CString m_sParam;
	CString m_sValue;
};
typedef CArray<CHTTPRequestParam, CHTTPRequestParam&> CHTTPRequestParams;

class CHTTPRequest
{
public:
	CHTTPRequest(int nHeaderHashSize = 60);

	void GetQueryStringParams(CHTTPRequestParams& params);
	static void GetQueryStringParams(LPCTSTR queryString, CHTTPRequestParams& params);

	typedef enum { HTTP_VERB_GET, HTTP_VERB_POST , HTTP_VERB_HEAD, HTTP_VERB_PUT, HTTP_VERB_LINK, HTTP_VERB_DELETE, HTTP_VERB_UNLINK, HTTP_VERB_UNKNOWN, } RequestVerb;

	CString m_sRequest;
	CString m_sVerb;
	RequestVerb m_Verb;
	CString m_sRawURL;
	CString m_sURL;
	CString m_sPagePath;
	CString m_sExtra;
	CString m_sRawExtra;
	CString m_sUserAgent;
	CString m_sAccept;
	CString m_sHost;
	CString m_sContentType;
	DWORD   m_nContentLength;
	BOOL    m_bKeepAlive;
	CString m_sUserName;

	DWORD m_dwHttpVersion;

	BOOL  m_bIfModifiedSincePresent;
	SYSTEMTIME         m_IfModifiedSince;

	char    m_pRawRequest[65536];
	DWORD   m_dwRawRequestLength;

	LPBYTE  m_pRawEntity;
	DWORD   m_dwRawEntityLength;

	CMapStringToString m_HeaderMap;

private:
	void Init(int nHeaderHashSize);
};

class CSockHTTPServer;

class CHTTPResponseHeader
{
public:
	//Constructors / Destructors
	CHTTPResponseHeader();

	void MakeCommonHeader(int nStatusCode, int nBodyLength, BOOL bResponseKeepAlive, LPCTSTR szServerName);
	//Methods
	void     AddStatusCode(int nStatusCode);
	void     AddStatus(LPCSTR pszStatusString);
	void     AddContentLength(int nSize);
	void     AddContentType(LPCTSTR pszMediaType);
	void     AddKeepAlive();
	void     AddDate(const SYSTEMTIME& st);
	void     AddLastModified(const SYSTEMTIME& st);
	void     AddWWWAuthenticateBasic(LPCTSTR pszRealm);
	void     AddWWWAuthenticateNTLM(LPCTSTR pszMessage);
	void     AddExpires(const SYSTEMTIME& st);
	void     AddLocation(LPCTSTR pszLocation);
	void     AddServer(LPCTSTR pszServer);
	void     AddW3MfcAllowFields(BOOL bAllowDeleteRequest);
	void     AddExtraHeaders(LPCSTR pszHeaders);
	void     SetAddEntitySeparator(BOOL bSeparator);

	CStringA DateToStr(const SYSTEMTIME& st);
#ifdef W3MFC_SSL_SUPPORT
	BOOL     Send(CW3MFCSocket& socket, DWORD dwTimeout, CSSL& ssl);
#endif
	BOOL     Send(CSockHTTPServer& socket, DWORD dwTimeout);
	CStringA GetData();

protected:
	CStringA m_sHeader;
	BOOL     m_bEntitySeparator;
};

class CSockHTTPServer : public CSock
{
public:
	static LPCTSTR GetClassName();
	CSockHTTPServer(LPCTSTR szServerName = _T(""));
	~CSockHTTPServer(void);

	void SetExchangeID(UINT uID);
	BOOL Run(HANDLE hEventServerWantToStop);

	virtual BOOL HandleGetPostHead() { return false; }

	virtual DWORD CSockHTTPServer::ReturnErrorMessage(int nStatusCode, LPCTSTR szErrorMessage);

	virtual CString GetServerName() { return GetEnv(_T("COMPUTERNAME")); }
	
private:
	CString GetHTMLErrorPage(int nStatusCode, LPCTSTR szErrorMessage);

	void PostLog(int nHTTPStatusCode, DWORD dwBodyLength);

	BOOL SplitRequestLine(LPCTSTR szLine, CString& sParam, CString& sValue);
	
	BOOL ParseRequest();
	BOOL ParseSimpleRequestLine(const CString& sLine);
	BOOL ParseRequestLine(const CString& sField, const CString& sValue);

	BOOL ParseWeekDay(LPCTSTR pszToken, WORD& nWeekDay);
	BOOL ParseMonth(LPCTSTR pszToken, WORD& nMonth);
	BOOL ParseDate(const CString& sField, SYSTEMTIME& time);
	BOOL ParseAuthorizationLine(const CString& sField);

	CString UTF8ToCString(const CString& sURL);
	TCHAR IntToHex(int Character);
	int HexToInt(TCHAR ch);

protected:
	BOOL SendHeaderAndDatas(LPCTSTR szHeader, LPCTSTR szDatas);

	CString URLEncode(const CString& sURL);
	CString URLDecode(const CString& sURL);

	CString m_sServerName;

	BOOL m_bResponseKeepAlive;
	UINT m_uID;

	CHTTPRequest m_Request;

	DECLLogger(M_LOGGER);
};
