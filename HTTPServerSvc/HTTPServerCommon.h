#pragma once

#include "MacrosCommon.h"

#if 0
#ifdef _DEBUG
#define STR_REG_HTTPSERVER_ROOT_HKLM_KEY			_CNV("Software\\FCSoftware\\HTTPServer") _CNV("(Dbg)")
#define STR_REG_HTTPSERVER_ROOT_HKCU_KEY			_CNV("Software\\FCSoftware\\HTTPServer") _CNV("(Dbg)")
#else // !_DEBUG
#define STR_REG_HTTPSERVER_ROOT_HKLM_KEY			_CNV("Software\\FCSoftware\\HTTPServer")
#define STR_REG_HTTPSERVER_ROOT_HKCU_KEY			_CNV("Software\\FCSoftware\\HTTPServer")
#endif
#endif

//	Loggers
typedef enum 
{  
	NOID_LOGGER,
	//
	BASE_PRODUCT_LOGGER,
	//	Svc
	HTTPSERVER_SVC_MAIND_LOGGER,
	HTTPSERVER_SVC_HTTPD_LOGGER,
	SOCKHTTPSERVER_LOGGER,
}Log4CppLogger;

//	Appenders
typedef enum 
{  
	NOID_APPENDER,
	//
	DEFAULT_APPENDER,
	//	Svc
	HTTPSERVER_SVCMAIND_APPENDER,
	HTTPSERVER_HTTPD_APPENDER,
}Log4CppAppender;


class CHTTPServerCommon
{
public:
	static LPCTSTR GetLog4CppLogger(Log4CppLogger loggerId);
	static BOOL    GetLog4CppLoggerDef(int index, CString& name, CString& description);
	static LPCTSTR GetLog4CppAppender(Log4CppAppender appenderId);
	static BOOL    GetLog4CppAppenderDef(int index, CString& name, CString& description);

	static CString ms_sRegEntryServiceParameters;
	static CString ms_sRegEntryHKCU;
};

#define L4C_APPENDER(_id)	CHTTPServerCommon::GetLog4CppAppender(_id)
#define L4C_LOGGER(_id)		CHTTPServerCommon::GetLog4CppLogger(_id)

DECLARE_REGGETSET_PARAM(HTTPServerSvc, WWWDir, CString);
DECLARE_REGGETSET_PARAM(HTTPServerSvc, DirectoryIndex, CString);
DECLARE_REGGETSET_PARAM(HTTPServerSvc, LogDir, CString);
DECLARE_REGGETSET_PARAM(HTTPServerSvc, HTTPDListenAddr, CString);
DECLARE_REGGETSET_PARAM(HTTPServerSvc, HTTPDListenPort, CString);
DECLARE_REGGETSET_PARAM(HTTPServerSvc, AllowedNetworks, CString);

DECLARE_REGGETSET_PARAM(HTTPServer, SocketBufferSize, DWORD);