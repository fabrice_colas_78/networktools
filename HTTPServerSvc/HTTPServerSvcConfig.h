#pragma once

class CHTTPServerSvcConfig
{
public:
	CHTTPServerSvcConfig(void);
	~CHTTPServerSvcConfig(void);
	
	void Load();

	const CString& GetWWWDir();

	const CString& GetLogDir();

	CString GetHTTPServerHTTPDListenAddr();
	CString GetHTTPServerHTTPDListenPort();

	BOOL IsDebugMode() { return m_fDebugMode; }

private:
	CString m_sHTTPServerHTTPDListenAddr;
	CString m_sHTTPServerHTTPDListenPort;
	
	CString m_sWWWDir;

	CString m_sLogDir;
	
	BOOL    m_fDebugMode;
};
