//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HTTPServerSvc.rc
//

#define IDS_APP_TITLE			103

#define IDS_DIRECTORY_LISTING_HEADER    1
#define IDS_DIRECTORY_LISTING_FOOTER    2
#define IDH_FILE_DELETED_OK             108
#define IDH_302                         109
#define IDH_400                         110
#define IDH_401                         111
#define IDH_404                         112
#define IDH_500                         113
#define IDH_501                         114
#define IDI_MAINFRM                     117

#define IDR_HISTO						120

// Valeurs par d�faut suivantes des nouveaux objets
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE	124
#define _APS_NEXT_COMMAND_VALUE		40001
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		101
#endif
#endif
